/**********************************************
 * 자동분산투자 insert
 * @returns
 **********************************************/
$(document).on("click", ".submit-btn", function() {
	var frm = document.autoForm
	
	if(!chkLogin(frm.member_seq.value)){
		return
	}
	var samprod_yn = null;
	if($(".switch-on").css("display") == "block") {
		samprod_yn = "Y";
	} else {
		samprod_yn = "N";
	}
	var auto_type = $("button.active").val();
	var auto_amount = $(".noUi-handle-lower").attr("aria-valuetext");
	var min_profit = $("#fund-select02-value-low").text();
	var max_profit = $("#fund-select02-value-upper").text();
	var min_period = $("#fund-select03-value-low").text();
	var max_period = $("#fund-select03-value-upper").text();

	var param = {
		"auto_type" : auto_type,
		"auto_amount" : auto_amount,
		"min_profit" : min_profit,
		"max_profit" : max_profit,
		"min_period" : min_period,
		"max_period" : max_period,
		"member_seq" : frm.member_seq.value,
		"samprod_yn" : samprod_yn
		
	}
	request("POST", "/api/investment", param);

})

/*
 * requestApi.js
 * 
 * 2020.04.13 김우철
 * 
 * API 모듈화
 */
function request(method, url, param) {
	const xhr = new XMLHttpRequest();
	var response = null;
	var result;

	xhr.open(method, url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(JSON.stringify(param));

	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			console.log(xhr.responseText);

			response = JSON.parse(this.responseText);

			if (response.result.result_code == 200) {
				alert("자동분산투자 신청이 완료되었습니다.")
				location.href = "/";
			} else {
				alert("자동분산투자에 실패하였습니다. 문의 바랍니다.");
			}

		} else {
			console.log("Error!");
		}
	};
}