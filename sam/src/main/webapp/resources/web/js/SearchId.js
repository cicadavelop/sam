/**********************************************
 * 아이디 찾기
 * @returns
 **********************************************/
$(document).on("click", ".find-id-btn", function() {
	var frm = document.searchIdForm;
	isValid = true;

	if (!frm.name.value) {
		alert("이름을 입력해주세요.");
		isValid = false;
		return;
	}
	var nameRule = /^[가-힣]{2,4}$/;
	if (!nameRule.test(frm.name.value)) {
		alert("정확한 이름을 입력해주세요.");
		isValid = false;
		return;
	}
	if(!frm.phone.value) {
		alert("전화번호를 입력해주세요.");
		isValid = false;
		return;
	}
	var phoneRule = /^\d{3}\d{3,4}\d{4}$/;
	if(!phoneRule.test(frm.phone.value)) {
		alert("전화번호 형식이 일치하지 않습니다.");
		isValid = false;
		return;
	}

	var param = {
		"name" : frm.name.value,
		"phone" : frm.phone.value
	}
	request("POST", "/find/id", param);

})


/*
 * requestApi.js
 * 
 * 2020.04.13 김우철
 * 
 * API 모듈화
 */
function request(method, url, param) {
	const xhr = new XMLHttpRequest();
	var response = null;
	var result;

	xhr.open(method, url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(JSON.stringify(param));

	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			console.log(xhr.responseText);

			response = JSON.parse(this.responseText);

			if (response.result.result_code == 200) {
				alert("회원님의 아이디는 "+response.data.findid.member_id+"입니다.");
				location.href="/member/login"
			} else {
				alert(response.result.result_msg);
			}

			// 커스텀 처리

		} else {
			console.log("Error!");
		}
	};
}