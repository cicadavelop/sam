/**********************************************
 * 아이디 찾기
 * @returns
 **********************************************/
$(document).on("click", ".find-pw-btn", function() {
	var frm = document.searchPwForm;
	isValid = true;
	if (!frm.member_id.value) {
		alert("아이디를 입력해주세요.");
		isValid = false;
		return;
	}
	if (!frm.name.value) {
		alert("이름을 입력해주세요.");
		isValid = false;
		return;
	}
	if(!frm.phone.value) {
		alert("전화번호를 입력해주세요.");
		isValid = false;
		return;
	}

	var param = {
		"member_id" : frm.member_id.value,
		"name" : frm.name.value,
		"phone" : frm.phone.value
	}
	request("POST", "/find/pw", param);

})


/*
 * requestApi.js
 * 
 * 2020.04.13 김우철
 * 
 * API 모듈화
 */
function request(method, url, param) {
	const xhr = new XMLHttpRequest();
	var response = null;
	var result;

	xhr.open(method, url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(JSON.stringify(param));

	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			console.log(xhr.responseText);

			response = JSON.parse(this.responseText);

			if (response.result.result_code == 200) {
				alert("회원님의 비밀번호는 "+response.data.findpw.password+" 입니다.");
				location.href="/";
			} else {
				alert(response.result.result_msg);
			}

			// 커스텀 처리

		} else {
			console.log("Error!");
		}
	};
}