/**********************************************
 * 보도자료 뿌리기
 * @returns
 **********************************************/

	$(document).ready(function(){
		request("GET", "/api/report", null);
	})
	
/**********************************************
 * 
 * @returns
 **********************************************/
$(document).on("click", "findPw", function() {
	var frm = document.loginForm;

	if (!frm.phone.value || !frm.authcode.value) {
		func.inputErrorBorder();
		return;
	}

	var param = {
		"phone" : frm.phone.value,
		"authcode" : frm.authcode.value
	}
	ajaxCallPost("/api/set/password", param, function(res) {
		if (!res.success) {
			func.inputErrorBorder();
		} else {
			popupFunc.openPopupAlert(loginok, loginmsg, function() {
				location.href = "/m/findReset";
			})
		}
	}, function(e) {
		$(".login-error").show();
	});

})

/*
 * requestApi.js
 * 
 * 2020.04.13 김우철
 * 
 * API 모듈화
 */
function request(method, url, param) {
	const xhr = new XMLHttpRequest();
	var response = null;
	var result;

	xhr.open(method, url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(null);
	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			console.log(xhr.responseText);

			response = JSON.parse(this.responseText);
			console.log(response.data);
			if (response.result.result_code == 200) {
				
				var template = $.templates("#selectList"); //<!-- 템플릿 선언 위치 지정 -->
				var htmlOutput = template.render(response.data); //<!-- 렌더링 진행 -->
				$("#news-list").html(htmlOutput); //<!-- 렌더링 결과 뿌려줌 -->
				
			}

			// 커스텀 처리

		} else {
			console.log("Error!");
		}
	};
}

