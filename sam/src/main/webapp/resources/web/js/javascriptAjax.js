/*
requestApi.js

2020.04.13
김우철

API 모듈화
*/
function request(method,url,param){
    const xhr = new XMLHttpRequest();

    var result;

    xhr.open(method, url);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.send(JSON.stringify(param));


    xhr.onreadystatechange = function (e) {
        if (xhr.readyState !== XMLHttpRequest.DONE) return;

        if(xhr.status === 200) { // 200: OK => https://httpstatuses.com
            console.log(xhr.responseText);
        } else {
            console.log("Error!");
        }
    };
}