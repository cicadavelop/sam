$(document).ready(function() {
   // 숫자만 입력
   $(".onlyNum").keyup(function(){$(this).val( $(this).val().replace(/[^0-9]/g,"") );} );
   $(".onlyEng").keyup(function(){$(this).val( $(this).val().replace(/[^\!-z]/g,"") );} );
   $(".onlyEngNum").keyup(function(){$(this).val( $(this).val().replace(/[^\!-z0-9]/g,"") );} );
   $(".onlyNumHyphen").keyup(function(){$(this).val( $(this).val().replace(/[^0-9\-]/g,"") );} );
   $(".onlyNumPeriod").keyup(function(){$(this).val( $(this).val().replace(/[^0-9\.]/g,"") );} );
   $(".notHangle").keyup(function(){$(this).val( $(this).val().replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, "") );} );
   
   // 달력
   $(document).on('focus', ".maskdate", function(i) {
      $(".maskdate").each(function(idx, element) {
          try   {
               $(element).datepicker({ dateFormat: "yy-mm-dd", buttonImage: "/content/images/btn/btnDate.gif", buttonImageOnly: false, autoSize: true, autoFocusNextInput: true });
          }
          catch(e){
             
          }
      });
    });
});

// orient - w : 가로, h : 세로
function wPrint(orient) {
   window.print();
}


/*
jQuery(function(){
   // 시작일
   $('#ins_sdate').focus(function(){ 
      $("#ins_sdate").datepicker({
         dateFormat : 'yy-mm-dd',
         onSelect: function(selected){
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#ins_edate").datepicker("option", "minDate", dt);
            $("#ins_sdate").datepicker('refresh');
         }
      });
   });
   
   // 회원등록: 시작일
   $('#sdate').focus(function(){ 
     $("#sdate").datepicker({
       dateFormat : 'yy-mm-dd',
       onSelect: function(selected){
         var dt = new Date(selected);
         dt.setDate(dt.getDate() + 1);
         $("#edate").datepicker("option", "minDate", dt);
         $("#sdate").datepicker('refresh');
       }
     });
   });
      
  // 시작일
  $('#ins_sdate2').focus(function(){ 
    $("#ins_sdate2").datepicker({
      dateFormat : 'yy-mm-dd',
      onSelect: function(selected){
        var dt = new Date(selected);
        dt.setDate(dt.getDate() + 1);
        $("#ins_edate2").datepicker("option", "minDate", dt);
        $("#ins_sdate2").datepicker('refresh');
      }
    });
  });
  
  
  // 종료일
  $('#ins_edate').focus(function(){   
     $("#ins_edate").datepicker({
        dateFormat : 'yy-mm-dd',
        onSelect: function(selected){
           var dt = new Date(selected);
           var sdate = $('#ins_sdate').val();
           if(selected < sdate || selected == sdate){
              alert('종료일은 시작일과 같거나 이전 일 수 없습니다.');
              $('#ins_edate').val('');
           }else{
              dt.setDate(dt.getDate() - 1);
              $("#ins_sdate").datepicker("option", "maxDate", dt);
              $("#ins_edate").datepicker('refresh');
           }
        } 
     });
  });
  
  // 회원등록: 종료일
  $('#edate').focus(function(){   
    $("#edate").datepicker({
      dateFormat : 'yy-mm-dd',
      onSelect: function(selected){
        var dt = new Date(selected);
        var sdate = $('#sdate').val();
        if(selected < sdate || selected == sdate){
          alert('종료일은 시작일과 같거나 이전 일 수 없습니다.');
          $('#edate').val('');
        }else{
          dt.setDate(dt.getDate() - 1);
          $("#sdate").datepicker("option", "maxDate", dt);
          $("#edate").datepicker('refresh');
        }
      } 
    });
  });
  
  // 종료일
  $('#ins_edate2').focus(function(){   
    $("#ins_edate2").datepicker({
      dateFormat : 'yy-mm-dd',
      onSelect: function(selected){
        var dt = new Date(selected);
        var sdate = $('#ins_sdate2').val();
        if(selected < sdate || selected == sdate){
          alert('종료일은 시작일과 같거나 이전 일 수 없습니다.');
          $('#ins_edate2').val('');
        }else{
          dt.setDate(dt.getDate() - 1);
          $("#ins_sdate2").datepicker("option", "maxDate", dt);
          $("#ins_edate2").datepicker('refresh');
        }
      } 
    });
  });
    
});
*/

(function($){
  actionAjaxCall = function (rsAsync, callUrl, parms, callBackMethod, rsDataType) {
    $.ajax({
      type: 'POST',
      url: callUrl,
        data: parms,
        cache: false,
        async: rsAsync,
        dataType: rsDataType,     //"xml", "html", "script", "json" 등 지정 가능 
        beforeSend: function(request, status) {
        },
        success: function(data, status, request) {
          if (callBackMethod in window) {
              window[callBackMethod](data);
          }
        },
      complete: function(request, status) {
        }
    });
  };
})(jQuery);


var gId       = "";
var gOption   = "";
var gParams   = "";
var gCallback = "";
var gSync     = "";
var gStyle    = "";
var gCss      = "";
var gChange   = "";
/**************************************************************************
* 셀렉트박스 생성
* parameter      :    sId               // ID
*                sValue            // 선택한 값
*                sLang            // 언어
*                sOption            // Option (ALL:전체, SELECT:선택, 그외 null처리)
**************************************************************************/
var setSelectBox = function(sId, sValue, sLang, sOption, sCss) {
   var sParams = {"id":sId, "cFlag":sId, "lang":sLang, "value":sValue};
   
   gId       = sId;
   gOption   = sOption;
   gParams   = sParams;
   gCss      = sCss;
   
   actionAjaxCall(false, "/cmm/cmmCode.do", gParams, "gfn_callback_selectbox", "json");
   
};

var gfn_callback_selectbox = function(data){
   var html = "";
   html += '<select id="' + gId + '" name="' + gId + '"';
   if(gStyle != "") html += ' style="' + gStyle + '"';
   if(gCss != "") html += ' class="' + gCss + '"';
   html += '>';
   
   if("ALL" == gOption.toUpperCase()){
      html += '<option value=""> ::: 전체 :::</option>';
   } else if("SELECT" == gOption.toUpperCase()){
      html += '<option value=""> ::: 선택 :::</option>';
   }
   if(data.success == "Y"){
      $.each(data.codeList,function(index, result) {
         html += '<option value="' + result.code + '"';
         if(gParams.value == result.code) html += ' selected="selected"';
         html += '>' + result.codeNm + '</option>';
      });
   }
   html += '</select>';
   $("#"+gId).html(html);
   
   //초기화
   gId       = "";
   gOption   = "";
   gParams   = "";
   gCallback = "";
   gSync     = "";
   gStyle    = "";
   gCss      = "";
   gChange   = "";
};

/**************************************************************************
* 라디오박스 생성
* parameter      :    sId               // ID
*                sValue            // 선택한 값
*                sLang            // 언어
**************************************************************************/
var setRadioBox = function(sId, sValue, sLang) {
   var sParams = {"id":sId, "cFlag":sId, "lang":sLang, "value":sValue};
   
   gId       = sId;
   gParams   = sParams;
   
   actionAjaxCall(false, "/cmm/cmmCode.do", gParams, "gfn_callback_radiobox", "json");
   
};

var gfn_callback_radiobox = function(data){
   var html = "";
   
   if(data.success == "Y"){
      $.each(data.codeList,function(index, result) {
         html += '<input type="radio" id="' + gId + '_' + index + '" name="' + gId + '"';
         html += ' value="' + result.code + '"';
         if(gParams.value != ""){
            if(gParams.value == result.code)html += ' checked="checked"';
         }
         html += '>';
         html += '<label for="' + gId + '_' + index + '">' + result.codeNm + '</label>';
      });
   }
   
   html += '</select>';
   $("#"+gId).html(html);
   
   //초기화
   gId       = "";
   gOption   = "";
   gParams   = "";
   gCallback = "";
   gSync     = "";
   gStyle    = "";
   gCss      = "";
   gChange   = "";
};

/**************************************************************************
* 체크박스 생성
* parameter      :    sId               // ID
*                sValue            // 선택한 값
*                sLang            // 언어
**************************************************************************/
var setCheckBox = function(sId, sValue, sLang) {
   var sParams = {"id":sId, "cFlag":sId, "lang":sLang, "value":sValue};
   
   gId       = sId;
   gParams   = sParams;
   
   actionAjaxCall(false, "/cmm/cmmCode.do", gParams, "gfn_callback_checkbox", "json");
   
};

var gfn_callback_checkbox = function(data){
   var html = "";
   
   if(data.success == "Y"){
      $.each(data.codeList,function(index, result) {
         html += '<input type="checkbox" id="' + gId + '_' + index + '" name="' + gId + '"';
         html += ' value="' + result.code + '"';
         if(gParams.value != ""){
            if(gParams.value == result.code)html += ' checked="checked"';
         }
         html += '>';
         html += '<label for="' + gId + '_' + index + '">' + result.codeNm + '</label>' + '<br/>';
      });
   }
   
   html += '</select>';
   $("#"+gId).html(html);
   
   //초기화
   gId       = "";
   gOption   = "";
   gParams   = "";
   gCallback = "";
   gSync     = "";
   gStyle    = "";
   gCss      = "";
   gChange   = "";
};

/**************************************************************************
* 셀렉트박스 생성2
* parameter      :    sId               // ID
*                sOption            // Option (ALL:전체, SELECT:선택, 그외 null처리)
*                sParam   :   gubun   // 구분
*                         codeGrp   // 코드그룹
*                         code   // 코드
*                        lang   // 언어 
*                        value   // 선택한 값
*               sCallback         // 콜백함수
*               sSync            // true, false
*               sStyle            // 스타일
*               sCss            // css
**************************************************************************/
var setSelectBox2 = function(sId, sOption, sParams, sCallback, sSync, sStyle, sCss, sChange) {
   gId       = sId;
   gOption   = sOption;
   gParams   = sParams;
   gCallback = sCallback;
   gSync     = sSync;
   gStyle    = sStyle;
   gCss      = sCss;
   gChange   = sChange;
   
   actionAjaxCall(sSync, "/cmm/cmmCode.do", sParams, "gfn_callback_selectbox2", "json");
   
};

var gfn_callback_selectbox2 = function(data){
   var html = "";
   html += '<select id="' + gId + '" name="' + gId + '"';
   if(gStyle != "") html += ' style="' + gStyle + '"';
   if(gCss != "") html += ' class="' + gCss + '"';
   if(gChange != "") html += ' onchange="' + gChange + '"';
   html += '>';
   
   if (gId == "searchHotelNo") {
      if (data.loginLevel != "O") {
         if("ALL" == gOption.toUpperCase()){
            html += '<option value=""> ::: 전체 :::</option>';
         } else if("SELECT" == gOption.toUpperCase()){
            html += '<option value=""> ::: 선택 :::</option>';
         }
      }
   } else {
      if("ALL" == gOption.toUpperCase()){
         html += '<option value=""> ::: 전체 :::</option>';
      } else if("SELECT" == gOption.toUpperCase()){
         html += '<option value=""> ::: 선택 :::</option>';
      }
   }
   if(data.success == "Y"){
      $.each(data.codeList,function(index, result) {
         html += '<option value="' + result.code + '"';
         if(gParams.value == result.code) html += ' selected="selected"';
         html += '>' + result.codeNm + '</option>';
      });
   }
   
   html += '</select>';
   $("#"+gId).html(html);
   
   if (gCallback in window) {
        window[gCallback](data);
    }
   
   if (gId == "searchHotelNo") {
      $("select[name=searchHotelNo]").change(function() {
         fnSetHotelNo();
      });
      fnGetHotelNo();
   }
   
   
   //초기화
   gId       = "";
   gOption   = "";
   gParams   = "";
   gCallback = "";
   gSync     = "";
   gStyle    = "";
   gCss      = "";
   gChange   = "";
};

/**
 * 화면에서 선택된 호텔코드를 세션에 세팅
 */
function fnSetHotelNo() {
   var hotelNoSelected = $("select[name=searchHotelNo]").val();
   var sParams = {"hotelNoSelected":hotelNoSelected};
   actionAjaxCall(false, "/mb/wmb_100HotelNoSet.do", sParams, "", "json");
}

/**
 * 세션에 저장된 선택된 호텔코드를 조회
 */
function fnGetHotelNo() {
   actionAjaxCall(false, "/mb/wmb_100HotelNoGet.do", "", "fnGetHotelNoCallback", "json");
}

/**
 * 세션에 저장된 선택된 호텔코드를 화면에 세팅
 * @param res
 */
function fnGetHotelNoCallback(res) {
   if ( ! (res.hotelNoSelected == null || res.hotelNoSelected == "") ) {
      $("select[name=searchHotelNo]").val(res.hotelNoSelected);
   } else {
      $("select[name=searchHotelNo] option:eq(0)").attr("selected", "selected");
      fnSetHotelNo();
   }
}

/**************************************************************************
* 라디오박스 생성2
* parameter      :    sId               // ID
*                sParam   :   gubun   // 구분
*                         codeGrp   // 코드그룹
*                         code   // 코드
*                        lang   // 언어 
*                        value   // 선택한 값
*               sCallback         // 콜백함수
*               sSync            // true, false
*               sStyle            // 스타일
*               sCss            // css
**************************************************************************/
var setRadioBox2 = function(sId, sParams, sCallback, sSync, sStyle, sCss) {
   gId       = sId;
   gParams   = sParams;
   gCallback = sCallback;
   gSync     = sSync;
   gStyle    = sStyle;
   gCss      = sCss;
   
   actionAjaxCall(sSync, "/cmm/cmmCode.do", sParams, "gfn_callback_radiobox2", "json");
   
};

var gfn_callback_radiobox2 = function(data){
   var html = "";
   
   if(data.success == "Y"){
      $.each(data.codeList,function(index, result) {
         html += '<input type="radio" id="' + gId + '_' + index + '" name="' + gId + '"';
         html += ' value="' + result.code + '"';
         if(gStyle != "") html += ' style="' + gStyle + '"';
         if(gCss != "") html += ' class="' + gCss + '"';
         if(gParams.value != ""){
            if(gParams.value == result.code)html += ' checked="checked"';
         }
         html += '>';
         html += '<label for="' + gId + '_' + index + '">' + result.codeNm + '</label>';
      });
   }
   
   html += '</select>';
   $("#"+gId).html(html);
   
   if (gCallback in window) {
        window[gCallback](data);
    }
   
   //초기화
   gId       = "";
   gOption   = "";
   gParams   = "";
   gCallback = "";
   gSync     = "";
   gStyle    = "";
   gCss      = "";
   gChange   = "";
};

/**************************************************************************
* 체크박스 생성2
* parameter      :    sId               // ID
*                sParam   :   gubun   // 구분
*                         codeGrp   // 코드그룹
*                         code   // 코드
*                        lang   // 언어 
*                        value   // 선택한 값
*               sCallback         // 콜백함수
*               sSync            // true, false
*               sStyle            // 스타일
*               sCss            // css
**************************************************************************/
var setCheckBox2 = function(sId, sParams, sCallback, sSync, sStyle, sCss) {
   gId       = sId;
   gParams   = sParams;
   gCallback = sCallback;
   gSync     = sSync;
   gStyle    = sStyle;
   gCss      = sCss;
   
   actionAjaxCall(sSync, "/cmm/cmmCode.do", sParams, "gfn_callback_checkbox2", "json");
   
};

var gfn_callback_checkbox2 = function(data){
   var html = "";
   
   if(data.success == "Y"){
      $.each(data.codeList,function(index, result) {
         html += '<input type="checkbox" id="' + gId + '_' + index + '" name="' + gId + '"';
         html += ' value="' + result.code + '"';
         if(gStyle != "") html += ' style="' + gStyle + '"';
         if(gCss != "") html += ' class="' + gCss + '"';
         if(gParams.value != ""){
            if(gParams.value == result.code)html += ' checked="checked"';
         }
         html += '>';
         html += '<label for="' + gId + '_' + index + '">' + result.codeNm + '</label>';
      });
   }
   
   html += '</select>';
   $("#"+gId).html(html);
   
   if (gCallback in window) {
        window[gCallback](data);
    }
   
   //초기화
   gId       = "";
   gOption   = "";
   gParams   = "";
   gCallback = "";
   gSync     = "";
   gStyle    = "";
   gCss      = "";
   gChange   = "";
};

/**************************************************************************
* 체크박스 생성3
* parameter   :   sId         // ID
*           sParam  : gubun // 구분
*                 codeGrp // 코드그룹
*                 code  // 코드
*               lang  // 언어 
*               value // 선택한 값
*         sCallback     // 콜백함수
*         sSync       // true, false
*         sStyle        // 스타일
*         sCss        // css
**************************************************************************/
var setCheckBox3 = function(sId, sParams, sCallback, sSync, sStyle, sCss) {
  gId       = sId;
  gParams   = sParams;
  gCallback = sCallback;
  gSync     = sSync;
  gStyle    = sStyle;
  gCss      = sCss;
  
  actionAjaxCall(sSync, "/cmm/cmmCode.do", sParams, "gfn_callback_checkbox3", "json");
  
};

var gfn_callback_checkbox3 = function(data){
  var html = "";
  
  if(data.success == "Y"){
    $.each(data.codeList,function(index, result) {
      html += '<input type="checkbox" id="' + gId + '_' + index + '" name="' + gId + '"';
      html += ' value="' + result.code + '"';
      if(gStyle != "") html += ' style="' + gStyle + '"';
      if(gCss != "") html += ' class="' + gCss + '"';
      if(gParams.value != ""){
        if(gParams.value == result.code)html += ' checked="checked"';
      }
      html += '>';
      html += '<label>' + result.codeNm + '</label>';
    });
  }
  
  html += '</select>';
  $("#"+gId).html(html);
  
  if (gCallback in window) {
        window[gCallback](data);
    }
  
  //초기화
  gId       = "";
  gOption   = "";
  gParams   = "";
  gCallback = "";
  gSync     = "";
  gStyle    = "";
  gCss      = "";
  gChange   = "";
};

//숫자만 입력하기
var gfn_checkIhIdNum = function(obj){
  var val = obj.value;
  for (var i=0; i<val.length; i++) {
      if (val.charAt(i)<'0' || val.charAt(i)>'9') {
          alert('숫자만 입력가능합니다.');
          obj.value = '';
          obj.focus();
          return false;
      }
  }
};

var gfn_urlParameterByName = function(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

var gfn_popZipCode = function(zipCode, addr1, addr2){
   if(zipCode.length > 0
      && addr1.length > 0
      && addr2.length > 0
   ){
      param = "?id1="+zipCode+"&id2="+addr1+"&id3="+addr2;
      var returnValue = window.open("/cmm/jusoPopup.do" + param,"pop","width=570,height=420, scrollbars=yes, resizable=yes");
   }
   
};
