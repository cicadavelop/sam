/**********************************************
 * 보도자료 뿌리기
 * @returns
 **********************************************/

	$(document).ready(function(){
		init();
	})
	
/**********************************************
 * 
 * @returns
 **********************************************/
	
$(document).on("click", ".proudct-btn", function() {
	var member_seq = $("input[name=member_seq]").val();
	if(!chkLogin(member_seq)){
		return
	};
})

/*
 * requestApi.js
 * 
 * 2020.04.13 김우철
 * 
 * API 모듈화
 */
function goSearch(){
	init();
}
function init(){
	var frm = document.form;
	var param = {
		"fund_title":frm.fund_title.value
	}
	request("POST", "/api/product", param);	
}
function request(method, url, param) {
	console.log(param)
	const xhr = new XMLHttpRequest();
	var response = null;
	var result;

	xhr.open(method, url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(JSON.stringify(param));
	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;
		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			response = JSON.parse(this.responseText);
			console.log(response.data.length);
			if (response.result.result_code == 200) {
				
				var len = response.data.list.length;
				$(".total-data-cnt").text(len);
				
				var template = $.templates("#selectList"); //<!-- 템플릿 선언 위치 지정 -->
				var htmlOutput = template.render(response.data); //<!-- 렌더링 진행 -->
				$("#invest-list").html(htmlOutput); //<!-- 렌더링 결과 뿌려줌 -->
			}
		} else {
			console.log("Error!");
		}
	};
}

