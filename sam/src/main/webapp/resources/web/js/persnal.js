/**********************************************
 * 회원가입
 * @returns
 **********************************************/
$(document).on("click", ".join-btn", function() {
	var frm = document.joinForm;
	isValid = true;
	
	if (!frm.member_id.value) {
		alert("아이디를 입력해주세요.");
		isValid = false;
		return;
	}
	var idRule = /^[a-z0-9]{5,12}$/g;
	if (!idRule.test(frm.member_id.value)) {
		alert("아이디는 6~12자 영문자 또는 숫자이어야합니다.")
		isValid = false;
		return;
	}
	if (!frm.name.value) {
		alert("이름을 입력해주세요.");
		isValid = false;
		return;
	}
	var nameRule = /^[가-힣]{2,4}$/;
	if (!nameRule.test(frm.name.value)) {
		alert("정확한 이름을 입력해주세요.");
		isValid = false;
		return;
	}
	if(!frm.phone.value) {
		alert("전화번호를 입력해주세요.");
		isValid = false;
		return;
	}
	var phoneRule = /^\d{3}\d{3,4}\d{4}$/;
	if(!phoneRule.test(frm.phone.value)) {
		alert("전화번호 형식이 일치하지 않습니다.");
		isValid = false;
		return;
	}
	if (!frm.reg_num.value) {
		alert("주민번호를 입력해주세요.");
		isValid = false;
		return;
	}
	var reg_numRule = /^(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))[1-4][0-9]{6}$/;
	if (!reg_numRule.test(frm.reg_num.value)) {
		alert("주민번호 형식이 일치하지 않습니다.");
		isValid = false;
		return;
	}
	if (!frm.password.value) {
		alert("비밀번호를 입력해주세요.");
		isValid = false;
		return;
	}
	if (!frm.repassword.value) {
		alert("비밀번호를 재입력해주세요.");
		isValid = false;
		return;
	}
	if (frm.password.value != frm.repassword.value) {
		alert("비밀번호가 일치하지 않습니다.");
		isValid = false;
		return;
	}
	var passRule = /^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z]{8,10}$/;
	if (!passRule.test(frm.password.value)) {
		alert("비밀번호는 8~10자 영문, 숫자 조합으로 입력해야합니다.");
		isValid = false;
		return;
	}
	if (!frm.email.value) {
		alert("이메일을 입력해주세요.");
		isValid = false;
		return;
	}
	var emailRule = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
	if (!emailRule.test(frm.email.value)) {
		alert("올바른 이메일 주소를 입력해주세요.");
		isValid = false;
	}
	if(isValid) {
		var param = {
			"member_id" : frm.member_id.value,
			"password" : frm.password.value,
			"email" : frm.email.value,
			"type" : frm.type.value,
			"name" : frm.name.value,
			"phone" : frm.phone.value,
			"reg_num" : frm.reg_num.value
		}
		request("POST", "/join", param);
	}
})

/*
 * requestApi.js
 * 
 * 2020.04.13 김우철
 * 
 * API 모듈화
 */
function request(method, url, param) {
	const xhr = new XMLHttpRequest();
	var response = null;
	var result;

	xhr.open(method, url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(JSON.stringify(param));

	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			console.log(xhr.responseText);

			response = JSON.parse(this.responseText);
			
			if (response.result.result_code == 200) {
				alert("회원가입이 완료되었습니다.");
				location.href="/";
			} else {
				alert(response.result.result_msg);
			}

			// 커스텀 처리

		} else {
			console.log("Error!");
		}
	};
}