/**********************************************
 * 아이디 찾기
 * @returns
 **********************************************/
$(document).on("click", ".modify-btn", function() {
	var frm = document.infoForm;
	isValid = true;
	if(!frm.phone.value) {
		alert("전화번호를 입력해주세요.");
		isValid = false;
		return;
	}
	var phoneRule = /^\d{3}\d{3,4}\d{4}$/;
	if(!phoneRule.test(frm.phone.value)) {
		alert("전화번호 형식이 일치하지 않습니다.");
		isValid = false;
		return;
	}
	if (!frm.email.value) {
		alert("이메일을 입력해주세요.");
		isValid = false;
		return;
	}
	var emailRule = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
	if (!emailRule.test(frm.email.value)) {
		alert("올바른 이메일 주소를 입력해주세요.");
		isValid = false;
		return;
	}
	var receipt_yn = $("input[name='receipt_yn']:checked").val();
	if (!receipt_yn) {
		alert("현금영수증 발행 여부를 선택해주세요.");
		isValid = false;
		return;
	}
	var email_yn = $("input:checkbox[id='check01']:checked").val();
	if (email_yn == "on") {
		email_yn = "Y";
	} else {
		email_yn = "N";
	}
	var sms_yn = $("input:checkbox[id='check02']:checked").val();
	if (sms_yn == "on") {
		sms_yn = "Y";
	} else {
		sms_yn = "N";
	}
	
	if(isValid) {
		var param = {
			"member_seq" : frm.member_seq.value,
			"phone" : frm.phone.value,
			"email" : frm.email.value,
			"receipt_yn" : receipt_yn,
			"email_yn" : email_yn,
			"sms_yn" : sms_yn
		}
		request("POST", "/myinfo/update", param);
	}
})


/*
 * requestApi.js
 * 
 * 2020.04.13 김우철
 * 
 * API 모듈화
 */
function request(method, url, param) {
	const xhr = new XMLHttpRequest();
	var response = null;
	var result;

	xhr.open(method, url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(JSON.stringify(param));

	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			console.log(xhr.responseText);

			response = JSON.parse(this.responseText);

			if (response.result.result_code == 200) {
				alert("회원정보가 변경되었습니다.")
				location.href="/member/modify";
			} else {
				alert(response.result.result_msg);
			}

			// 커스텀 처리

		} else {
			console.log("Error!");
		}
	};
}