$(function(){
	$(document).on("click", ".submit-btn", function(){
		var frm = document.form;
		
		if(!$('input:radio[name=loan_type]').is(':checked')){
			alert("담보를 선택해주세요.");
			$('input:radio[name=loan_type]').focus();
			return;
		}
		
		if(!frm.collateral_content.value){
			alert("담보내용을 입력해주세요.");
			frm.collateral_content.focus();
			return;
		}
		
		if(!frm.name.value){
			alert("이름을 입력해주세요.");
			frm.name.focus();
			return;
		}
		
		if(!frm.phone.value){
			alert("휴대폰 번호를 입력해주세요.");
			frm.phone.focus();
			return;
		}
		
		if(!frm.reg_num.value){
			alert("생년월일을 입력해주세요.");
			frm.reg_num.focus();
			return;
		}
		
		if(frm.reg_num.value.length != 8){
			alert("생년월일을 형식에 맞게 입력해주세요.");
			frm.reg_num.focus();
			return;
		}
		
		if(!$('input:radio[name=gender]').is(':checked')){
			alert("성별을 선택해주세요.");
			$('input:radio[name=gender]').focus();
			return;
		}
		
		if(!frm.hope_loan.value){
			alert("희망대출액을 입력해주세요.");
			frm.hope_loan.focus();
			return;
		}
		
		if(!frm.hope_loan_period.value){
			alert("대출희망기간 선택해주세요.");
			frm.hope_loan_period.focus();
			return;
		}
		
		var param = {
			"loan_type":frm.loan_type.value,
			"collateral_content":frm.collateral_content.value,
			"name":frm.name.value,
			"phone":frm.phone.value,
			"reg_num":frm.reg_num.value,
			"gender":frm.gender.value,
			"hope_loan":frm.hope_loan.value,
			"hope_loan_period":frm.hope_loan_period.value,
			"etc_info":frm.etc_info.value
		}
		
		requestPost("/api/loan/create", param, function(res){
			if(res.success){
				alert("신청이 완료되었습니다.");
				location.href="";
			}
		},function(e){
			alert("에러 : " + e);
		})
		
		
	})
})
