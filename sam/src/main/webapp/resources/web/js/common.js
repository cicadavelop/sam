/**********************************************
 * 로그인
 * @returns
 **********************************************/
$(document).on("click", ".logout-btn", function() {
	request("POST", "/logout", null);
})

/*
 * requestApi.js
 * 
 * 2020.04.13 김우철
 * 
 * API 모듈화
 */
function request(method, url, param) {
	const xhr = new XMLHttpRequest();
	var response = null;
	var result;

	xhr.open(method, url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(JSON.stringify(param));
	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			console.log(xhr.responseText);

			response = JSON.parse(this.responseText);

			if (response.result.result_code == 200) {
				location.href = "/";
			} else {
				alert("로그아웃 실패");
			}

			// 커스텀 처리

		} else {
			console.log("Error!");
		}
	};
}

function requestGet(url, callbackSuccess, callbackFail) {
	console.log("=========================================================")
	console.log("endPoint : " + url);
	
	const xhr = new XMLHttpRequest();
	var res = null;
	var result;

	xhr.open("GET", url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			res = JSON.parse(this.responseText);

			console.log(res);
    		console.log("=========================================================")
			callbackSuccess(res);

		} else {
			console.log("Error!");
    		console.log("=========================================================")
			callbackFail(xhr.status);
		}
	};
}


function requestPost(url, param,  callbackSuccess, callbackFail) {
	console.log("=========================================================")
	console.log("endPoint : " + url);
	console.log(param);
	
	const xhr = new XMLHttpRequest();
	var res = null;
	var result;

	xhr.open("POST", url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(JSON.stringify(param));
	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			res = JSON.parse(this.responseText);

			console.log(res);
    		console.log("=========================================================")
			callbackSuccess(res);

		} else {
			console.log("Error!");
    		console.log("=========================================================")
			callbackFail(xhr.status);
		}
	};
}

/**
 * 로그인 체크 공통함수
 * @param member_seq
 * @returns
 */
function chkLogin(member_seq){
	if(!member_seq){
		alert("로그인한 사용자만 신청 가능합니다.");
		if(confirm("로그인 페이지로 이동하시겠습니까?")){
			location.href="/member/login"
		}
		return false;
	}
}