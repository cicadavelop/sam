$(document).ready(function() {
  $('.left-warp .side-close').click(function(){
      if($('.left-warp').hasClass('on')){
          left_menu_onoff();
      }
  });

  $('.right-warp .side-close').click(function(){
      if($('.right-warp').hasClass('on')){
          right_menu_onoff();
      }
  });

  var mMenuBtn = $(".depth01 > a");
    mMenuBtn.click(function(){
      $(this).parent().siblings().removeClass("active");
      if($(this).parent().hasClass("active")){
        $(this).parent().removeClass("active");
      }else{
        $(this).parent().addClass("active");
      }
    });



  var winW = $(window).width();
  var winH = $(window).height();
  





  $('.top-nav > ul > li').mouseenter(function(){
    $(this).addClass("on");
    $(this).find('.sub-wrap').addClass("active");
    $(this).find(".sub-right-img").css({"visibility":"visible","opacity":"1"});
  });
  $('.top-nav > ul > li').mouseleave(function(){
    $(this).removeClass("on");
    $(this).find('.sub-wrap').removeClass("active");
    $(this).find(".sub-right-img").css({"visibility":"hidden","opacity":"0"});
  });

  $('.top-util a').mouseenter(function(){
    $(this).addClass("on");
    $(this).find('.sub-wrap').addClass("active");
    $(this).find(".sub-right-img").css({"visibility":"visible","opacity":"1"});
  });
  $('.top-util a').mouseleave(function(){
    $(this).removeClass("on");
    $(this).find('.sub-wrap').removeClass("active");
    $(this).find(".sub-right-img").css({"visibility":"hidden","opacity":"0"});
  });

  $(".top-nav .depth2").fadeOut();
    $('.depth1 > li').mouseenter(function(){
    $(this).find('.depth2').show();
    $(".sub-right-img").css({"visibility":"hidden","opacity":"0"});
  });
    $('.depth1 > li').mouseleave(function(){
    $(this).find('.depth2').hide();
    $(".sub-right-img").css({"visibility":"visible","opacity":"1"});
  });

  $('.sub-wrap').mouseleave(function(){
    $(".sub-right-img").css({"visibility":"hidden","opacity":"0"});
  });


  $(".move-click").click(function(e){
        e.preventDefault();
        $( 'html, body' ).stop().animate( { scrollTop : $("#move").offset().top } )
   });





  $("#content > div").hide(); // Initially hide all content
  $("#tabs li:first").attr("class","current"); // Activate first tab
  $("#content > div:first").fadeIn(); // Show first tab content

  $('#tabs a').click(function(e) {
    e.preventDefault();
    if ($(this).closest("li").attr("class") == "current"){ //detection for current tab
      return      
    }
    else{            
      $("#content > div").hide(); //Hide all content
      $("#tabs li").attr("class",""); //Reset id's
      $(this).parent().attr("class","current"); // Activate this
      $('#' + $(this).attr('name')).fadeIn(); // Show content for current tab
    }
  });

  $(".q-line").click(function() {
    $(this).parent().find(".a-line").slideToggle(function() {
      $(this).parent().find('.q-line').css({"background":"url(images/board-icon.svg)",'background-repeat' : 'no-repeat', 'background-position':'98% center', 'background-size':'25px 25px'});
      if($(this).css("display") == "none") {
        $(this).parent().find('.q-line').css({"background":"url(images/board-icon.svg)",'background-repeat' : 'no-repeat', 'background-position':'98% center', 'background-size':'25px 25px'});
      }
    });
  });

  $(".tab-content a.more").click(function() {
    if($('ul.faq-content1').is(":visible")){
      $(".faq-wrap .more span").text(2);
      $("ul.faq-content2").slideDown(function() {
        if($('ul.faq-content2').is(":visible")){
          $(".tab-content a.more").click(function() {
            $(".faq-wrap .more span").text(3);
            $("ul.faq-content3").slideDown();
          });
        }
      });
    }
  });


  $(".invest-item").each(function(num){
   var drink=$(this);
   setTimeout(function(){
     drink.animate({ opacity: "1"}, 200, "linear");
    }, num * 150);
  });

  $(".progress-bar-active").each(function(num){
   var drink=$(this);
   setTimeout(function(){
     drink.animate({ width: "100%"}, 400, "swing");
    }, num * 150);
  });



    $(".cert-item a").click(function() {
      $(this).parent().find(".cert-popup").css("visibility","visible");
    });

    $(".cert-popup .close, .cert-popup .pop-bg").click(function() {
      $(this).parent().css("visibility","hidden");
    });

    $(".item-first .animate").addClass("animateon");








  var check = $(".fund-setting input[type='checkbox']");
  check.click(function(){
    $(".fund-setting p").toggle();
  });


    

});


$(window).resize(function() {
    var winW = $(window).width();
    var winH = $(window).height();
    if(winW > 1024) {
      $('.left-warp').hide();
      $('.right-warp').hide();
      if($('.left-warp').hasClass('on')){
        left_menu_onoff();
      }
      if($('.right-warp').hasClass('on')){
        right_menu_onoff();
      }
    } else if(winW > 768) {
      if($('.left-warp').hasClass('on')){
        left_menu_onoff();
      }
      if($('.right-warp').hasClass('on')){
        right_menu_onoff();
      }
      $('.black-bg').fadeOut();
    } else if(winW < 768) {
    }
});

$(window).scroll(function() {
  var subTop = $("#topmenu").outerHeight() + $(".video-top").outerHeight() + $(".sub-top-title h2").outerHeight() - 80;
  if($(this).scrollTop() > subTop) {
    $(".sub-top-menu").addClass("fixmenu");
  } else {
    $(".sub-top-menu").removeClass("fixmenu");
  }

  $(".slideon").each(function(){
    var pos = $(this).offset().top;
    var winTop = $(window).scrollTop();
    if (pos < winTop + 800) {
      $(this).addClass("slideup");
    }
  });

  $(".animate").each(function(){
    var pos = $(this).offset().top;
    var winTop = $(window).scrollTop();
    if (pos < winTop + 800) {
      $(this).addClass("animateon");
    }
  });
});


function left_menu_onoff(){
    $(".left-warp").stop().animate({width:'toggle'},350);
    if($('.left-warp').hasClass('on')){
        $("#wrap").stop().animate({left:'0'},350,function(){
         $("html, body").removeClass("not-scroll")
        });
        $(".left-warp").stop().animate({left:'-110%'},350);
        $(".left-warp .side-close").stop().fadeOut(350);
        $(".black-bg").stop().fadeOut(350);
        $('.left-warp').removeClass('on');
    }else{
        $(".left-warp").stop().animate({left:'0',width:'300px'},350);
        $(".left-warp .side-close").stop().fadeIn(350);
        $(".black-bg").stop().fadeIn(350);
        $('.left-warp').addClass('on');
        $("html, body").addClass("not-scroll")
    }
}
function right_menu_onoff(){
    $(".right-warp").stop().animate({width:'toggle'},350);
    if($('.right-warp').hasClass('on')){
        $("#wrap").stop().animate({right:'0'},350,function(){
         $("html, body").removeClass("not-scroll")
        });
        $(".right-warp").stop().animate({right:'-110%'},350);
        $(".right-warp .side-close").stop().fadeOut(350);
        $(".black-bg").stop().fadeOut(350);
        $('.right-warp').removeClass('on');
    }else{
        $(".right-warp").stop().animate({right:'0',width:'300px'},350);
        $(".right-warp .side-close").stop().fadeIn(350);
        $(".black-bg").stop().fadeIn(350);
        $('.right-warp').addClass('on');
        $("html, body").addClass("not-scroll")
    }
}

