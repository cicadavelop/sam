$(document).ready(function() {
  var slideWrapper = $(".main-slider"),
      slideSwipe = $(".main-swiper"),
      iframes = slideWrapper.find('.embed-player'),
      lazyImages = slideWrapper.find('.slide-image'),
      lazyCounter = 0;

      slideWrapper.slick({
        autoplaySpeed:4000,
        lazyLoad:"progressive",
        speed:400,
        autoplay: false,
        arrows:true,
        dots:true,
        cssEase: 'linear',
        easing: 'easeInOutBack',
      });

      slideSwipe.slick({
        autoplaySpeed:4000,
        lazyLoad:"progressive",
        speed:400,
        autoplay: false,
        arrows:true,
        dots:false,
        cssEase: 'linear',
        easing: 'easeInOutBack',
      });

      $(".main-slide-counter").slick({
        autoplaySpeed:5000,
        lazyLoad:"progressive",
        speed:400,
        autoplay: true,
        arrows:false,
        dots:true,
        cssEase: 'linear',
        easing: 'easeInOutBack',
        fade:true,
      });
      $('#pause').click(function() {
        $('.main-slide-counter').slick('slickPause');
        $(this).find('img').attr("src","images/run.svg");
      });

  var countNum= 281085430000;
  $({ val : 0 }).animate({ val : countNum }, {
    duration: 2000,
    step: function() {
      var num = numberWithCommas(Math.floor(this.val));
      $("#main-count01").text(num);
    },
    complete: function() {
      var num = numberWithCommas(Math.floor(this.val));
      $("#main-count01").text(num);
    }
  });

var countNum2= 42166623738;
  $({ val : 0 }).animate({ val : countNum2 }, {
    duration: 2000,
    step: function() {
      var num = numberWithCommas(Math.floor(this.val));
      $("#main-count02").text(num);
    },
    complete: function() {
      var num = numberWithCommas(Math.floor(this.val));
      $("#main-count02").text(num);
    }
  });

  

  function numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }


  slider = $('.mainVisual').bxSlider({
    auto:true,
    autoControls: true,
    pager:true,
    controls:false,
    speed:0,
    mode:'fade',
    onSliderLoad:function(currentIndex){

      var bxcounter1= 7772;
      $({ val : 0 }).animate({ val : bxcounter1 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter01").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter01").text(num);
        }
      });

      var bxcounter2 = 31;
      $({ val : 0 }).animate({ val : bxcounter2 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter02").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter02").text(num);
        }
      });

      var bxcounter3 = 11;
      $({ val : 0 }).animate({ val : bxcounter3 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter03").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter03").text(num);
        }
      });

      var bxcounter31 = 96;
      $({ val : 0 }).animate({ val : bxcounter31 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter03-1").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter03-1").text(num);
        }
      });


      var bxcounter4 = 1356270;
      $({ val : 0 }).animate({ val : bxcounter4 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter04").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter04").text(num);
        }
      });

    },
    onSlideBefore:function($slideElement, oldIndex, newIndex){

      var bxcounter1= 7772;
      $({ val : 0 }).animate({ val : bxcounter1 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter01").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter01").text(num);
        }
      });

      var bxcounter2 = 31;
      $({ val : 0 }).animate({ val : bxcounter2 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter02").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter02").text(num);
        }
      });

      var bxcounter3 = 11;
      $({ val : 0 }).animate({ val : bxcounter3 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter03").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter03").text(num);
        }
      });

      var bxcounter31 = 96;
      $({ val : 0 }).animate({ val : bxcounter31 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter03-1").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter03-1").text(num);
        }
      });

      var bxcounter4 = 1356270;
      $({ val : 0 }).animate({ val : bxcounter4 }, {
        duration: 2000,
        step: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter04").text(num);
        },
        complete: function() {
          var num = numberWithCommas(Math.floor(this.val));
          $("#bxcounter04").text(num);
        }
      });

    }
  });


  $("#content div.tab-content").hide(); 
    $("#tabs li:first").attr("id","current"); 
    $("#content div.tab-content:first").show(); 
   
    $('#tabs a').click(function(e) {
        e.preventDefault();
        if ($(this).closest("li").attr("id") == "current"){ 

         return      
        }
        else{            
        $("#content div.tab-content").hide(); 
        $("#tabs li").attr("id",""); 
        $(this).parent().attr("id","current"); 
        $('#' + $(this).attr('name')).show(); 
        }
    });

      

});



$(window).resize(function() {
});

