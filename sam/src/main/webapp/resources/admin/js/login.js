/**********************************************
 * 로그인
 * @returns
 **********************************************/
$(document).on("click", ".au-btn--block", function() {
	var frm = document.loginForm;

	if (!frm.email.value || !frm.password.value) {
		func.inputErrorBorder();
		return;
	}

	var param = {
		"email" : frm.email.value,
		"password" : frm.password.value
	}
	request("POST", "/supervise/login", param);

})

/*
 * requestApi.js
 * 
 * 2020.04.13 김우철
 * 
 * API 모듈화
 */
function request(method, url, param) {
	const xhr = new XMLHttpRequest();
	var response = null;
	var result;

	xhr.open(method, url);
	xhr.setRequestHeader('Content-type', 'application/json');
	xhr.send(JSON.stringify(param));

	xhr.onreadystatechange = function(e) {
		if (xhr.readyState !== XMLHttpRequest.DONE)
			return;

		if (xhr.status === 200) { // 200: OK => https://httpstatuses.com
			console.log(xhr.responseText);

			response = JSON.parse(this.responseText);

			if (response.result.result_code == 200) {
				location.href = "/supervise/";
			} else {
				alert("로그인 실패");
			}

			// 커스텀 처리

		} else {
			console.log("Error!");
		}
	};
}


/*************************************************************
* 자동 로그인 (쿠키)
**************************************************************/

$(document).ready(function(){
	 
    // 저장된 쿠키값을 가져와서 ID 칸에 넣어준다. 없으면 공백으로 들어감.
    var key = getCookie("key");
    $("#member_id").val(key); 
     
    if($("#member_id").val() != ""){ // 그 전에 ID를 저장해서 처음 페이지 로딩 시, 입력 칸에 저장된 ID가 표시된 상태라면,
        $("#idSaveCheck").attr("checked", true); // ID 저장하기를 체크 상태로 두기.
    }
     
    $("#idSaveCheck").change(function(){ // 체크박스에 변화가 있다면,
        if($("#idSaveCheck").is(":checked")){ // ID 저장하기 체크했을 때,
            setCookie("key", $("#member_id").val(), 7); // 7일 동안 쿠키 보관
        }else{ // ID 저장하기 체크 해제 시,
            deleteCookie("key");
        }
    });
     
    // ID 저장하기를 체크한 상태에서 ID를 입력하는 경우, 이럴 때도 쿠키 저장.
    $("#member_id").keyup(function(){ // ID 입력 칸에 ID를 입력할 때,
        if($("#idSaveCheck").is(":checked")){ // ID 저장하기를 체크한 상태라면,
            setCookie("key", $("#member_id").val(), 7); // 7일 동안 쿠키 보관
        }
    });
});
 
function setCookie(cookieName, value, exdays){
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
    document.cookie = cookieName + "=" + cookieValue;
}
 
function deleteCookie(cookieName){
    var expireDate = new Date();
    expireDate.setDate(expireDate.getDate() - 1);
    document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
}
 
function getCookie(cookieName) {
    cookieName = cookieName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cookieName);
    var cookieValue = '';
    if(start != -1){
        start += cookieName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cookieValue = cookieData.substring(start, end);
    }
    return unescape(cookieValue);
}