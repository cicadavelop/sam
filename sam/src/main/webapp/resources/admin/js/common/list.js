 $(document).ready(function() {
  var table = $('#commonTable').DataTable({
         dom: 'B<"clear">lfrtip',
         buttons: [ 'copy', 
             {
                 extend: 'csv',
                 title: TITLE+'목록'
             },
             {
                 extend: 'excel',
                 title: TITLE+'목록'
             },
             {
                 extend: 'pdf',
                 title: TITLE+'목록'
             },
             {
                 extend: 'print',
                 title: TITLE+'목록'
             }
         ],
         bAutoWidth : true,
         "order": [
             [0, 'desc']
         ],
		 bLengthChange:true,
		 "lengthMenu": [ [10, 25, 50, 75, 100, -1 ], [10, 25, 50, 75, 100, "All" ] ],
		 ordering: true,
		 processing: true,
		 searching: true,
      	"displayLength": 50,
     	"drawCallback": function(settings) {
          var api = this.api();
          var rows = api.rows({
              page: 'current'
          }).nodes();
          var last = null;
      	}
  });
  
  	$(".key-set").click(function(){
  		self.location = "/supervise/"+KEY+"/"+$(this).data("key");
    });
      
});