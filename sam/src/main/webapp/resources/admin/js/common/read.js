$(document).ready(function() {
      	$("#remove").on("click", function() {
      		if(confirm("삭제하시겠습니까?")){
	      		var seq = $(this).data("seq");
	      		$.ajax({
	      			type : 'GET',
	      			url : '/supervise/show/remove/'+TABLENM+'/'+TABLESEQ+'/' + seq,
	      			headers : {
	      				"Content-Type" : "application/json",
	      				"X-HTTP-Method-Override" : "DELETE"
	      			},
	      			dataType : 'text',
	      			success : function(result) {
	      				if (result>0) {
	      					alert("삭제 되었습니다.");
	      					self.location = "/supervise/"+KEY;
	      				}
	      			}
	      		});
      		}
      	}); 
});