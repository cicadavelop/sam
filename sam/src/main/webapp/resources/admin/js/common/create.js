/*폼 벨리데이션*/
	 
	 var form_validation = function() {
	 
	    var e = function() {
	            jQuery(".form-valide").validate({
	                ignore: [],
	                errorClass: "invalid-feedback animated fadeInDown",
	                errorElement: "div",
	                errorPlacement: function(e, a) {
	                    jQuery(a).parents(".form-group > div").append(e)
	                },
	                highlight: function(e) {
	                    jQuery(e).closest(".form-group").removeClass("is-invalid").addClass("is-invalid")
	                },
	                success: function(e) {
	                    jQuery(e).closest(".form-group").removeClass("is-invalid"), jQuery(e).remove()
	                },
	                rules: rules,
	                messages: messages
	            })
	        }
	        return {
	            init: function() {
	                e(), null, jQuery(".js-select2").on("change", function() {
	                    jQuery(this).valid()
	                })
	            }
	        }
	       
	}();
	jQuery(function() {
	    form_validation.init()
	}); 