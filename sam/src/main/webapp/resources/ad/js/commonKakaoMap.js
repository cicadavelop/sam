
//주소-좌표 변환 객체를 생성합니다
var geocoder = new kakao.maps.services.Geocoder();

//지도를 클릭했을 때 클릭 위치 좌표에 대한 주소정보를 표시하도록 이벤트를 등록합니다
function getAddress(lat, lng, callback){
	var na = {
		getLng:function(){
			return lng;
		},
		getLat:function(){
			return lat;
		}
		
	}
	searchDetailAddrFromCoords(na, function(result, status) {
	    if (status === kakao.maps.services.Status.OK) {
	        var detailAddr = !!result[0].road_address ? result[0].road_address.address_name: result[0].address.address_name;
	        callback(detailAddr);
	    }else{
	    	callback('');
	    }
	});
}
function searchDetailAddrFromCoords(coords, callback) {
 // 좌표로 법정동 상세 주소 정보를 요청합니다
 geocoder.coord2Address(coords.getLng(), coords.getLat(), callback);
}