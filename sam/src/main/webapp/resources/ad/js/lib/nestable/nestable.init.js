$(document).ready(function()
{
    "use strict";
    var updateOutput = function(e)
    {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        	ajax_change_sort(list.nestable('serialize'));
        
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    
    
    
    $('#nestable4').nestable({
    }).on('change', updateOutput);

// output initial serialised data
    updateOutput($('#nestable2').data('output', $('#nestable2-output')));
    updateOutput($('#nestable3').data('output', $('#nestable3-output')));
    updateOutput($('#nestable4').data('output', $('#nestable4-output')));
 

    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });
});

$loding_time ="N";
function ajax_change_sort(list){
	
	if($loding_time === 'Y'){
		
		var objs = new Array();
		console.log(list)
		var x= 0;
		var before_facility_seq = 0;
		for (var i = 0; i < list.length; i++) {
			var obj = new Object();
			if(list[i].seq != '0' && list[i].seq != ''){
				
				if(list[i].type == 'facility'){
					obj.sort=x+1;
					obj.facility_seq=list[i].seq; before_facility_seq = list[i].seq;
					obj.seq=0;
					
					
					x++;
				}else if(list[i].type == 'touch'){
					obj.sort=0;
					obj.facility_seq=before_facility_seq;
					obj.seq=list[i].seq;
				}
				obj.type=list[i].type;
				obj.course_seq=$course_seq;
				objs.push(obj);
				console.log(obj)
			}
		}
		
		var url = "/supervise/course/change/facility";
		var data = JSON.stringify(objs);
		if(list.length == 0){
			url = "/supervise/course/delete/facility";
			data = $course_seq;
		}
		
		
		$.ajax({
			type:"POST",  
			url:url,
			data : "list="+data,
			dataType : "text",
			success:function(args) {
				if(args != 'Y'){
					alert("시설물 순서 변경 중 에러가 발생하였습니다.")
				}
			},
			error: function(request, status, error){
				console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
			}
		});
		
	}
}