/**
 * ---------------------------------------
 * This demo was created using amCharts 4.
 * 
 * For more information visit:
 * https://www.amcharts.com/
 * 
 * Documentation is available at:
 * https://www.amcharts.com/docs/v4/
 * ---------------------------------------
 */

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("user-condition", am4charts.XYChart);

for(var i = 0 ; i < "${user_list_count}" ; i++){
	console.log(i);
	var item = {
						"date":"${user_list[i].tt}",
						"당월가입자": "${user_list[i].month_count}", "누적가입자" : "${user_list[i].total_count}", "누적준회원":"${user_list[i].total_auth_count}", "누적정회원":"${user_list[i].total_auth2_count}"
					}
	chart.data.push(item);
}

// Add data
/*
chart.data = [
{
  "date": "USA",
  "visits": 2025, "test" : 510, "test2":1000
}, {
  "date": "China",
  "visits": 1882, "test" : 510, "test2":1000
}, {
  "date": "Japan",
  "visits": 1809, "test" : 510, "test2":1000
}, {
  "date": "Germany",
  "visits": 1322, "test" : 510, "test2":1000
}, {
  "date": "UK",
  "visits": 1122, "test" : 510, "test2":1000
}, {
  "date": "France",
  "visits": 1114, "test" : 510, "test2":1000
}, {
  "date": "India",
  "visits": 984, "test" : 510, "test2":1000
}, {
  "date": "Spain",
  "visits": 711, "test" : 510, "test2":1000
}, {
  "date": "Netherlands",
  "visits": 665, "test" : 510, "test2":1000
}, {
  "date": "Russia",
  "visits": 580, "test" : 510, "test2":1000
}, {
  "date": "South Korea",
  "visits": 443, "test" : 510, "test2":1000
}, {
  "date": "Canada",
  "visits": 441, "test" : 510, "test2":1000
}, {
  "date": "Brazil",
  "visits": 395, "test" : 510, "test2":1000
}];
*/
// Create axes

var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "date";
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.renderer.minGridDistance = 30;



var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
var series = chart.series.push(new am4charts.ColumnSeries());
series.dataFields.valueY = "visits";
series.dataFields.categoryX = "date";
series.name = "Visits";
series.columns.template.tooltipText = "정회원: [bold]{valueY}[/]";
series.columns.template.fillOpacity = .8;

var series2 = chart.series.push(new am4charts.ColumnSeries());
series2.dataFields.valueY = "test";
series2.dataFields.categoryX = "date";
series2.name = "test";
series2.columns.template.tooltipText = "준회원: [bold]{valueY}[/]";
series2.columns.template.fillOpacity = .8;

var series3 = chart.series.push(new am4charts.ColumnSeries());
series3.dataFields.valueY = "test2";
series3.dataFields.categoryX = "date";
series3.name = "test2";
series3.columns.template.tooltipText = "인증대기회원: [bold]{valueY}[/]";
series3.columns.template.fillOpacity = .8;

var columnTemplate = series.columns.template;
columnTemplate.strokeWidth = 1;
columnTemplate.strokeOpacity = 1;

var columnTemplate2 = series2.columns.template;
columnTemplate2.strokeWidth = 1;
columnTemplate2.strokeOpacity = 1;

var columnTemplate3 = series3.columns.template;
columnTemplate3.strokeWidth = 1;
columnTemplate3.strokeOpacity = 1;

// Add legend 하단의 누를시에 해당 시리즈를 보이게 하는 버튼
chart.legend = new am4charts.Legend();
chart.legend.position = "bottom";

chart.cursor= new am4charts.XYCursor();

var scrollbarX = new am4charts.XYChartScrollbar();
scrollbarX.series.push(series);
scrollbarX.series.push(series2);
scrollbarX.series.push(series3);
chart.scrollbarX = scrollbarX;
//해당 시리즈를 숨기고 하단의 카테고리 네임들로만 보이도록 설정
//scrollbarX.scrollbarChart.seriesContainer.hide();