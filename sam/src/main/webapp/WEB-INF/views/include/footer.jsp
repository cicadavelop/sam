<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

		<div id="footer">
			<div class="inner">
				<div class="footer-menu">
					<div class="footer-item">
						<div class="util">
							<a href="privacy1.jsp">이용약관</a>
							<a href="privacy2.jsp">개인정보 처리 방침</a>
							<a href="privacy3.jsp">윤리강령</a>
							<a href="#">사업자정보</a>
						</div>
					</div>
					<div class="footer-item">
						<p class="footer-cs"><span>고객센터</span>평일 10시 ~ 18시 (점심 12시 ~ 13시)</p>
						<div>
							<p>전화 <span>02-477-3780</span>팩스 <span>02-6092-0030</span></p>
							<p>대표메일 <span>samivm@naver.com</span>사업제휴 <span>samivm@naver.com</span></p>
						</div>
					</div>
					<div class="footer-item">
						<ul class="fr">
							<li><a href="#"><img src="/resources/images/footer-icon01.png" alt=""></a></li>
							<li><a href="#"><img src="/resources/images/footer-icon02.png" alt=""></a></li>
							<li><a href="#"><img src="/resources/images/footer-icon03.png" alt=""></a></li>
							<li><a href="#"><img src="/resources/images/footer-icon04.png" alt=""></a></li>
						</ul>
					</div>
				</div>
			
				<div class="footer-box">
					<div>
						<span class="footer-company">(주)샘인베스트먼트 | 대표 유서우</span>
						<span>사업자번호 123-12-12345 | 통신판매업 신고준비중 | 서울특별시 중구 마른내로 136 서림빌딩 2층, 201호</span>
						<p>샘인베스트먼트는 투자원금과 수익을 보장하지 않으며, 투자손실에 대한 책임은 모두 투자자에게 있습니다.</p>
					</div>
					<div>
						<span class="footer-company">샘인베스트먼트(주) | 대표 유서우</span>
						<span>(주)샘인베스트먼트 | 대표 유서우        사업자번호 123-12-12345 | P2P연계대부업 2020-금감원-1130 | 서울특별시 중구 마른내로 136 서림빌딩 2층, 201호 | 등록기관 금융감독원(1332) 등록 <a href="#">대부(중개)업체 통합조회 서비스 바로가기 &gt;</a></span>
						<p>대출금리 연 23.9% 이내 (연체금리 최고 연 24% 이내) 대출 실행시 취급수수료 및 기타 부대비용이 발생할 수 있습니다. 중도상환은 전액상환만 가능하며 계약조건에 따라 중도상환금액의 3.0% 이내의 수수료가 부과되거나 면제될 수 있습니다. 과도한 빚은 당신에게 큰 불행을 안겨줄 수 있습니다. 대출 시 귀하의 신용등급이 하락할 수 있습니다. 중개수수료를 요구하거나 받는 행위는 불법입니다.</p>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>





<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="/resources/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/resources/js/slick.min.js"></script>
<script src="/resources/js/main.js"></script>
<script src="/resources/js/script.js"></script>
<script src="/resources/js/jquery.bxslider.min.js"></script>
<script src="/resources/web/js/common.js"></script>