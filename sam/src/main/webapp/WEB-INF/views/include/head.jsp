<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!--[if lt IE 9]> 
			<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script> 
		<![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
		<link type="text/css" rel="stylesheet" href="/resources/css/reset.css" media="screen" />
		<link type="text/css" rel="stylesheet" href="/resources/css/style.css" media="screen" />
		<link type="text/css" rel="stylesheet" href="/resources/css/flexslider.css" media="screen" />
		<link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Noto+Sans+KR:100,300,400,500,700,900&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="/resources/css/slick.css">
		<link rel="stylesheet" href="/resources/css/swiper.css">
		<script src="https://use.fontawesome.com/624b0b42f7.js"></script>
		<title>샘인베스트먼트</title>
	</head> 