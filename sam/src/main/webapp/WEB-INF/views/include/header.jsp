<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div id="mTopmenu" class="m-topmenu">
	<h1>
		<a href="/"><img src="/resources/images/logo.png" title="" alt="로고"></a>
	</h1>
	<div class="mypage">
		<a href="javascript:right_menu_onoff();"><i class="fa fa-user-o" aria-hidden="true"></i></a>
	</div>
	<div class="menu">
		<a href="javascript:left_menu_onoff();"> <span></span> <span></span> <span></span>
		</a>
	</div>
</div>


<div class="left-warp">
	<div class="left-wrap-top">
		<h1>
			<a href="/"><img src="/resources/images/logo.png" title="" alt="로고"></a>
		</h1>
		<div class="side-close">
			<a href="#none"> <span></span> <span></span>
			</a>
		</div>
	</div>
	<div class="scroll-func" style="height: 92%; overflow-y: auto;">
		<div class="depth">
			<ul>
				<li class="depth01"><a href="#none">투자하기</a> <span class="more">&gt;</span>
					<ul class="depth02">
						<li><a href="/invest/dispersion">자동분산투자</a></li>
						<li><a href="/invest/product">투자상품 보기</a></li>
					</ul></li>
				<li class="depth01"><a href="#none">대출하기</a> <span class="more">&gt;</span>
					<ul class="depth02">
						<li><a href="/loan/individual">개인신용대출</a></li>
						<li><a href="/loan/property">부동산담보대출</a></li>
						<li><a href="/loan/artificial">법인대출</a></li>
					</ul></li>
				<li class="depth01"><a href="/terms/guide">이용안내</a></li>
				<li class="depth01"><a href="/terms/release">보도자료</a></li>
				<li class="depth01"><a href="/terms/company">회사소개</a></li>
				<li class="depth01"><a href="/terms/contact">CONTACT US</a></li>
				<li class="depth01"><a href="/terms/faq">FAQ</a></li>
			</ul>
		</div>
	</div>
</div>

<div class="right-warp">
	<div class="right-wrap-top">
		<div class="side-close">
			<a href="#none"> <span></span> <span></span>
			</a>
		</div>
	</div>
	<div class="scroll-func" style="height: 95%; overflow-y: auto;">
		<c:choose>
			<c:when test="${empty sessionScope.MemberEntity.member_seq }">
				<div class="right-login" style="display: block;">
					<div class="right-menu">
						<a href="/member/login">로그인</a> <a href="/member/join">회원가입</a>
						<!--<a href="mypage.jsp">마이페이지</a>-->
						<!--<a href="#none">로그아웃</a>-->
					</div>
					<p>
						로그인 하신 뒤,<br> 샘인베스트먼트를 이용해보세요!
					</p>
				</div>
			</c:when>
			<c:otherwise>
				<div class="right-logoff">
					<div class="right-menu">
						<div class="fl">
							<strong>${sessionScope.MemberEntity.name }</strong>님
						</div>
						<div class="fr logout-btn">
							<a href="#">로그아웃</a>
						</div>
					</div>
					<div class="right-account">
						<div class="fl">
							<div class="fl">
								<strong>나의 계좌</strong>
							</div>
							<div class="fr">
								<a href="/member/myaccount"> <span>0원</span> <i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>
								<p>(신한은행 5621-409-333300)</p>
							</div>
						</div>
						<div class="fr">
							<a href="point.jsp">
								<div class="fl">
									<strong>나의 포인트</strong>
								</div>
								<div class="fr">
									<span>5,000 P</span> <i class="fa fa-angle-right" aria-hidden="true"></i>
								</div>
							</a>
						</div>
						<ul>
							<li><a href="/member/mypage">대시보드</a></li>
							<li><a href="/member/investhst">투자내역</a></li>
							<li><a href="/member/transaction">거래내역</a></li>
						</ul>
					</div>
				</div>
				<div class="right-icon">
					<a href="/invest/product"><img src="/resources/images/mypage-icon01.png">투자하기</a> 
					<a href="/invest/dispersion"><img src="/resources/images/mypage-icon02.png">자동투자</a> 
					<a href="/loan/individual"><img src="/resources/images/mypage-icon03.png">대출받기</a>
					<a href="/invest/product"><img src="/resources/images/mypage-icon04.png">투자상품 보기</a>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</div>


<div class="black-bg"></div>

<div id="wrap">
	<!-- 상단메뉴 -->
	<div id="topmenu">
		<div class="inner">
			<h1>
				<a href="/"><img src="/resources/images/logo.png" title="" alt="로고"></a>
			</h1>

			<div class="top-nav">
				<ul class="top-nav-l fl">
					<li><a href="#none" class="top-nav-title">투자하기</a>
						<div class="sub-wrap">
							<ul class="depth1">
								<li><a href="/invest/dispersion">자동분산투자</a></li>
								<li><a href="/invest/product">투자상품 보기</a></li>
							</ul>
						</div></li>
					<li><a href="#none" class="top-nav-title">대출하기</a>
						<div class="sub-wrap">
							<ul class="depth1">
								<li><a href="/loan/individual">개인신용대출</a></li>
								<li><a href="/loan/property">부동산담보대출</a></li>
								<li><a href="/loan/artificial">법인대출</a></li>
							</ul>
						</div></li>
				</ul>
				<c:choose>
					<c:when test="${empty sessionScope.MemberEntity.member_seq }">
						<ul class="top-nav-l fr">
							<div class="top-util fr">
								<a href="/member/login" class="white-btn">로그인</a> <a href="/member/join" class="white-btn">회원가입</a>
							</div>
						</ul>
					</c:when>
					<c:otherwise>
						<ul class="top-nav-l fr">
							<li class="util-wrap"><a href="javascript:void(0)" class="white-btn top-nav-title">${sessionScope.MemberEntity.member_id }님</a>
								<div class="sub-wrap">
									<ul class="top-util">
										<li>
											<div class="top-info">
												<div>
													<strong>${sessionScope.MemberEntity.name }</strong>님
												</div>
												<p>${sessionScope.MemberEntity.member_id }</p>
												<a href="#" class="white-btn logout-btn">로그아웃</a>
											</div>
											<div class="dash-line top-modify">
												<a href="/member/mypage">나의 투자현황 보기</a> <a href="/member/modify">회원정보 수정</a>
											</div>
											<div class="top-info-account">
												<div>
													<div class="fl">
														<strong>나의 계좌</strong>
													</div>
													<div class="fr">
														<a href="/member/myaccount"> <span>0원</span> <i class="fa fa-angle-right" aria-hidden="true"></i>
														</a>
													</div>
												</div>
												<p>(신한은행 5621-409-333300)</p>
												<div>
													<div class="fl">
														<strong>나의 포인트</strong>
													</div>
													<div class="fr">
														<a href="/member/mypoint"> <span>5,000 P</span> <i class="fa fa-angle-right" aria-hidden="true"></i>
														</a>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div></li>
						</ul>
					</c:otherwise>
				</c:choose>
				<div class="top-nav-r fr">
					<ul class="fl">
						<li><a href="/terms/guide" class="top-nav-title">이용안내</a></li>
						<li><a href="/terms/release" class="top-nav-title">보도자료</a></li>
						<li><a href="/terms/company" class="top-nav-title">회사소개</a></li>
						<li><a href="/terms/contact" class="top-nav-title">CONTACT US</a></li>
						<li><a href="/terms/faq" class="top-nav-title">FAQ</a></li>
					</ul>

				</div>
			</div>
		</div>
	</div>
	<!-- // 상단메뉴 -->