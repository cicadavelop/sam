<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
					<div class="side clear">
						<div class="mypage-info">
							<div class="mypage-name"><strong><a href="/member/modify">홍길동</a></strong>님</div>
							<p class="mypage-id">abcdedf123</p>
							<div class="mypage-point">나의 포인트<span>5,000P</span></div>
						</div>
						<div class="sub-mymenu">
							<ul>
								<li class="on"><a href="/member/mypage">대시보드</a></li>
								<li><a href="/member/investhst">투자내역</a></li>
								<li><a href="/member/myaccount">나의계좌</a></li>
								<li><a href="/member/transaction">거래내역</a></li>
								<li><a href="/member/point">포인트</a></li>
							</ul>
						</div>
					</div>