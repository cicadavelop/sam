<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="inner privacy">
        

					<h1>윤리 강령</h1>
					<p>샘인베스트먼트는 아래와 같이 윤리 강령을 선포합니다.</p>
					<ol>
					<li>고객의 소중한 자산을 별도 계정으로 분류하여 투명하게 운용합니다.</li>
					<li>고객의 자금을 임의로 지인 및 관계사에 대출하지 않습니다. *</li>
					<li>고유의 Risk Scoring System을 통해 대출채권을 객관적이고 공정하게 평가합니다.</li>
					<li>고객으로부터 금품, 접대 및 향응을 제공받지 않으며 부당한 이익을 제공하지 않습니다. </li>
					<li>업무상 취득한 정보를 활용하여 고객의 투자 기회를 선점하지 않습니다. **</li>
					<li>공시하는 투자 정보의 정확성에 대해 선량한 관리자의 의무를 다합니다. </li>
					<li>고객의 개인정보를 보호합니다.</li>
					</ol>
					<p>* 대출 신청자가 당사의 관계사 혹은 임직원의 지인일 경우, 투자 공시를 통해 지인/계열사 여부를 별도 표시하고 관련 정보를 고객님들께 사전 고지하여 투자자들의 투자 판단에 충분히 고려될 수 있도록 하겠습니다.</p>
					<p>** 샘인베스트먼트 임직원의 채권투자를 투자오픈 후 6시간 동안 제한하도록 하겠습니다. 이는 고객님들께 좋은 상품에 투자할 수 있는 기회를 먼저 드리기 위함이며 임직원의 투자 선점을 제한하기 위함입니다.</p>
					<p>안녕하십니까.<br>
					샘인베스트먼트 대표 유서우입니다. <br>
					
					
					<p>샘인베스트먼트는 금산분리법의 취지에 공감합니다. 금산분리의 목적은 은행 사유화를 막고 예금자의 소중한 돈을 보호하기 위함입니다. 이것은 너무나도 타당합니다. 하지만 이런 규제때문에 IT기업과 금융산업이 밀접한 협력관계를 맺을 수 없다면, 대한민국은 현재 글로벌금융시장에 지각변동을 일으키고 있는 핀테크 분야에서 해외 기업들에게 시장을 빼앗겨 버리는 우를 범할 것이기에 보완책을 찾아야 할 것입니다.</p>
					<p>샘인베스트먼트는 금산분리법의 취지에 부합하고 고객님들의 권익을 보호하기 위해 아래와 같이 윤리강령을 제정하였고 컴플라이언스팀 또한 2020년 3월 5일부터 가동할 예정입니다.</p>
					<p>컴플라이언스 팀에서는 윤리적 문제에 대한 고객님들의 의견을 상시 접수하오니, 많은 의견 주시기 바랍니다.</p>
					<p>사람과 금융의 가치를 만드는 샘인베스트먼트 입니다. 윤리적이고 합리적인 기업으로 거듭나겠습니다. <br>
					감사합니다.</p>
					<p>샘인베스트먼트 임직원 드림</p>
					</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>