<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container mypage-wrap clear">
				<div class="inner">
					<%@include file="../include/mymenu.jsp" %>
					<div class="sub-content">
						<div class="invest-state clear">
							<div class="invest-button fl">
								<button class="on">전체 [0]</button>
								<button>상환중 [0]</button>
								<button>상환지연 [0]</button>
								<button>상환연체 [0]</button>
								<button>상환완료 [0]</button>
							</div>
							<div class="invest-select">
								<select>
									<option>전체 [0]</option>
									<option>상환중 [0]</option>
									<option>상환지연 [0]</option>
									<option>상환연체 [0]</option>
									<option>상환완료 [0]</option>
								</select>
							</div>
							<div class="invest-button02 fr">
								<button class="on">최신순</button>
								<button>채권순</button>
							</div>
						</div>
						<div class="invest-total">
							<span>총 투자금액</span> <strong>0원</strong>
						</div>
						<div class="invest-empty">
							조회된 상품이 없습니다.
						</div>
					</div>
					
				</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>