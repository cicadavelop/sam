<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="sub-top-title sub-top05">ABOUT US</div>
				<div class="inner">
	           		<img src="/resources/images/sub05/sub05-img.png" alt="" width="100%">
	           	</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>