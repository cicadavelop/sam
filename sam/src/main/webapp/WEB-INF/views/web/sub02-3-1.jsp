<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="sub-top-title sub-top02-2">법인 대출</div>
				
				<div class="inner">
					<div class="contact-title">
						<ul>
							<li><a href="sub02-3.html">대출신청서 작성</a></li>
							<li class="on"><a href="sub02-3-1.html">대출가능종목리스트</a></li>

						</ul>
					</div>

					<div class="contact-wrap">
						<div class="contact-board board">
							<div class="contact-search">
								<input type="text" name="search" class="input-control">
								<button type="submit" class="black-btn">검색</button>
							</div>
							<p class="contact-search-desc">*등록된 가능종목외에 다른 종목들은 심사를 통하여 가능여부가 결정됩니다.</p>
							<table class="table search-table">
								<colgroup>
									<col width="30%">
									<col width="20%">									 
									<col width="25%">
									<col width="25%">
								</colgroup>
								<tr>
									<th>종목명</th>
									<th>기준가</th>
									<th>대출비율</th>
									<th>상장현황</th>
								</tr>
								<tr>
									<td>KH바텍</td>
									<td>20,700</td>
									<td>20~30%</td>
									<td>코스닥</td>
								</tr>
								<tr>
									<td>제이에스테크</td>
									<td>4,500</td>
									<td>30~40%</td>
									<td>비상장</td>
								</tr>
								<tr>
									<td>피플바이오</td>
									<td>10,000</td>
									<td>30~40%</td>
									<td>비상장</td>
								</tr>
								<tr>
									<td>라이언스</td>
									<td>25,000</td>
									<td>20~30%</td>
									<td>비상장</td>
								</tr>
								<tr>
									<td>엔에스스튜디오</td>
									<td>4,200</td>
									<td>20~30%</td>
									<td>비상장</td>
								</tr>
								<tr>
									<td>노보셀바이오</td>
									<td>2,400</td>
									<td>20~30%</td>
									<td>비상장</td>
								</tr>
								<tr>
									<td>유민에쓰티</td>
									<td>5,000</td>
									<td>40~50%</td>
									<td>비상장</td>
								</tr>
								<tr>
									<td>모든바이오</td>
									<td>2,500</td>
									<td>30~40%</td>
									<td>비상장</td>
								</tr>
								<tr>
									<td>스타네크</td>
									<td>2,000</td>
									<td>10~20%</td>
									<td>비상장</td>
								</tr>
								<tr>
									<td>엔솔바이오사이언스</td>
									<td>7,800</td>
									<td>30~40%</td>
									<td>코넥스</td>
								</tr>
							</table>
							<div class="pagenation">
								<ol>
									<li class="prev"><a href="#">«</a></li>
									<li class="this"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li class="next"><a href="#">»</a></li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>