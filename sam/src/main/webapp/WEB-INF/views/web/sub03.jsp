<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="sub-top-title sub-top03">이용 가이드</div>
				<div class="inner">
					<div class="sub-use">
						<h2>투자가이드</h2>
						<p class="sub-title"><span>샘인베스트먼트 부동산 담보채권</span> 투자 손쉽게 하세요</p>
						<div class="sub-use-wrap">
							<img src="/resources/images/sub03/sub03-main01-1.jpg" alt="">
							<img src="/resources/images/sub03/sub03-main01-2.jpg" alt="">
							<a href="#" class="sub-use-link1">회원가입</a>
							<a href="#" class="sub-use-link2">가상계좌번호 확인</a>
							<a href="sub01-2.html" class="sub-use-link3">투자상품 확인</a>
							<a href="sub01-1.html" class="sub-use-link4">자동투자서비스 신청</a>
						</div>
						<p class="sub-title">샘인베스트먼트 투자 후 ~ <span>상환일까지 편하게 받으세요</span></p>
						<div class="sub-use-wrap">
							<img src="/resources/images/sub03/sub03-main02-1.jpg" alt="">
							<img src="/resources/images/sub03/sub03-main02-2.jpg" alt="">
						</div>
						<div class="sub-use-banner">
							<a href="#">
								<p>부동산 P2P<br>투자 TIP</p>
								<span>바로가기<img src="/resources/images/more-icon.png"></span>
							</a>
							<a href="#">
								<p>샘인베스트먼트<br>투자자 후기</p>
								<span>바로가기<img src="/resources/images/more-icon.png"></span>
							</a>
						</div>
						<p class="sub-title">안심하세요!<br>샘인베스트먼트가 <span>투자금을 안전하게 관리</span>합니다.</p>
						<div class="sub-use-wrap sub-use-wrap03">
							<img src="/resources/images/sub03/sub03-main03.jpg" width="100%" alt="">
						</div>
					</div>
				</div>
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
	
<%@include file="../include/footer.jsp" %>
</body>
</html>