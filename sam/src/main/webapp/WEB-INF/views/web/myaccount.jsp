<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container mypage-wrap clear">
				<div class="inner">
				<%@include file="../include/mymenu.jsp" %>
					<div class="sub-content">
						<div class="myaccount">
							<div class="account-item clear">
								<div class="fl">보유 예치금</div>
								<strong class="fr">0원</strong>
							</div>
							<div class="account-item clear">
								<div class="fl">나의 가상계좌</div>
								<div class="fr">
									<div class="account-input">국민은행</div>
									<div class="account-input">1234-123-123456</div>
									<div class="account-input">홍길동</div>
								</div>
							</div>
							<div class="account-item clear">
								<div class="fl">나의 출금계좌</div>
								<div class="fr inputWrap disable">
									<select>
										<option>국민은행</option>
										<option>신한은행</option>
										<option>농협</option>
										<option>우리은행</option>
									</select>
									<input type="text" class="inputNum" readonly="readonly" placeholder="'-'를 제외한 계좌번호를 입력해주세요.">
									<input type="text" class="inputName" readonly="readonly" placeholder="홍길동"><a href="javascript:void(0)" class="change-btn">변경</a>
								</div>
							</div>
							<div class="account-item clear">
								<div class="fl">출금신청금액</div>
								<div class="fr">
									<div class="output01"><input type="text" id="outputcom" placeholder="0" onkeyup="numberCommas(this.value)"><span>원</span></div>
									<div class="output02"><button>전액</button></div>
									<div class="output03"><button>출금신청</button></div>
								</div>
							</div>
						</div>
						<div class="account-guide">
							<p>※ 고객님의 가상계좌로 예치금 입금 후 투자신청이 가능합니다.</p>
							<p class="red">※ 가상계좌 예치금은 입금시간 기준으로 24시간 동안 출금이 제한됩니다.</p>
							<p>※ 가상계좌 예치금은 고객님의 출금계좌로 환급 받으실 수 있습니다.</p>
							<p>※ 출금신청금액은 신청 후 1분 이내에 고객님의 출금계좌로 이체됩니다.<span class="red">(단, 은행 전산망 점검시간(23:25~00:35)에는 출금 불가)</span></p>
						</div>
					</div>
					
				</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>