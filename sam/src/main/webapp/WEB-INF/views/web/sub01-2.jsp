<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp"%>
<body>
	<%@include file="../include/header.jsp"%>

	<!-- 본문 -->
	<section id="container" class="sub-container">
		<input type="hidden" name="member_seq" value="${sessionScope.MemberEntity.member_seq}">
		<div class="sub-top-title sub-top01-2" style="margin-bottom: 0;">투자상품 보기</div>
		<div class="invest-top-search">
			<div class="inner">
				<form name="form">
					<div class="i-search clear">
						<div class="w85 fl">
							<input type="text" placeholder="투자상품 검색어 입력" name="fund_title" /><a href="javascript:goSearch()"><i class="fa fa-search" aria-hidden="true"></i></a>
						</div>
						<div class="w15 fr">
							<a href="javascript:goSearch()">상세검색<i class="fa fa-plus" aria-hidden="true"></i></a>
						</div>
					</div>
				</form>
				<div class="i-search-item-wrap pc-i">
					<div class="i-search-item">
						<div class="i-check-list">
							<strong class="fl">상품유형</strong>
							<div class="fl">
								<div class="check-input">
									<input type="checkbox" name="iSearch01" id="iSearch01" value="부동산담보"> <label for="iSearch01"><span>부동산담보</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch01" id="iSearch02" value="상품개조"> <label for="iSearch02"><span>상품개조</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch01" id="iSearch03" value="기타"> <label for="iSearch03"><span>기타</span></label>
								</div>
							</div>
						</div>
					</div>
					<div class="i-search-item">
						<div class="i-check-list">
							<strong class="fl">채권순위</strong>
							<div class="fl">
								<div class="check-input">
									<input type="checkbox" name="iSearch02" id="iSearch01-1" value="1순위"> <label for="iSearch01-1"><span>1순위</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch02" id="iSearch01-2" value="2순위"> <label for="iSearch01-2"><span>2순위</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch02" id="iSearch01-3" value="3순위 이하"> <label for="iSearch01-3"><span>3순위 이하</span></label>
								</div>
							</div>
						</div>
					</div>
					<div class="i-search-item i-search-item3">
						<div class="i-check-list">
							<strong class="fl">채권상태</strong>
							<div class="fl">
								<div class="check-input">
									<input type="checkbox" name="iSearch03" id="iSearch02-1" value="대기중"> <label for="iSearch02-1"><span>대기중</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch03" id="iSearch02-2" value="모집중"> <label for="iSearch02-2"><span>모집중</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch03" id="iSearch02-3" value="모집완료"> <label for="iSearch02-3"><span>모집완료</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch03" id="iSearch02-4" value="상환중"> <label for="iSearch02-4"><span>상환중</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch03" id="iSearch02-5" value="상환지연"> <label for="iSearch02-5"><span>상환지연</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch03" id="iSearch02-6" value="상환연체"> <label for="iSearch02-6"><span>상환연체</span></label>
								</div>
								<div class="check-input">
									<input type="checkbox" name="iSearch03" id="iSearch02-7" value="상환완료"> <label for="iSearch02-7"><span>상환완료</span></label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="mobile-i">
					<select>
						<option>상품유형</option>
						<option>부동산담보</option>
						<option>상품개조</option>
						<option>기타</option>
					</select> <select>
						<option>채권순위</option>
						<option>1순위</option>
						<option>2순위</option>
						<option>3순위 이하</option>
					</select> <select>
						<option>채권상태</option>
						<option>대기중</option>
						<option>모집중</option>
						<option>모집완료</option>
						<option>상환중</option>
						<option>상환지연</option>
						<option>상환연체</option>
						<option>상환완료</option>
					</select>
				</div>
			</div>
		</div>
		<div class="inner">
			<div class="function clear">
				<div class="fl">
					총 <span class="blue total-data-cnt">0</span>건의 상품이 검색되었습니다.
				</div>
				<div class="fr">
					<a href="#"><img src="/resources/images/recent.svg">최신순</a>
				</div>
			</div>
			<div class="sub-invest">
				<!--<h2>샘인베스트먼트 투자상품</h2>-->
				<div class="sub-invest-wrap">
					<div id="invest-list">
						
						
						
						
						
						
					</div>
				</div>
	</section>
	<!-- // 본문 끝 -->


	</div>

<div class="clear"></div>
<%@include file="../include/footer.jsp"%>
<script type="text/x-jsrender" id="selectList">
	{{for list}}
	<div class="invest-item" style="opacity:1;">
		<div class="invest-info fl">
			<div class="invest-img fl">
				<img src="{{:fund_image}}" alt="">
			</div>
			<div class="invest-title fl">
				<p>
					<img src="/resources/images/invest-icon01.png" alt="">
				</p>
				<strong>{{:fund_title}}</strong> <span>/ {{:fund_sub}}</span>
			</div>
		</div>
		<div class="invest-line fl">
			<div class="invest-line-item">
				<span class="invest-num"><i class="fa fa-bar-chart"></i>{{:fund_profit}}</span> <span class="invest-month"><i class="fa fa-calendar-check-o"></i>{{:fund_period}}개월</span>
			</div>
			<div class="invest-line-item">
				<div class="invest-pay fl">
					<span>{{:fund_loan}}</span>만원
				</div>
				<div class="invest-bar fl">
					<div class="progress-bar">
						<div class="progress-bar-active"></div>
					</div>
					<span>100.0%</span>
				</div>
			</div>
			<div class="invest-line-item">
				<a href="#" class="white-btn">상세</a> <a href="#" class="black-btn proudct-btn">대출실행</a>
			</div>
		</div>
	</div>
	{{/for}}
</script>
<script src="/resources/web/js/sub01-2.js"></script>
<script src="https://www.jsviews.com/download/jsrender.js"></script>
</body>
</html>