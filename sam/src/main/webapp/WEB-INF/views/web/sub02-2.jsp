<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="sub-top-title sub-top02-2">부동산 담보대출</div>
				
				<div class="inner">
					<div class="contact-title">
						<ul>
							<li class="on"><a href="sub02-2.html">대출신청서 작성</a></li>
							<!--<li><a href="sub02-2-1.html">대출가능종목리스트</a></li>-->

						</ul>
						<div class="contact-info">
							<div class="fl text-left">
								*아래 정보를 모두 입력해주세요.<br>
								*대출가능금액 조회는 주식명과 주식수를 정확하게 입력해야만 가능합니다.<br>
								*등록된 가능종목외에 다른 종목들은 심사를 통하여 가능여부가 결정됩니다.
							</div>
							<div class="fr"><a href="#">* 대출신청 가이드 보기</a></div>
						</div>
					</div>
					<form name="form">
					<div class="contact-wrap">
						<div class="contact-board board">
							<div class="board-table">
								<table class="table">
									<colgroup>
										<col width="15%">
										<col width="">										 
									</colgroup>
									<tr>
										<th class="text-left">담보</th>
										<td>
											<input type="radio" name="loan_type" value="HOUSE" checked="checked">
											<label for="check01">부동산 담보</label>
											<input type="radio" name="loan_type" value="COMPANY">
											<label for="check02">기업담보</label>
											<input type="radio" name="loan_type" value="OTHER">
											<label for="check03">기타담보</label>
										</td>
									</tr>
									<tr>
										<th class="text-left">담보내용</th>
										<td>
											<div class="input-group">
												<label><input type="text" name="collateral_content" required="required"></label>
												<span class="label-title"></span>
												<a href="#" class="black-btn">대출가능금액 조회</a>
											</div>
										</td>
									</tr>
									<tr>
										<th height="1" style="background-color:#dbdbdb;padding:0;"></th>
										<td height="1" style="background-color:#dbdbdb;padding:0;"></td>
									</tr>
									<tr>
										<th class="text-left">이름</th>
										<td>
											<input type="text" name="name" class="input-control">
										</td>
									</tr>
									<tr>
										<th class="text-left">휴대폰 번호</th>
										<td>
											<input type="number" name="phone" placeholder="예) 01012345678" class="input-control" maxlength="11">
										</td>
									</tr>
									<tr>
										<th class="text-left">생년월일</th>
										<td>
											<input type="number" name="reg_num" placeholder="예) 19810412" class="input-control" maxlength="8">
										</td>
									</tr>
									<tr>
										<th class="text-left">성별</th>
										<td>
											<div class="radio-group">
												<div class="radio-btn">
													<input type="radio" name="gender" checked="checked" value="M" class="radio-input">
													<label class="radio-btn-label" for="radio01-1">남성</label>
												</div>
												<div class="radio-btn">
													<input type="radio" name="gender" value="W" class="radio-input">
													<label class="radio-btn-label" for="radio01-2">여성</label>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th height="1" style="background-color:#dbdbdb;padding:0;"></th>
										<td height="1" style="background-color:#dbdbdb;padding:0;"></td>
									</tr>
									<tr>
										<th class="text-left">희망대출액</th>
										<td>
											<div class="input-group">
												<label><input type="number" name="hope_loan" required="required"></label>
												<span class="label-title">만원</span>
											</div>
										</td>
									</tr>
									<tr>
										<th class="text-left">대출희망기간</th>
										<td>
											<div class="input-group">
												<select name="hope_loan_period">
													<option value="1">1개월</option>
													<option value="2">2개월</option>
													<option value="3">3개월</option>
													<option value="4">4개월</option>
													<option value="5">5개월</option>
													<option value="6">6개월</option>
													<option value="7">7개월</option>
													<option value="8">8개월</option>
													<option value="9">9개월</option>
													<option value="10">10개월</option>
													<option value="11">11개월</option>
													<option value="12">12개월</option>
													<option value="13">13개월</option>
													<option value="14">14개월</option>
													<option value="15">15개월</option>
													<option value="16">16개월</option>
													<option value="17">17개월</option>
													<option value="18">18개월</option>
													<option value="19">19개월</option>
													<option value="20">20개월</option>
													<option value="21">21개월</option>
													<option value="22">22개월</option>
													<option value="23">23개월</option>
													<option value="24">24개월</option>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<th class="text-left">기타사항</th>
										<td>
											<div class="input-group">
												<label><input type="text" name="etc_info" placeholder="가입경로나 추천인 또는 기타 특이사항을 남겨 주세요." required="required"></label>
											</div>
										</td>
									</tr>
								</tbody>
							</table>	
							<div><button type="button" class="black-btn submit-btn">대출상담신청</button></div>	
							<p style="margin-top:25px;color:#555;">* 제출하시면 심사하여 문자 및 전화로 안내할 예정입니다.</p>									
						</div>
					</div>
				</div>
				</form>
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
<script src="/resources/web/js/sub02-2.js"></script>
</body>
</html>