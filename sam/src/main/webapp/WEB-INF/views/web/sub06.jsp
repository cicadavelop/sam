<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="sub-top-title sub-top06">CONTACT US</div>
					<div class="contact">
						<div class="contact-content fl">
							<h2>CONTACT US<span>샘인베스트먼트</span></h2>
							<div class="contact-info">
								<ul>
									<li>
										<span class="title">Address</span>
										<span>서울시 중구 마른내로 136 서림빌딩 2층 201호 샘인베스트먼트</span>
									</li>
									<li class="contact-tel">
										<span class="title">Tel</span>
										<span>02-477-3780</span>
									</li>
									<li class="contact-tel">
										<span class="title">Fax</span>
										<span>02-6092-0030</span>
									</li>
									<li>
										<span class="title">Email</span>
										<span>samivm@naver.com</span>
									</li>
									<li>
										<span class="title">카카오톡</span>
										<span>@샘인베스트먼트</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="contact-map fr">
							<!-- * 카카오맵 - 지도퍼가기 -->
				<!-- 1. 지도 노드 -->
				<div id="daumRoughmapContainer1583052395868" class="root_daum_roughmap root_daum_roughmap_landing"></div>

				<!--
					2. 설치 스크립트
					* 지도 퍼가기 서비스를 2개 이상 넣을 경우, 설치 스크립트는 하나만 삽입합니다.
				-->
				<script charset="UTF-8" class="daum_roughmap_loader_script" src="https://ssl.daumcdn.net/dmaps/map_js_init/roughmapLoader.js"></script>

				<!-- 3. 실행 스크립트 -->
				<script charset="UTF-8">
					new daum.roughmap.Lander({
						"timestamp" : "1583052395868",
						"key" : "xa8s",
						"mapWidth" : "847",
						"mapHeight" : "450"
					}).render();
				</script>
				 
				<!--지도 여기까지-->
						</div>
					</div>
           
				
           


			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>