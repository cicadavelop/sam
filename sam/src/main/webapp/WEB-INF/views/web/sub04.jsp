<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>

			<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="sub-top-title sub-top04"></div>
				<div class="inner">
					<div class="sub-news">
						<div id="news-list">
 							<div class="news-images"><img src="/resources/images/sub-news01.jpg" alt=""></div>
							<div class="news-text">
								<div class="title"></div>
								<p>
									- 에어프라이기, 신세계상품권 등 다양한 상품으로 투자자 이목 집중

									장외주식 전문 P2P기업 샘인베스트먼트이 진행한 ‘’3월 혜택은 Only(利)U‘ 이벤트가 3월21일 종료했다.

									’Only(오직) 샘인베스트먼트 회원에게만 증정하는 利(이)익‘이라는 의미를 담고 있는 이번 이벤트는 기본 당첨인원을 ’222명‘이상으로 설정하였는데 이는 ’3월의 시작‘이라는 부분에 의미를 강조하여 당첨 인원수를 결정한 것이라고샘인베스트먼트측은 설명했다.


									이벤트 혜택은 2,000만원 이상 투자자 1명에게 ’에어프라이기‘를 증정했고 200만원 이상 투자자 ’222명‘에게는 신세계상품권 1만원을 증정했다. 이 중 이벤트 기간 내 투자 최고액은 1.5억원 이상이었다.

									관계자는 “에어프라이기 및 상품권의 경우 일상 생활에서 유용하게 사용할 수 있기에 당첨 혜택에 대한 만족도가 높았던 것으로 예상되며 당첨 인원도 대폭 확대한 덕분에 많은 투자자분들이 참여해주신 것 같다”라고 말했다.

									이에 덧붙여 “매월 이벤트가 진행 될 때마다 관련 조건을 충족해주시는 분들이 증가하고 있다. 카카오채널 및 고객센터를 통해 이벤트 관련 문의도 많이 유입되고 있는 것으로 보아 이벤트에 대한 투자자분들의 관심도 함께 증가하고 있는 것 같다.”라고 설명했다.

									샘인베스트먼트 이벤트에 관련 된 내용은 해당 홈페이지 공지사항을 통해 확인 가능하다.
								</p>
								<div class="news-link">
									<div><img src="/resources/images/sub-news-icon01.png" alt=""></div>
									<div><a href="#">뉴스바로보기 →</a></div>
								</div>
							</div>
						</div>
						<!-- <div class="news-cont">
							<div class="news-images"><img src="/resources/images/sub-news02.jpg" alt=""></div>
							<div class="news-text">
								<div class="title">샘인베스트먼트-이데일리TV, 장외이슈 심층분석 '장외주식 투자설명서'</div>
								<p>
									- 에어프라이기, 신세계상품권 등 다양한 상품으로 투자자 이목 집중

									장외주식 전문 P2P기업 샘인베스트먼트이 진행한 ‘’3월 혜택은 Only(利)U‘ 이벤트가 3월21일 종료했다.

									’Only(오직) 샘인베스트먼트 회원에게만 증정하는 利(이)익‘이라는 의미를 담고 있는 이번 이벤트는 기본 당첨인원을 ’222명‘이상으로 설정하였는데 이는 ’3월의 시작‘이라는 부분에 의미를 강조하여 당첨 인원수를 결정한 것이라고샘인베스트먼트측은 설명했다.


									이벤트 혜택은 2,000만원 이상 투자자 1명에게 ’에어프라이기‘를 증정했고 200만원 이상 투자자 ’222명‘에게는 신세계상품권 1만원을 증정했다. 이 중 이벤트 기간 내 투자 최고액은 1.5억원 이상이었다.

									관계자는 “에어프라이기 및 상품권의 경우 일상 생활에서 유용하게 사용할 수 있기에 당첨 혜택에 대한 만족도가 높았던 것으로 예상되며 당첨 인원도 대폭 확대한 덕분에 많은 투자자분들이 참여해주신 것 같다”라고 말했다.

									이에 덧붙여 “매월 이벤트가 진행 될 때마다 관련 조건을 충족해주시는 분들이 증가하고 있다. 카카오채널 및 고객센터를 통해 이벤트 관련 문의도 많이 유입되고 있는 것으로 보아 이벤트에 대한 투자자분들의 관심도 함께 증가하고 있는 것 같다.”라고 설명했다.

									샘인베스트먼트 이벤트에 관련 된 내용은 해당 홈페이지 공지사항을 통해 확인 가능하다.
								</p>
								<div class="news-link">
									<div><img src="/resources/images/sub-news-icon01.png" alt=""></div>
									<div><a href="#">뉴스바로보기 →</a></div>
								</div>
							</div>
						</div> -->
						<div class="pagenation">
							<ol>
								<li class="prev"><a href="#">«</a></li>
								<li class="this"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li class="next"><a href="#">»</a></li>
							</ol>
						</div>
					</div>					
				</div>
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
	<%@include file="../include/footer.jsp" %>
	<script type="text/x-jsrender" id="selectList">
{{for report}}
	<div class="news-cont">
		<div class="news-images"><img src="{{:report_image}}" alt=""></div>
		<div class="news-text">
			<div class="title">{{:report_title}}</div>
			<p>
			{{:report_content}}
			</p>
			<div class="news-link">
				<div><img src="" alt=""></div>
				<div><a href="{{:report_url}}" target="_blank">뉴스바로보기 →</a></div>
			</div>
		</div>
	</div>		
{{/for}}
	</script>
	<script src="/resources/web/js/Report.js"></script>
	<script src="https://www.jsviews.com/download/jsrender.js"></script>
</body>
</html>