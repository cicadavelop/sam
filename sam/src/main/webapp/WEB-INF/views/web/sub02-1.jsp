<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="sub-top-title sub-top02-1">개인 신용대출</div>
				<div class="sub02-1">
					<div class="inner">
						<img src="/resources/images/sub02-1/sub02-1_main.jpg" alt="" width="100%" style="max-width: 932px;">
						<a href="#" class="sub02-link1">한도조회 및 대출신청</a>
						<a href="#" class="sub02-link2">주택담보대출 신청하기</a>
					</div>
				</div>
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
	
<%@include file="../include/footer.jsp" %>
</body>
</html>