<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container mypage-wrap clear">
				<div class="inner">
					<%@include file="../include/mymenu.jsp" %>
					<div class="sub-content">
						<div class="t-top point">
							<div class="row point-row">
								<div class="cell">
									<strong>나의 포인트</strong>
									<div>5,000P</div>
								</div>
								<div class="cell">
									<strong>이번 달 소멸예정 포인트</strong>
									<div>0P</div>
									<span class="q-icon">?</span>
									<div class="q-cont">
										<strong class="title">이번 달 소멸예정 포인트</strong>
										<p class="desc">포인트는 적립일로부터 12개월 경과시 매월 5일 자동 소멸됩니다. (예, 2020년 4월 1일~30일 적립 포인트는 2021년 5월 5일에 일괄 소멸)</p>
									</div>
								</div>
							</div>
							<div class="row t-top-view">
								<div class="cell">
									<strong class="fl">조회기간</strong>
									<div class="fl">
										<div class="date-wrap p-date-wrap">
											<i class="fa fa-calendar" aria-hidden="true"></i>
											<span><input type="text" class="config-demo01 form-control" value="2020-04-01"></span>
											<em>~</em>
											<span><input type="text" class="config-demo02 form-control" value="2020-04-30"></span>
										</div>
										<div class="date-wrap m-date-wrap">
											<span><input type="text" class="config-demo01 form-control" value="2020-04-01"><i class="fa fa-calendar" aria-hidden="true"></i></span>
											<em>~</em>
											<span><input type="text" class="config-demo02 form-control" value="2020-04-30"><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</div>
									</div>
								</div>
								<div class="cell">
									<div class="t-button">
										<button class="on">1개월</button>
										<button>3개월</button>
										<button>6개월</button>
										<button>1년</button>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="cell">
									<strong class="fl">조회내용</strong>
									<div class="fl point-check">
										<div class="check-input">
											<input type="checkbox" id="checkall">
											<label for="checkall"><span>전체</span></label>
										</div>
										<div class="check-input">
											<input type="checkbox" name="chk" id="label01">
											<label for="label01"><span>입금</span></label>
										</div>
										<div class="check-input">
											<input type="checkbox" name="chk" id="label02">
											<label for="label02"><span>출금</span></label>
										</div>
										<div class="check-input">
											<input type="checkbox" name="chk" id="label03">
											<label for="label03"><span>소멸</span></label>
										</div>
									</div>
								</div>
								<div class="cell">
									<a href="#" class="t-view">조회하기</a>
								</div>
							</div>
						</div>
						<table cellspacing="0" cellpadding="0" border="0" class="t-table t-table-pc">
							<colgroup>
								<col width="13%"/>
								<col width="12%"/>
								<col width="*"/>
								<col width="15%"/>
								<col width="15%"/>
							</colgroup>
							<tr>
								<th>날짜</th>
								<th>구분</th>
								<th>내역</th>
								<th>포인트</th>
								<th>잔여포인트</th>
							</tr>
							<tr>
								<td>2020.03.19</td>
								<td>적립</td>
								<td align="left">투자자등록</td>
								<td align="right" class="blue">+5,000</td>
								<td align="right">5,000</td>
							</tr>
							<tr>
								<td>2020.03.19</td>
								<td>적립</td>
								<td align="left">투자자등록</td>
								<td align="right" class="blue">+5,000</td>
								<td align="right">5,000</td>
							</tr>
						</table>


						<table cellspacing="0" cellpadding="0" border="0" class="t-table t-table-mobile">
							<colgroup>
								<col width="13%"/>
								<col width="12%"/>
								<col width="*"/>
								<col width="15%"/>
								<col width="15%"/>
							</colgroup>
							<tr>
								<td align="left">2020.03.19</td>
								<td align="right" class="blue"><strong>+5,000</strong></td>
							</tr>
							<tr>
								<td align="left"><strong>적립</strong></td>
								<td align="right">잔여포인트 5,000</td>
							</tr>
						</table>

						<table cellspacing="0" cellpadding="0" border="0" class="t-table t-table-mobile">
							<colgroup>
								<col width="13%"/>
								<col width="12%"/>
								<col width="*"/>
								<col width="15%"/>
								<col width="15%"/>
							</colgroup>
							<tr>
								<td align="left">2020.03.19</td>
								<td align="right" class="blue"><strong>+5,000</strong></td>
							</tr>
							<tr>
								<td align="left"><strong>적립</strong></td>
								<td align="right">잔여포인트 5,000</td>
							</tr>
						</table>

						<div class="pagenation p-pagenation">
							<ol>
								<li class="prev"><a href="#none"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
								<li class="this"><a href="#none">1</a></li>
								<li><a href="#none">2</a></li>
								<li class="next"><a href="#none"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
							</ol>
						</div>
					</div>
				</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>