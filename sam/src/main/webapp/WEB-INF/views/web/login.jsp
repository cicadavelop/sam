<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="inner">
					<div class="sub-member">
						<div class="member-wrap">
						<form name="loginForm" id="loginForm">
							<h2>로그인</h2>
							<div class="input-wrap">
								<input type="text" name="member_id" id="member_id" placeholder="아이디" />
								<input type="password" name="password" placeholder="비밀번호" />
							</div>
							<div class="check-input"><input type="checkbox" id="idSaveCheck"/><label for="idSaveCheck"><span>아이디 저장</span></label></div>
							<div class="login-btn"><input type="button" value="로그인"></div>
							<div class="member-btn">
								<p class="fl"><a href="/member/join">회원가입</a></p>
								<p class="fr"><a href="/member/search">아이디/비밀번호 찾기</a></p>
							</div>
							<div class="facebook-login"><a href="#">FACEBOOK 로그인</a></div>
						</form>
						</div>
					</div>
				</div>
				
			</section>
			<!-- // 본문 끝 -->
		</div>
		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
<script src="/resources/web/js/login.js"></script>
</body>
</html>