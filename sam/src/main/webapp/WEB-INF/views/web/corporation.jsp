<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="inner">
					<div class="sub-member">
						<div class="member-wrap">
							<h2>법인 회원가입</h2>
							<div class="join-wrap">
								<div class="join-form">
									<form name="joinForm">
										<div class="join-form-item">
											<span>아이디</span>
											<div><input type="text" name="member_id" placeholder="아이디를 입력하세요" class="input-line"/></div>
										</div>
										<div class="join-form-item">
											<span>이름</span>
											<div><input type="text" name="name" placeholder="이름을 입력하세요." class="input-line"/></div>
										</div>
										<div class="join-form-item">
											<span>휴대전화</span>
											<div><input type="text" name="phone" placeholder="아이디를 입력하세요" class="input-line"/></div>
										</div>
										<div class="join-form-item">
											<span>주민등록번호</span>
											<div><input type="password" name="reg_num" placeholder="주민번호를 입력하세요" class="input-line"/></div>
										</div>
										<div class="join-form-item">
											<span>비밀번호</span>
											<div><input type="password" name="password" placeholder="비밀번호를 입력하세요" class="input-line"/></div>
										</div>
										<div class="join-form-item">
											<span>비밀번호 확인</span>
											<div><input type="password" name="repassword" placeholder="비밀번호를 한번 더 입력하세요" class="input-line"/></div>
										</div>
										<div class="join-form-item">
											<span>담당자 이메일</span>
											<div><input type="text" name="email" placeholder="이메일을 입력하세요" class="input-line"/></div>
										</div>
										<input type="hidden" name="type" value="1">
										<div class="join-btn"><input type="button" value="가입하기"></div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
		
<%@include file="../include/footer.jsp" %>
<script src="/resources/web/js/persnal.js"></script>
</body>
</html>