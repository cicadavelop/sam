<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container">
				<div class="main-slider">
					<div class="main-slider-item main-slider-item01">
						<div class="main-slider-img" style="background:url('/resources/imagesmain-slider-bg01.png')no-repeat center"></div>
						<div class="main-slider-text-wrap">
							<div class="main-slider-text">
								<p><span>개인신용</span>투자 가능 상품</p>
								<div class="number"><strong>125</strong>개</div>
								<ul>
									<li>대환대출 90</li>
									<li>개인자금 35</li>
									<li>직장인소액대출 97건</li>
								</ul>
								<a href="sub01-2.html" class="more">투자상품 더 보기</a>
								<div class="main-slider-thumb"><img src="/resources/imagesmain-slider01.png" alt=""></div>
							</div>
						</div>
					</div>
					<div class="main-slider-item main-slider-item02">
						<div class="main-slider-img" style="background:url('/resources/imagesmain-slider-bg02.png')no-repeat center"></div>
						<div class="main-slider-text-wrap">
							<div class="main-slider-text">
								<h2>아트박스 에어팟 2세대 필름제조</h2>
								<div class="number"><strong>100.00</strong>%</div>
								<ul>
									<li><span>A+</span>3개월 / 7,600만원</li>
								</ul>

								<a href="sub01-2.html" class="more">상품 보러가기</a>

								<div class="main-slider-thumb"><img src="/resources/imagesmain-slider02.png" alt=""></div>
							</div>
						</div>
					</div>
					<div class="main-slider-item main-slider-item03">
						<div class="main-slider-img" style="background:url('/resources/imagesmain-slider-bg03.png')no-repeat center"></div>
						<div class="main-slider-text-wrap">
							<div class="main-slider-text">
								<h2>아트박스 립밤 캐릭터 피규어 제조</h2>
								<div class="number"><strong>100.00</strong>%</div>
								<ul>
									<li><span>A+</span>7개월 / 3,000만원</li>
								</ul>

								<a href="sub01-2.html" class="more">상품 보러가기</a>

								<div class="main-slider-thumb"><img src="/resources/imagesmain-slider03.png" alt=""></div>
							</div>
						</div>
					</div>
					<div class="main-slider-item main-slider-item04">
						<div class="main-slider-img" style="background:url('/resources/imagesmain-slider-bg04.png')no-repeat center"></div>
						<div class="main-slider-text-wrap">
							<div class="main-slider-text">
								<h2>에이케어 스포츠 센터 관리앱</h2>
								<div class="number"><strong>70.00</strong>%</div>
								<ul>
									<li><span>A+</span>12개월 / 5,000만원</li>
								</ul>

								<a href="sub01-2.html" class="more">상품 보러가기</a>

								<div class="main-slider-thumb"><img src="/resources/imagesmain-slider04.png" alt=""></div>
							</div>
						</div>
					</div>
					<div class="main-slider-item main-slider-item01 main-slider-item05">
						<div class="main-slider-img" style="background:url('/resources/imagesmain-slider-bg05.jpg')no-repeat center"></div>
						<div class="main-slider-text-wrap">
							<div class="main-slider-text">
								<p><span>자동분산투자</span></p>
								<div class="number">나의 투자유형에 맞춰<br>
									<strong>자동으로 분산투자</strong> 하세요.</div>
								<ul>
									<li>895명의 투자자가 이용중입니다.</li>
								</ul>
								<a href="sub01-1.html" class="more">더 알아보기</a>
								<div class="main-slider-thumb"><img src="/resources/imagesmain-slider05.png" alt=""></div>
							</div>
						</div>
					</div>
				</div>


				<div class="main-counter-banner">
					<!--<div class="inner">
						<div class="counter-item01 counter-item">
							<p>평균 연금리</p>
							<div class="main-count">17.3<span>%</span></div>
						</div>
						<div class="counter-item02 counter-item">
							<p>누적 대출액</p>
							<div class="main-count"><span id="main-count01" class="count-number"></span><span>원</span></div>
							<span>2020년 03월 01일 기준</span>
						</div>
						<div class="counter-item03 counter-item">
							<p>대출잔액</p>
							<div class="main-count"><span id="main-count02" class="count-number"></span><span>원</span></div>
						</div>
						<div class="counter-item04 counter-item">
							<p>연체율</p>
							<div class="main-count">2.1<span>%</span></div>
						</div>
						<div class="counter-item05 counter-item">
							<a href="#">담보유지라인 확보로 안전한 <span>투자하기</span><img src="/resources/imagesmain-counter01.png" alt=""></a>
							<a href="#">금융기관대비 합리적인 대출 <span>신청하기</span><img src="/resources/imagesmain-counter02.png" alt=""></a>
						</div>
					</div>-->
				</div>

				<div class="main-fund">
					<div class="main-fund-top"><img src="/resources/imagesfund-top-img.png" alt=""></div>
					<div class="inner">
						<div class="fund-item">
							<div class="fund-title fl slideon">
								<span class="fund-num"><img src="/resources/imagesfund-num01.png" alt=""></span>
								<strong>믿을 수 있는 샘인베스트먼트</strong>
								<p>2019년 오픈 이후 누적대출액 10억 돌파<br>이미 1천여명의 대출자가 선택한 샘인베스트먼트</p>
							</div>
							<div class="fund-img slideon"><img src="/resources/imagesfund-img01.png" alt=""></div>
						</div>
						<div class="fund-item">
							<div class="fund-title fl slideon">
								<span class="fund-num"><img src="/resources/imagesfund-num02.png" alt=""></span>
								<strong>신속한투자진행 </strong>
								<p>ONE STOP 솔루션</p>
							</div>
							<div class="fund-img slideon"><img src="/resources/imagesfund-img02.png" alt=""></div>
						</div>
						<div class="fund-item">
							<div class="fund-title fl slideon">
								<span class="fund-num"><img src="/resources/imagesfund-num03.png" alt=""></span>
								<strong>높은금리,안정적서비스</strong>
								<p>LTV 최대 80%까지</p>
							</div>
							<div class="fund-img slideon"><img src="/resources/imagesfund-img03.png" alt=""></div>
						</div>
						<div class="fund-item">
							<div class="fund-title fl slideon">
								<span class="fund-num"><img src="/resources/imagesfund-num04.png" alt=""></span>
								<strong>저렴한 투자수수료!</strong>
								<p>샘인베스트먼트는 투자수수료를 저렴하게 서비스합니다!</p>
							</div>
							<div class="fund-img slideon"><img src="/resources/imagesfund-img04.png" alt=""></div>
						</div>
					</div>
				</div>


				<div class="main-swiper">
					<div class="main-swiper-item main-swiper-item01"><a href="#"><img src="/resources/imagesmain-swiper01.png" alt=""></a></div>
					<div class="main-swiper-item main-swiper-item02"><a href="#"><img src="/resources/imagesmain-swiper02.png" alt=""></a></div>
					<div class="main-swiper-item main-swiper-item03"><a href="#"><img src="/resources/imagesmain-swiper03.png" alt=""></a></div>
					<div class="main-swiper-item main-swiper-item04"><a href="#"><img src="/resources/imagesmain-swiper04.png" alt=""></a></div>
					<div class="main-swiper-item main-swiper-item05"><a href="#"><img src="/resources/imagesmain-swiper05.png" alt=""></a></div>
				</div>


				<div class="clear"></div>


				<div class="main-tab">
					<div class="inner">
						<h2>샘인베스트먼트 스토리</h2>
						<ul id="tabs">
							<li><a href="#" name="tab1">투자TIP</a></li>
							<li><a href="#" name="tab2">투자후기</a></li>
							<li><a href="#" name="tab3">언론보도</a></li>
							<li><a href="#" name="tab4">공지사항</a></li>
						</ul>

						<div id="content">
							<div id="tab1" class="tab-content">
								<ul>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img01-01.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img01-02.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img01-03.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
								</ul>
							</div>
							<div id="tab2" class="tab-content">
								<ul>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img02-01.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img02-02.jpg" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img02-03.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img02-04.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img02-05.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img02-06.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
								</ul>
							</div>
							<div id="tab3" class="tab-content">
								<ul>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img03-01.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img03-02.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img03-03.jpg" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
								</ul>
							</div>
							<div id="tab4" class="tab-content">
								<ul>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img04.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img04.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
									<li>
										<a href="#">
											<div class="thumb"><img src="/resources/imagestab-img04.png" alt=""></div>
											<div class="info">
												<div class="title">
													<span class="subject fl">투자TIP</span>
													<span class="date fr">2020.02.26</span>
												</div>
												<div class="desc">[이벤트] 3월 첫투자 이벤트</div>
											</div>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				
				<!--<div class="main-slide-counter-wrap">
					<div class="inner">
						<div id="mainVisual">
							<ul class="mainVisual">
								<li>
									<div class="bxWrap">
										<strong>누적상환액<span>2020년 1월 30일 기준</span></strong>
										<div class="bx-num"><span id="bxcounter01"></span><span>억</span></div>
										<div class="disclosure-wrap">
											<div class="disclosure">
												<span class="disclosure-title">전월대비</span>
												<span class="disclosure-gray">(억)</span>
												<span class="disclosure-up">▲</span>
												<span class="disclosure-up-blue">+206</span>
											</div>
											<div class="disclosure disclosure2">
												<span class="disclosure-title">등락률</span>
												<span class="disclosure-gray">(%)</span>
												<span class="disclosure-up">▲</span>
												<span class="disclosure-up-blue">+1.98</span>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="bxWrap">
										<strong>원금손실률<span>2020년 1월 30일 기준</span></strong>
										<div class="bx-num">0.<span id="bxcounter02"></span><span>%</span></div>
										<div class="disclosure-wrap">
											<div class="disclosure">
												<span class="disclosure-title">전월대비</span>
												<span class="disclosure-gray">(억)</span>
												-
											</div>
											<div class="disclosure disclosure2">
												<span class="disclosure-title">등락률</span>
												<span class="disclosure-gray">(%)</span>
												-
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="bxWrap">
										<strong>평균수익률<span>2020년 1월 30일 기준</span></strong>
										<div class="bx-num"><span id="bxcounter03"></span>.<span id="bxcounter03-1"></span><span>%</span></div>
										<div class="disclosure-wrap">
											<div class="disclosure">
												<span class="disclosure-title">전월대비</span>
												<span class="disclosure-gray">(억)</span>
												<span class="disclosure-down">▼</span>
												<span class="disclosure-down-blue">-0.03</span>
											</div>
											<div class="disclosure disclosure2">
												<span class="disclosure-title">등락률</span>
												<span class="disclosure-gray">(%)</span>
												<span class="disclosure-down">▼</span>
												<span class="disclosure-down-blue">-0.25</span>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="bxWrap">
										<strong>누적투자건수<span>2020년 1월 30일 기준</span></strong>
										<div class="bx-num"><span id="bxcounter04"></span><span>건</span></div>
										<div class="disclosure-wrap">
											<div class="disclosure">
												<span class="disclosure-title">전월대비</span>
												<span class="disclosure-gray">(건)</span>
												<span class="disclosure-up">▲</span>
												<span class="disclosure-up-blue">+32,499</span>
											</div>
											<div class="disclosure disclosure2">
												<span class="disclosure-title">등락률</span>
												<span class="disclosure-gray">(%)</span>
												<span class="disclosure-up">▲</span>
												<span class="disclosure-up-blue">+2.46</span>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					
				</div>-->



				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
	
<%@include file="../include/footer.jsp" %>
</body>
</html>