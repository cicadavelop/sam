<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="inner">
					<div class="sub-member">
						<div class="member-wrap">
							<h2>아이디 찾기</h2>
							<div class="join-wrap">
								<div class="join-form">
									<form name="searchIdForm">
										<div class="join-form-item">
											<span>이름</span>
											<div><input type="text" name="name" id="name" placeholder="이름을 입력하세요" class="input-line"/></div>
										</div>
										<div class="join-form-item">
											<span>휴대폰 번호</span>
											<div><input type="text" name="phone" id="phone" placeholder="-없이 번호 입력" class="input-line"/></div>
										</div>
										<div class="join-btn find-id-btn"><input type="button" value="확인"></div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
<script src="/resources/web/js/SearchId.js"></script>
</body>
</html>