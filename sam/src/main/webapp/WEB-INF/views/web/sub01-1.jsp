<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<style>
	.submit-btn {
	border: 1px solid #333 !important;
    background: #333 !important;
    color: #fff !important;
    }
	</style>
	<body>
	<%@include file="../include/header.jsp" %>
				<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="sub-top-title sub-top01-1"> 자동 분산투자</div>
				<div class="sub-inner">
					<div class="fund-select-wrap" role="application">
					<form name="autoForm">
						<input type="hidden" name="member_seq" value="${sessionScope.MemberEntity.member_seq}">
						<div class="fund-select-top">
							<h3>상품유형</h3>
							<button type="button" name="fund-select" value="1" class="fl select-btn01">수익&amp;안정<span>수익과 안정을 한번에!</span></button>
							<button type="button" name="fund-select" value="2" class="fl select-btn02">고수익형<span>부동산 건축자금 계열</span></button>
							<button type="button" name="fund-select" value="3" class="fl select-btn03">초안정형<span>부동산 담보상품 계열</span></button>
						</div>
						<div class="fund-select">
							<div class="fund-select01 fund-bottom-10">
								<h3>
									건별 투자금액
									<div class="fr">
										<span id="fund-select01-value"></span>
									</div>
								</h3>
  								<div class="hslider" id="fund-select01"></div> 
  								<div class="fund-select-line-wrap">
	  								<div class="fund-select-line" style="left:0"><span>10만원</span></div>  
	  								<div class="fund-select-line" style="left:11.1111%"></div>
	  								<div class="fund-select-line" style="left:22.2222%"></div>
	  								<div class="fund-select-line" style="left:33.3333%"></div>
	  								<div class="fund-select-line" style="left:44.4444%"></div>
	  								<div class="fund-select-line" style="left:55.5555%"></div>
	  								<div class="fund-select-line" style="left:66.6666%"></div>
	  								<div class="fund-select-line" style="left:77.7777%"></div>
	  								<div class="fund-select-line" style="left:88.8888%"></div>
	  								<div class="fund-select-line" style="left:99.9999%"></div>
	  								<div class="fund-select-line" style="left:100%"><span>500만원</span></div>
	  							</div>
							</div>
							<div class="fund-select02 fund-bottom-10">
								<h3>
									수익률
									<div class="fr">
										<span id="fund-select02-value-low" class="fund-value-low"></span>
										<span id="fund-select02-value-upper" class="fund-value-upper"></span>
									</div>
								</h3>
								<div class="fund-type">
									<span class="green"></span><span>안정</span>
									<span class="orange"></span><span>균형</span>
									<span class="blue"></span><span>고수익</span>
								</div>
  								<div class="hslider" id="fund-select02"></div>  
  								<div class="fund-select-line-wrap">
	  								<div class="fund-select-line" style="left:0;"><span style="color:#8cc987;">7%</span></div>  
	  								<div class="fund-select-line" style="left:6.66%;"><span style="color:#8cc987;">8%</span></div>
	  								<div class="fund-select-line" style="left:13.32%;"><span style="color:#8cc987;">8%</span></div>
	  								<div class="fund-select-line" style="left:19.98%;"><span style="color:#8cc987;">10%</span></div>
	  								<div class="fund-select-line" style="left:26.64%;"><span style="color:#8cc987;">11%</span></div>
	  								<div class="fund-select-line" style="left:33.3%;"><span style="color:#f8b078;">12%</span></div>
	  								<div class="fund-select-line" style="left:39.96%;"><span style="color:#f8b078;">13%</span></div>
	  								<div class="fund-select-line" style="left:46.62%;"><span style="color:#f8b078;">14%</span></div>
	  								<div class="fund-select-line" style="left:53.28%;"><span style="color:#f8b078;">15%</span></div>
	  								<div class="fund-select-line" style="left:59.94%;"><span style="color:#f8b078;">16%</span></div>
	  								<div class="fund-select-line" style="left:66.6%;"><span style="color:#88a2fa;">17%</span></div>
	  								<div class="fund-select-line" style="left:73.26%;"><span style="color:#88a2fa;">18%</span></div>
	  								<div class="fund-select-line" style="left:79.92%;"><span style="color:#88a2fa;">19%</span></div>
	  								<div class="fund-select-line" style="left:86.58%;"><span style="color:#88a2fa;">20%</span></div>
	  								<div class="fund-select-line" style="left:93.24%;"><span style="color:#88a2fa;">21%</span></div>
	  								<div class="fund-select-line" style="left:99.9%;"><span style="color:#88a2fa;">22%</span></div>
	  							</div>
							</div>
							<div class="fund-select03 fund-bottom-10">
								<h3>
									투자기간
									<div class="fr">
										<span id="fund-select03-value-low" class="fund-value-low"></span>
										<span id="fund-select03-value-upper" class="fund-value-upper"></span>
									</div>
								</h3>
  								<div class="hslider" id="fund-select03"></div>  
  								<div class="fund-select-line-wrap">
	  								<div class="fund-select-line" style="left:0"><span>6개월</span></div>  
	  								<div class="fund-select-line" style="left:10%"></div>
	  								<div class="fund-select-line" style="left:20%"></div>
	  								<div class="fund-select-line" style="left:30%"></div>
	  								<div class="fund-select-line" style="left:40%"></div>
	  								<div class="fund-select-line" style="left:50%"></div>
	  								<div class="fund-select-line" style="left:60%"></div>
	  								<div class="fund-select-line" style="left:70%"></div>
	  								<div class="fund-select-line" style="left:80%"></div>
	  								<div class="fund-select-line" style="left:90%"></div>
	  								<div class="fund-select-line" style="left:100%"><span>24개월</span></div>
	  							</div>
							</div>
						</div>
						<div class="fund-setting">
							<h3>동일상품 중복투자 설정</h3>
							<span>중복투자를 허용하시면, 동일 물건의 이후 차수 투자상품에 투자될 수 있습니다.</span>
							<div class="fund-switch">
								<label class="switch">
									<input type="checkbox">
									<span class="slider round"></span>
									<p class="switch-off active">OFF</p><p class="switch-on">ON</p>
								</label>
							</div>
						</div>
						<div class="fund-btn">
							<button type="button" name="fund-select-btn" value="초기화"><i class="fa fa-repeat" aria-hidden="true"></i>초기화</button>
							<button class="submit-btn" type="button" name="fund-select-btn" value="신청하기">신청하기</button>
						</div>
						</form>
					</div>
				</div>

			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
<script src="/resources/js/nouislider.js"></script>
<script src="/resources/web/js/sub01-1.js"></script>
		<script>
					
		    var fSlider = document.getElementById('fund-select01');
		    var fSlider2 = document.getElementById('fund-select02');
		    var fSlider3 = document.getElementById('fund-select03');
		    var fSliderValue = document.getElementById('fund-select01-value');
		    var fSliderValue2 = [
		      document.getElementById('fund-select02-value-low'),
		      document.getElementById('fund-select02-value-upper')
		    ];
		    var fSliderValue3 = [
		      document.getElementById('fund-select03-value-low'),
		      document.getElementById('fund-select03-value-upper')
		    ];

		    noUiSlider.create(fSlider, {
		      start: [250],
		      step: 10,
		      range: {
		        min: [10],
		        max: [500]
		      }
		    });
		    fSlider.noUiSlider.on('update', function (values, handle) {
		        fSliderValue.innerHTML = values[handle];
		        var str = values[handle];
		        var text = Math.floor(str);
		        $("#fund-select01-value").html(text);
		    });

		    noUiSlider.create(fSlider2, {
		      start: [7, 15],
		      step: 1,
		      connect: true,
		      range: {
		          'min': [7],
		          'max': [22]
		      }
		    });
		    fSlider2.noUiSlider.on('update', function (values, handle) {
		      fSliderValue2[handle].innerHTML = values[handle];
		      var str = values[0];
		      var text = Math.floor(str);
		      var str1 = values[1];
		      var text1 = Math.floor(str1);
		      $("#fund-select02-value-low").html(text);
		      $("#fund-select02-value-upper").html(text1);
		    });

		     noUiSlider.create(fSlider3, {
		      start: [6, 16],
		      step: 1,
		      connect: true,
		      range: {
		          'min': [6],
		          'max': [24]
		      }
		    });
		    fSlider3.noUiSlider.on('update', function (values, handle) {
		      fSliderValue3[handle].innerHTML = values[handle];
		      var str = values[0];
		      var text = Math.floor(str);
		      var str1 = values[1];
		      var text1 = Math.floor(str1);
		      $("#fund-select03-value-low").html(text);
		      $("#fund-select03-value-upper").html(text1);
		    });

		    $(".fund-select-top .select-btn01").addClass("active");
		    var button1 = $(".select-btn01");
		    var button2 = $(".select-btn02");
		    var button3 = $(".select-btn03");

		    button1.on('click', function () {
		      $(".fund-select-top button").removeClass('active');
		      $(this).addClass("active");
		      fSlider2.noUiSlider.updateOptions({
		          start: [7, 15]
		      });
		      fSlider3.noUiSlider.updateOptions({
		          start: [6, 16]
		      });
		    });

		    button2.on('click', function () {
		      $(".fund-select-top button").removeClass('active');
		      $(this).addClass("active");
		      fSlider2.noUiSlider.updateOptions({
		          start: [10, 15]
		      });
		      fSlider3.noUiSlider.updateOptions({
		          start: [6, 16]
		      });
		    });

		    button3.on('click', function () {
		      $(".fund-select-top button").removeClass('active');
		      $(this).addClass("active");
		      fSlider2.noUiSlider.updateOptions({
		          start: [7, 10]
		      });
		      fSlider3.noUiSlider.updateOptions({
		          start: [6, 12]
		      });
		    });
// 		    $(document).ready(function(){
// 		    	$("label.switch").click(function(){
// 					if($(".switch-off").css("display") == "block") {
// 						$(".switch-off").attr("class", "switch-off active");
// 						$(".switch-on").attr("class", "switch-on");
// 					}
// 		    	})
// 		    	$("label.switch").click(function(){
// 					if($(".switch-on").css("display") == "block") {
// 						$(".switch-on").attr("class", "switch-on active");
// 						$(".switch-off").attr("class", "switch-off");
// 					}
// 		    	})
// 		    })
		</script>
</body>
</html>