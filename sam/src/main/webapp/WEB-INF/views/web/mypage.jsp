<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container mypage-wrap clear">
				<div class="inner">
				<%@include file="../include/mymenu.jsp" %>
					<div class="sub-content">
						<div class="sub-dash">
							<div class="dash-top clear">
								<div>
									<p>현재 투자중인 금액</p>
									<strong>0원</strong>
								</div>
								<div>
									<p>총 투자금액</p>
									<strong>0원</strong>
								</div>
								<div>
									<p>누적 상환 금액</p>
									<strong>0원</strong>
								</div>
								<div>
									<p>투자평균 수익률(세전)</p>
									<strong>0.00%</strong>
								</div>
								<div>
									<p>총 투자수익(세전)</p>
									<strong>0원</strong>
								</div>
								<div>
									<p>대출현황</p>
									<strong>0원</strong>
								</div>
							</div>
							<div class="my-cash">
								<div class="my-cash-top clear">
									<strong class="fl">보유 예치금</strong>
									<a href="/member/myaccount" class="fr">출금</a>
								</div>
								<div class="my-cash-cont">
									<div class="clear">
										<span class="fl">신한은행</span>
										<span class="fr">1234-123-123456</span>
									</div>
									<div class="clear">
										<span class="fl">예금주</span>
										<span class="fr">길동쓰</span>
									</div>
								</div>
								<div class="my-cash-bottom">
									<span>0원</span>
								</div>
								<div class="dash-btn"><a href="/sub01-1">자동투자 설정하기</a></div>
							</div>
						</div>
					</div>
					
				</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>