<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container mypage-wrap clear">
				<div class="inner">
				<%@include file="../include/mymenu.jsp" %>
					<div class="sub-content">
						<div class="t-top">
							<div class="row t-top-view">
								<div class="cell">
									<strong class="fl">조회기간</strong>
									<div class="fl">
										<div class="date-wrap p-date-wrap">
											<i class="fa fa-calendar" aria-hidden="true"></i>
											<span><input type="text" class="config-demo01 form-control" value="2020-04-01"></span>
											<em>~</em>
											<span><input type="text" class="config-demo02 form-control" value="2020-04-30"></span>
										</div>
										<div class="date-wrap m-date-wrap">
											<span><input type="text" class="config-demo01 form-control" value="2020-04-01"><i class="fa fa-calendar" aria-hidden="true"></i></span>
											<em>~</em>
											<span><input type="text" class="config-demo02 form-control" value="2020-04-30"><i class="fa fa-calendar" aria-hidden="true"></i></span>
										</div>
									</div>
								</div>
								<div class="cell">
									<div class="t-button">
										<button class="on">1개월</button>
										<button>3개월</button>
										<button>6개월</button>
										<button>1년</button>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="cell">
									<strong class="fl">조회내용</strong>
									<div class="fl">
										<div class="check-input">
											<input type="checkbox" id="checkall">
											<label for="checkall"><span>전체</span></label>
										</div>
										<div class="check-input">
											<input type="checkbox" name="chk" id="label01">
											<label for="label01"><span>입금</span></label>
										</div>
										<div class="check-input">
											<input type="checkbox" name="chk" id="label02">
											<label for="label02"><span>출금</span></label>
										</div>
									</div>
								</div>
								<div class="cell">
									<a href="#" class="t-view">조회하기</a>
								</div>
							</div>
						</div>
						<table cellspacing="0" cellpadding="0" border="0" class="t-table">
							<colgroup>
								<col width="13%"/>
								<col width="12%"/>
								<col width="*"/>
								<col width="15%"/>
								<col width="15%"/>
							</colgroup>
							<tr>
								<th>거래일자</th>
								<th>구분</th>
								<th>거래내역</th>
								<th>입출금</th>
								<th>잔액</th>
							</tr>
							<tr>
								<td class="empty" colspan="5">조회된 거래내역이 없습니다.</td>
							</tr>
						</table>
					</div>
				</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>