<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
	
				<!-- 본문 -->
			<section id="container" class="sub-container mypage-wrap clear">
				<div class="inner">
					<div class="modify myaccount">
						<form name="infoForm">
							<input type="hidden" name="member_seq" value="${sessionScope.MemberEntity.member_seq }">
							<div class="account-item clear">
								<div class="fl">이름</div>
								<div class="fr disable">
									<input type="text" readonly="readonly" name="name" placeholder="${sessionScope.MemberEntity.name }" value="${sessionScope.MemberEntity.name }">
								</div>
							</div>
							<div class="account-item clear">
								<div class="fl">아이디</div>
								<div class="fr disable">
									<input type="text" readonly="readonly" name="member_id" placeholder="${sessionScope.MemberEntity.member_id }" value="${sessionScope.MemberEntity.member_id }">
								</div>
							</div>
							<div class="account-item clear">
								<div class="fl">비밀번호</div>
								<div class="fr disable">
									<input type="password" readonly="readonly" name="password" placeholder="****" class="pw-input">
									<a href="/member/searchpw">변경</a>
								</div>
							</div>
							<div class="account-item clear">
								<div class="fl">휴대전화</div>
								<div class="fr disable">
									<div class="phone-cert">
										<input type="text" placeholder="'-'제외한 숫자만 입력" class="pw-input">
										<a href="javascript:void(0)">인증요청</a>
									</div>
									<div>
										<input type="text" readonly="readonly" name="phone" placeholder="${sessionScope.MemberEntity.phone }" value="${sessionScope.MemberEntity.phone }" class="phone-change pw-input">
										<a href="javascript:void(0)" class="change-btn phone-btn">변경</a>
									</div>
								</div>
							</div>
							<div class="account-item clear">
								<div class="fl">이메일</div>
								<div class="fr">
									<input type="text" placeholder="${sessionScope.MemberEntity.email }" name="email" value="${sessionScope.MemberEntity.email }">
								</div>
							</div>
							<div class="account-item memNum clear">
								<div class="fl">주민등록번호</div>
								<div class="fr disable">
									<input type="text" readonly="readonly" name="reg_num" placeholder="${sessionScope.MemberEntity.reg_num }" value="${sessionScope.MemberEntity.reg_num }">
								</div>
							</div>
							<div class="account-item gray-line clear">
								<div class="fl">현금영수증 발행</div>
								<div class="fr" style="text-align: left;">
									<div class="cash">
										<input type="radio" id="radio01" name="receipt_yn" value="Y" <c:if test="${sessionScope.MemberEntity.receipt_yn eq 'Y' }">checked</c:if>>
										<label for="radio01"><span>발행함</span></label>
									</div>
									<div class="cash">
										<input type="radio" id="radio02" name="receipt_yn" value="N" <c:if test="${sessionScope.MemberEntity.receipt_yn eq 'N' }">checked</c:if>>
										<label for="radio02"><span>발행안함</span></label>
									</div>
								</div>
							</div>
							<div class="account-item gray-line clear">
								<div class="fl">마케팅 동의</div>
								<div class="fr" style="text-align: left;">
									<p class="blue">알림 수신 동의시 샘인베스트먼트 투자상품 오픈 정보/이벤트 소식 등을 제공 받으실 수 있습니다.</p>
									<div class="check-input">
										<input type="checkbox" id="check01" <c:if test="${sessionScope.MemberEntity.email_yn eq 'Y' }">checked</c:if>>
										<label for="check01"><span>이메일 수신동의</span></label>
									</div>
									<div class="check-input">
										<input type="checkbox" id="check02" <c:if test="${sessionScope.MemberEntity.sms_yn eq 'Y' }">checked</c:if>>
										<label for="check02"><span>SMS 수신동의</span></label>
									</div>
								</div>
							</div>
							<div class="modify-btn"><a href="#none">수정완료</a></div>
						</form>
					</div>
					<p class="member-out">샘인베스트먼트 서비스를 더 이상 이용하지 않는다면? <a href="#" class="blue">회원탈퇴</a></p>
					
				</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
		<%@include file="../include/footer.jsp" %>
		<script src="/resources/web/js/Modify.js"></script>
		<script src="/resources/js/jquery.easing.1.3.js"></script>
		<script>
			$(document).ready(function() {
				$(".change-btn").click(function() {
					$(".phone-cert").show();
					$(".phone-btn").text("확인");
					$(this).parent().parent().removeClass("disable");
					$(".phone-change").prop('readonly',false);
					$(".phone-change").attr("placeholder", "인증번호를 입력해주세요.");
				});
				var frm = document.infoForm;
				var regnum = frm.reg_num.value;
				if(regnum.length > 6) {
					var regnum1 = regnum.substring(0,6);
					var regnum2 = regnum.substring(6,7);
					for(i=1; i<regnum.substring(6).length && i < 7; i++) {
						regnum2 = regnum2 + "*";
					}
					$("input[name=reg_num]").val(regnum1+"-"+regnum2); 
				}
			});
		</script>
</body>
</html>