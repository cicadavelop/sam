<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<%@include file="../include/head.jsp" %>
	<body>
	<%@include file="../include/header.jsp" %>
		<!-- 본문 -->
			<section id="container" class="sub-container">
				<div class="inner">
					<div class="sub-member">
						<div class="member-wrap">
							<h2>아이디/비밀번호 찾기</h2>
							<div class="join-wrap">
								<div class="join-select">
									<p>아이디를 잊으셨나요?</p>
									<a href="/member/searchid" class="personal">아이디 찾기</a>
									<p>비밀번호를 잊으셨나요?</p>
									<a href="/member/searchpw" class="corporation">비밀번호 찾기</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</section>
			<!-- // 본문 끝 -->
			
			
		</div>

		<div class="clear"></div>
<%@include file="../include/footer.jsp" %>
</body>
</html>