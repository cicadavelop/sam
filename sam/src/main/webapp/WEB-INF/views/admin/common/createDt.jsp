<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="form-group row">
	<label class="col-lg-4 col-form-label" for="content">등록일</label>
	<div class="col-lg-6">
		<jsp:useBean id="toDay" class="java.util.Date" />
		<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd HH:mm" var="toDay" />
		${toDay }
    </div>
</div>


<div class="form-group row">
	<div class="col-lg-6 col-form-label">
		<button class="btn btn-primary" type="submit" >등록</button>
		<button class="btn btn-danger" type="button" onclick="javascript:history.back(-1)" >취소</button>
	</div>
</div>