<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<td>${row.create_dt_text }</td>
<td>${row.create_by_text }</td>
<%-- <td>${row.update_dt_text }</td> --%>
<%-- <td>${row.update_by_text }</td> --%>
<td>
	<div class="btn-group" data-toggle="buttons">
		<label
			class="btn btn-warning showBtn showYnBtn <c:if test="${row.show_yn eq 'Y' }">active</c:if>"
			data-role="Y" data-seq="${seq}" data-table-name="${TABLENM }"
			data-table-seq="${TABLESEQ }"> <input type="radio"
			name="showyn"
			<c:if test="${row.show_yn eq 'Y' }">checked="checked"</c:if>
			style="display: none">노출
		</label> <label
			class="btn btn-warning showBtn showYnBtn <c:if test="${row.show_yn eq 'N' }">active</c:if>"
			data-role="N" data-seq="${seq}" data-table-name="${TABLENM }"
			data-table-seq="${TABLESEQ }"> <input type="radio"
			name="showyn"
			<c:if test="${row.show_yn eq 'N' }">checked="checked"</c:if>
			style="display: none">비노출
		</label>
	</div>
</td>