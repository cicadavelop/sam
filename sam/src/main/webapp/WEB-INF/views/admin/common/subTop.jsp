<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- 상단 -->
  <h4 class="card-title">
	${TITLE} 상세<br>
	<button class="btn btn-primary" onclick="javascript:location.href='/supervise/${KEY}'">목록</button>
	<a href="/supervise/${KEY}/update/${SEQ}"><button class="btn btn-info">수정</button></a>
	<button class="btn btn-danger" id="remove" data-seq="${SEQ}">삭제</button>
</h4>
<!-- 상단 -->