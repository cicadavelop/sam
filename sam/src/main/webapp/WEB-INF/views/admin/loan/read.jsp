<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<c:set var="TITLE" value="대출신청" />
<c:set var="KEY" value="loan" />
<c:set var="SEQ" value="${read.loan_seq}" />
<c:set var="TABLENM" value="loan" />
<c:set var="TABLESEQ" value="loan_seq" />
<script>
	var TITLE = "대출신청";
	var KEY = "loan";
	var TABLENM = "loan";
	var TABLESEQ = "loan_seq";
</script>

<!-- Page wrapper  -->
<div class="page-wrapper">
   <%@include file="/WEB-INF/views/admin/common/top.jsp" %>
   
    <!-- 내용  -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">번호 : ${SEQ}</h6>
                         <table class="table">
                        	<tr>
                        		<th>담보</th>
                        		<td>${read.loan_type_text}</td>
                        		<th>이름</th>
                        		<td>${read.name }</td>
                        		<th>휴대폰 번호</th>
                        		<td>${read.phone}</td>
                        	</tr>
                        	<tr>
                        		<th>생년월일</th>
                        		<td>${read.reg_num}</td>
                        		<th>성별</th>
                        		<td>${read.gender_text }</td>
                        		<th></th>
                        		<td></td>
                        	</tr>
                        	<tr>
                        		<th>희망대출액</th>
                        		<td>${read.hope_loan}</td>
                        		<th>대출희망기간</th>
                        		<td>${read.hope_loan_period }개월</td>
                        		<th></th>
                        		<td></td>
                        	</tr>
                        	<%@include file="/WEB-INF/views/admin/common/readHistory.jsp" %>
                        	<tr>
                        		<td colspan="99">담보내용 : ${read.collateral_content}</td>
                        	</tr>
                        	<tr>
                        		<td colspan="99">기타사항 : ${read.etc_info}</td>
                        	</tr>
                        	
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!-- 내용  -->
   
   
    <script src="/resources/admin/js/common/read.js"></script>
</div>
