<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<!-- Page wrapper  -->
<c:set var="TITLE" value="대출신청" />
<c:set var="KEY" value="loan" />
<c:set var="TABLENM" value="loan" />
<c:set var="TABLESEQ" value="loan_seq" />
<script>
	var TITLE = "대출신청";
	var KEY = "loan";
</script>



<div class="page-wrapper">

	<%@include file="/WEB-INF/views/admin/common/top.jsp" %>
    
    <!-- 내용  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">${TITLE} 목록
                        </h4>
                        <div class="table-responsive m-t-40">
                            <table id="commonTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>담보</th>
                                        <th>이름</th>
                                        <th>휴대폰 번호</th>
                                        <th>생년월일</th>
                                        <th>성별</th>
                                        <th>희망대출액</th>
                                        <th>대출희망기간</th>
                                        <%@include file="/WEB-INF/views/admin/common/historyTh.jsp" %>
                                    </tr>
                                </thead>
                                <tbody>
                                	<c:forEach var="row" items="${list }">
                                		<c:set var="seq" value="${row.loan_seq }" />
                                		<tr>	
	                                		<td>${row.rownum }</td>
	                                		<td>${row.loan_type_text}</td>
	                                		<td class="key-set" data-key="${row.loan_seq}">${row.name }</td>
	                                		<td>${row.phone }</td>
	                                		<td>${row.reg_num }</td>
	                                		<td>${row.gender_text }</td>
	                                		<td>${row.hope_loan }</td>
	                                		<td>${row.hope_loan_period }개월</td>
	                                		<%@include file="/WEB-INF/views/admin/common/historyTd.jsp" %>
	                                	</tr>
                                	</c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 내용  -->
    
    <script src="/resources/admin/js/common/list.js"></script>
</div>

