<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<c:set var="TITLE" value="펀드상품" />
<c:set var="KEY" value="fund" />
<script>
	var TITLE = "펀드상품";
	var KEY = "fund";
	
</script>

<div class="page-wrapper">
   	<%@include file="/WEB-INF/views/admin/common/top.jsp" %>
   	
   	<!-- 내용  -->
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                       <h4 class="card-title">${TITLE}수정</h4>
                        <div class="form-validation">
                            <form class="form-valide" action="/supervise/${KEY }/update" method="post" name="form" enctype="multipart/form-data">
	                            <input type="hidden" name="${KEY }_seq" value="${read.fund_seq }"/>
								<input type="hidden" name="write_by" id="write_by" value="${sessionScope.AdminEntity.admin_seq }"/>	


                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">제목<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="fund_title" name="fund_title" placeholder="(주)아트박스 〔상.제〕펀딩" value="${read.fund_title }">
                                    </div>
                                </div>
                                
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">서브제목<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="fund_sub" name="fund_sub" placeholder="대출채권 1001호" value="${read.fund_sub }">
                                    </div>
                                </div>
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">수익률<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="fund_profit" name="fund_profit" placeholder="ex) 연 3.54%" value="${read.fund_profit }">
                                    </div>
                                </div>
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">기간<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="fund_period" name="fund_period" placeholder="ex) 12" value="${read.fund_period }">
                                    </div>
                                </div>
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">대출금액<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="fund_loan" name="fund_loan" placeholder="ex) 5000" value="${read.fund_loan }">
                                    </div>
                                </div>
                                
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">대표이미지</label>
                                    <div class="col-lg-6">
                                           <input class="form-control" type="file" name="fund_images" id="fund_images" />
                                           <input class="form-control" type="hidden" name="fund_image" id="fund_image" value="${read.fund_image }"/>
                                           <a href="${read.fund_image }"> ${read.fund_image }</a>
                                    </div>
                                </div>
								
								
                               	<%@include file="/WEB-INF/views/admin/common/updateDt.jsp" %>
                                
                                
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 내용  -->
    
    
</div>
<!-- End Page wrapper  -->
<script>
var rules = {
        "fund_title": {
            required: !0
        },
        "fund_sub": {
            required: !0
        },
        "fund_profit": {
            required: !0
        },
        "fund_period": {
            required: !0
        },
        "fund_loan": {
            required: !0
        },
    }
    var messages = {
        "fund_title": {
      	  required: "펀드상품 제목을 입력해주세요.",
        },
        "fund_sub": {
      	  required: "펀드상품 서브제목을 입력해주세요.",
        },
        "fund_profit": {
      	  required: "펀드상품 수익률을 입력해주세요.",
        },
        "fund_period": {
      	  required: "펀드상품 기간을 입력해주세요.",
        },
        "fund_loan": {
      	  required: "펀드상품 대출금액을 입력해주세요.",
        },
    }
</script>
 <script src="/resources/admin/js/common/create.js"></script>