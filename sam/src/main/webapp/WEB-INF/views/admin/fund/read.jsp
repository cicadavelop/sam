<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<c:set var="TITLE" value="펀드상품" />
<c:set var="KEY" value="fund" />
<c:set var="SEQ" value="${read.fund_seq}" />
<c:set var="TABLENM" value="fund_prod" />
<c:set var="TABLESEQ" value="fund_seq" />
<script>
	var TITLE = "펀드상품";
	var KEY = "fund";
	var TABLENM = "fund_prod";
	var TABLESEQ = "fund_seq";
</script>

<!-- Page wrapper  -->
<div class="page-wrapper">
   <%@include file="/WEB-INF/views/admin/common/top.jsp" %>
   
    <!-- 내용  -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                    	<%@include file="/WEB-INF/views/admin/common/subTop.jsp" %>
                    	
                        <h6 class="card-title">번호 : ${SEQ}</h6>
                         <table class="table">
                        	<tr>
                        		<th>대표이미지</th>
                        		<td><a href="${read.fund_image}" target="_blank" ><img src="${read.fund_image}" style="width: 100px"></a></td>
                        		<th>제목</th>
                        		<td>${read.fund_title }</td>
                        		<th>서브제목</th>
                        		<td>${read.fund_sub}</td>
                        		<th>수익률</th>
                        		<td>${read.fund_profit}</td>
                        		<th>기간</th>
                        		<td>${read.fund_period}</td>
                        		<th>대출금액</th>
                        		<td>${read.fund_loan}</td>
                        	</tr>
                        	<%@include file="/WEB-INF/views/admin/common/readHistory.jsp" %>
                        	
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!-- 내용  -->
   
   
    <script src="/resources/admin/js/common/read.js"></script>
</div>
