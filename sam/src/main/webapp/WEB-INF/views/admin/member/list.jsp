<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<!-- Page wrapper  -->
<c:set var="TITLE" value="사용자" />
<c:set var="KEY" value="member" />
<c:set var="TABLENM" value="member" />
<c:set var="TABLESEQ" value="member_seq" />
<script>
	var TITLE = "사용자";
	var KEY = "member";
</script>



<div class="page-wrapper">

	<%@include file="/WEB-INF/views/admin/common/top.jsp" %>
    
    <!-- 내용  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">${TITLE} 목록
                        	<a href="/supervise/${KEY }/create"><button class="btn btn-primary">등록</button></a>
                        </h4>
                        <div class="table-responsive m-t-40">
                            <table id="commonTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>아이디</th>
                                        <th>이메일</th>
                                        <th>휴대폰 번호</th>
                                        <th>이름</th>
                                        <th>생년월일</th>
                                        <%@include file="/WEB-INF/views/admin/common/historyThNotShowYn.jsp" %>
                                    </tr>
                                </thead>
                                <tbody>
                                	<c:forEach var="row" items="${list }">
                                		<c:set var="seq" value="${row.member_seq }" />
                                		<tr>	
	                                		<td>${row.rownum }</td>
	                                		<td class="key-set" data-key="${row.member_seq}">${row.member_id }</td>
	                                		<td>${row.email }</td>
	                                		<td>${row.phone }</td>
	                                		<td>${row.name }</td>
	                                		<td>${row.reg_num }</td>
	                                		<%@include file="/WEB-INF/views/admin/common/historyTdNotShowYn.jsp" %>
	                                	</tr>
                                	</c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 내용  -->
    
    <script src="/resources/admin/js/common/list.js"></script>
</div>

