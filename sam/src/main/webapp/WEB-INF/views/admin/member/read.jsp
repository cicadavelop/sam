<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<c:set var="TITLE" value="사용자" />
<c:set var="KEY" value="member" />
<c:set var="SEQ" value="${read.member_seq}" />
<c:set var="TABLENM" value="member" />
<c:set var="TABLESEQ" value="member_seq" />
<script>
	var TITLE = "사용자";
	var KEY = "member";
	var TABLENM = "member";
	var TABLESEQ = "member_seq";
</script>

<!-- Page wrapper  -->
<div class="page-wrapper">
   <%@include file="/WEB-INF/views/admin/common/top.jsp" %>
   
    <!-- 내용  -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                    	<%@include file="/WEB-INF/views/admin/common/subTop.jsp" %>
                    	
                        <h6 class="card-title">번호 : ${SEQ}</h6>
                         <table class="table">
                        	<tr>
                        		<th>아이디</th>
                        		<td>${read.member_id }</td>
                        		<th>이메일</th>
                        		<td>${read.email }</td>
                        		<th>회원타입</th>
                        		<td>${read.type_text}</td>
                        	</tr>
                        	<tr>
                        		<th>이름</th>
                        		<td>${read.name}</td>
                        		<th>휴대폰 번호</th>
                        		<td>${read.phone }</td>
                        		<th>생년월일</th>
                        		<td>${read.reg_num}</td>
                        	</tr>
                        	<%@include file="/WEB-INF/views/admin/common/readHistoryNotShowYn.jsp" %>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!-- 내용  -->
   
   
    <script src="/resources/admin/js/common/read.js"></script>
</div>
