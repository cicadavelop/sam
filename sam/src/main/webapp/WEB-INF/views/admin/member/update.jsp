<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<c:set var="TITLE" value="사용자" />
<c:set var="KEY" value="member" />
<script>
	var TITLE = "사용자";
	var KEY = "member";
	
</script>
<style>
	input[type=radio]{
		width: 20px;
		display: inline;
	}
</style>
<div class="page-wrapper">
   	<%@include file="/WEB-INF/views/admin/common/top.jsp" %>
   	
   	<!-- 내용  -->
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                       <h4 class="card-title">${TITLE}수정</h4>
                        <div class="form-validation">
                            <form class="form-valide" action="/supervise/${KEY }/update" method="post" name="form" enctype="multipart/form-data">
                            <input type="hidden" name="${KEY }_seq" value="${read.member_seq }"/>
							<input type="hidden" name="write_by" value="${sessionScope.AdminEntity.admin_seq }"/>	


                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">아이디<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="member_id" name="member_id" value="${read.member_id }">
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">비밀번호<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="password" class="form-control" id="password" name="password">
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">이메일<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="email" name="email" value="${read.email }">
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">회원타입<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="radio" class="form-control" name="type" value="0" <c:if test="${read.type eq '0' }">checked="checked"</c:if>>개인회원<br>
                                           <input type="radio" class="form-control" name="type" value="1" <c:if test="${read.type eq '1' }">checked="checked"</c:if>>법인회원
                                    </div>
                                </div>
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">휴대폰 번호<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="number" class="form-control" id="phone" name="phone" value="${read.phone }">
                                    </div>
                                </div>
                                
                                    
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">이름<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="name" name="name" value="${read.name }">
                                    </div>
                                </div>
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">생년월일<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="number" class="form-control" id="reg_num" name="reg_num" value="${read.reg_num }">
                                    </div>
                                </div>
								
								
                               	<%@include file="/WEB-INF/views/admin/common/updateDt.jsp" %>
                                
                                
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 내용  -->
    
    
</div>
<!-- End Page wrapper  -->
<script>
var rules = {
        "member_id": {
            required: !0
        },
        "type": {
            required: !0
        },
        "email": {
            required: !0
        },
        "phone": {
            required: !0
        },
        "reg_num": {
            required: !0
        },
        "name": {
            required: !0
        },
    }
    var messages = {
        "member_id": {
      	  required: "사용자 아이디를 입력해주세요.",
        },
        "type": {
      	  required: "사용자 타입을 선택해주세요.",
        },
        "email": {
      	  required: "사용자 이메일을 입력해주세요.",
        },
        "phone": {
      	  required: "사용자 휴대폰 번호를 입력해주세요.",
        },
        "reg_num": {
      	  required: "사용자 생년월일을 입력해주세요.",
        },
        "name": {
      	  required: "사용자 이름을 입력해주세요.",
        },
    }
</script>
 <script src="/resources/admin/js/common/create.js"></script>