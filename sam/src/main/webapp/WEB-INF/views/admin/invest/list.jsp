<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<!-- Page wrapper  -->
<c:set var="TITLE" value="자동분산투자" />
<c:set var="KEY" value="invest" />
<c:set var="TABLENM" value="auto_investment" />
<c:set var="TABLESEQ" value="auto_seq" />
<script>
	var TITLE = "자동분산투자";
	var KEY = "invest";
</script>



<div class="page-wrapper">

	<%@include file="/WEB-INF/views/admin/common/top.jsp" %>
    
    <!-- 내용  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">${TITLE} 목록
                        	<a href="/supervise/${KEY }/create"><button class="btn btn-primary">등록</button></a>
                        </h4>
                        <div class="table-responsive m-t-40">
                            <table id="commonTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>상품유형</th>
                                        <th>건별 투자금액</th>
                                        <th>수익률</th>
                                        <th>투자기간</th>
                                        <th>동일상품 중복투자</th>
                                        <%@include file="/WEB-INF/views/admin/common/historyTh.jsp" %>
                                    </tr>
                                </thead>
                                <tbody>
                                	<c:forEach var="row" items="${list }">
                                		<c:set var="seq" value="${row.auto_seq }" />
                                		<tr>	
	                                		<td>${row.rownum }</td>
	                                		<c:choose>
	                                			<c:when test="${row.auto_type eq '1' }">
	                                				<td>수익&안정</td>
	                                			</c:when>
	                                			<c:when test="${row.auto_type eq '2' }">
	                                				<td>고수익형</td>
	                                			</c:when>
	                                			<c:otherwise>
	                                				<td>초안정형</td>
	                                			</c:otherwise>
	                                		</c:choose>
	                                		<td class="key-set" data-key="${row.auto_seq}">${row.auto_amount }만원</td>
	                                		<td>${row.min_profit }%~${row.max_profit }%</td>
	                                		<td>${row.min_period }~${row.max_period }개월</td>
	                                		<td>${row.samprod_yn }</td>
	                                		<%@include file="/WEB-INF/views/admin/common/historyTd.jsp" %>
	                                	</tr>
                                	</c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 내용  -->
    
    <script src="/resources/admin/js/common/list.js"></script>
</div>

