<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<c:set var="TITLE" value="보도자료" />
<c:set var="KEY" value="report" />
<c:set var="SEQ" value="${read.report_seq}" />
<c:set var="TABLENM" value="report" />
<c:set var="TABLESEQ" value="report_seq" />
<script>
	var TITLE = "보도자료";
	var KEY = "report";
	var TABLENM = "report";
	var TABLESEQ = "report_seq";
</script>

<!-- Page wrapper  -->
<div class="page-wrapper">
   <%@include file="/WEB-INF/views/admin/common/top.jsp" %>
   
    <!-- 내용  -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                    	<%@include file="/WEB-INF/views/admin/common/subTop.jsp" %>
                    	
                        <h6 class="card-title">번호 : ${SEQ}</h6>
                         <table class="table">
                        	<tr>
                        		<th>대표이미지</th>
                        		<td><a href="${read.report_image}" target="_blank" ><img src="${read.report_image}" style="width: 100px"></a></td>
                        		<th>제목</th>
                        		<td>${read.report_title }</td>
                        		<th>URL</th>
                        		<td>${read.report_url}</td>
                        	</tr>
                        	<%@include file="/WEB-INF/views/admin/common/readHistory.jsp" %>
                        	<tr>
                        		<td colspan="99">${read.report_content}</td>
                        	</tr>
                        	
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!-- 내용  -->
   
   
    <script src="/resources/admin/js/common/read.js"></script>
</div>
