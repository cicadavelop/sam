<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<c:set var="TITLE" value="보도자료" />
<c:set var="KEY" value="report" />
<script>
	var TITLE = "보도자료";
	var KEY = "report";
	
</script>

<div class="page-wrapper">
   	<%@include file="/WEB-INF/views/admin/common/top.jsp" %>
   	
   	<!-- 내용  -->
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                       <h4 class="card-title">${TITLE}수정</h4>
                        <div class="form-validation">
                            <form class="form-valide" action="/supervise/${KEY }/update" method="post" name="form" enctype="multipart/form-data">
                            <input type="hidden" name="${KEY }_seq" value="${read.report_seq }"/>
							<input type="hidden" name="write_by" value="${sessionScope.AdminEntity.admin_seq }"/>	


                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">제목<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="report_title" name="report_title" value="${read.report_title }">
                                    </div>
                                </div>
                                
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">내용<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <textarea class="form-control" id="report_content" name="report_content">${read.report_content }</textarea>
                                    </div>
                                </div>
                                
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">연결주소<span class="text-danger">*</span></label>
                                    <div class="col-lg-6">
                                           <input type="text" class="form-control" id="report_url" name="report_url" value="${read.report_url }">
                                    </div>
                                </div>
                                
                                
                               	<div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="album_seq">대표이미지</label>
                                    <div class="col-lg-6">
                                           <input class="form-control" type="file" name="report_images" id="report_images" />
                                           <input class="form-control" type="hidden" name="report_image" id="report_image" value="${read.report_image }"/>
                                           <a href="${read.report_image }"> ${read.report_image }</a>
                                    </div>
                                </div>
								
								
                               	<%@include file="/WEB-INF/views/admin/common/updateDt.jsp" %>
                                
                                
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 내용  -->
    
    
</div>
<!-- End Page wrapper  -->
<script>
	var rules = {
        "report_title": {
            required: !0
        },
        "report_content": {
            required: !0
        },
        "report_url": {
            required: !0
        },
    }
    var messages = {
        "report_title": {
      	  required: "보도자료 제목을 입력해주세요.",
        },
        "report_content": {
      	  required: "보도자료 내용을 입력해주세요.",
        },
        "report_url": {
      	  required: "보도자료 링크를 입력해주세요.",
        },
    }
</script>
 <script src="/resources/admin/js/common/create.js"></script>