<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/admin/common/jstl.jsp" %>
<!-- Page wrapper  -->
<c:set var="TITLE" value="보도자료" />
<c:set var="KEY" value="report" />
<c:set var="TABLENM" value="report" />
<c:set var="TABLESEQ" value="report_seq" />
<script>
	var TITLE = "보도자료";
	var KEY = "report";
</script>



<div class="page-wrapper">

	<%@include file="/WEB-INF/views/admin/common/top.jsp" %>
    
    <!-- 내용  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">${TITLE} 목록
                        	<a href="/supervise/${KEY }/create"><button class="btn btn-primary">등록</button></a>
                        </h4>
                        <div class="table-responsive m-t-40">
                            <table id="commonTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>대표이미지</th>
                                        <th>제목</th>
                                        <th>URL</th>
                                        <%@include file="/WEB-INF/views/admin/common/historyTh.jsp" %>
                                    </tr>
                                </thead>
                                <tbody>
                                	<c:forEach var="row" items="${list }">
                                		<c:set var="seq" value="${row.report_seq }" />
                                		<tr>	
	                                		<td>${row.rownum }</td>
	                                		<td><img src="${row.report_image }"></td>
	                                		<td class="key-set" data-key="${row.report_seq}">${row.report_title }</td>
	                                		<td>${row.report_url }</td>
	                                		<%@include file="/WEB-INF/views/admin/common/historyTd.jsp" %>
	                                	</tr>
                                	</c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 내용  -->
    
    <script src="/resources/admin/js/common/list.js"></script>
</div>

