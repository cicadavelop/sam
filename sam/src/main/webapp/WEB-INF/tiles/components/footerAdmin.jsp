<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
    <!-- Jquery JS-->
    <!-- Bootstrap JS-->
    <script src="/resources/admin/template/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="/resources/admin/template/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- /resources/vendor JS       -->
    <script src="/resources/admin/template/vendor/slick/slick.min.js">
    </script>
    <script src="/resources/admin/template/vendor/wow/wow.min.js"></script>
    <script src="/resources/admin/template/vendor/animsition/animsition.min.js"></script>
    <script src="/resources/admin/template/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="/resources/admin/template/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="/resources/admin/template/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="/resources/admin/template/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="/resources/admin/template/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="/resources/admin/template/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="/resources/admin/template/vendor/select2/select2.min.js">
    </script>
    <script src="/resources/admin/template/vendor/vector-map/jquery.vmap.js"></script>
    <script src="/resources/admin/template/vendor/vector-map/jquery.vmap.min.js"></script>
    <script src="/resources/admin/template/vendor/vector-map/jquery.vmap.sampledata.js"></script>
    <script src="/resources/admin/template/vendor/vector-map/jquery.vmap.world.js"></script>

    <!-- Main JS-->
    <script src="/resources/admin/template/js/main.js"></script>
<footer class="footer"><a href="http://cicada.or.kr/" target="_blank"></a></footer>