<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!DOCTYPE html>
<html lang="UTF-8">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard 2</title>

    <!-- Fontfaces CSS-->
    <link href="/resources/admin/template/css/font-face.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="/resources/admin/template/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- /resources/vendor CSS-->
    <link href="/resources/admin/template/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="/resources/admin/template/vendor/vector-map/jqvmap.min.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="/resources/admin/template/css/theme.css" rel="stylesheet" media="all">
    
    
    
    
    <!-- 템플릿 -->
    <link href="/resources/ad/css/lib/dropzone/dropzone.css" rel="stylesheet">
    <link href="/resources/ad/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/ad/css/helper.css" rel="stylesheet">
    <link href="/resources/ad/css/style.css" rel="stylesheet">
    <link href="/resources/ad/css/lib/select2/select2.css" rel="stylesheet">

    <link href="/resources/admin/css/common.css" rel="stylesheet">
    
    
    
    <script src="/resources/ad/js/lib/jquery/jquery.min.js"></script>
    <script src="/resources/ad/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="/resources/ad/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="/resources/ad/js/lib/form-validation/jquery.validate.min.js"></script>
    <script src="/resources/ad/js/charts/core.js"></script>
    <script src="/resources/ad/js/charts/charts.js"></script>
    <script src="/resources/ad/js/charts/animated.js"></script>
    <script src="/resources/ad/js/lib/datatables/datatables.min.js"></script>
    <script src="/resources/ad/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="/resources/ad/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="/resources/ad/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="/resources/ad/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="/resources/ad/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="/resources/ad/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="/resources/ad/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="/resources/ad/js/lib/dropzone/dropzone.js"></script>
    <script src="/resources/ad/js/lib/select2/select2.js"></script>
    
    <!-- 템플릿 -->
    <script src="/resources/admin/js/common/common.js"></script>
    
    
    
</head>
<body class="fix-header fix-sidebar">
    <!-- Main wrapper  -->
    <div id="main-wrapper">
    
		<tiles:insertAttribute name="headerAdmin" />
		<tiles:insertAttribute name="contentAdmin" />
		<tiles:insertAttribute name="footerAdmin" />
		
	</div>
    
</body>
</html>
