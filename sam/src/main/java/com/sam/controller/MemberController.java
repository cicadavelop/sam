package com.sam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sam.service.MasterService;

@Controller
@RequestMapping("/member/**")
public class MemberController {
	
	@Autowired
	MasterService service;
	
	@GetMapping(value="/join")
	public String join() {
		
		return "/web/join";
	}
	
	@GetMapping(value="/persnal")
	public String persnal() {
		
		return "/web/persnal";
	}
	
	@GetMapping(value="/corporation")
	public String corporation() {
		
		return "/web/corporation";
	}
	
	@GetMapping(value="/login")
	public String login() {
		
		return "/web/login";
	}
	
	@GetMapping(value="/search")
	public String search() {
		
		return "/web/search";
	}
	
	@GetMapping(value="/searchid")
	public String search_id() {
		
		return "/web/search-id";
	}
	
	@GetMapping(value="/searchpw")
	public String search_pw() {
		
		return "/web/search-pw";
	}
	
	@GetMapping(value="/modify")
	public String modify() {
		
		return "/web/modify"; 
	}
	
	@GetMapping(value="/mypage")
	public String mypage() {
		
		return "/web/mypage";
	}
	
	@GetMapping(value="/investhst")
	public String investHst() {
		
		return "/web/investHistory";
	}
	
	@GetMapping(value="/myaccount")
	public String myaccount() {
		
		return "/web/myaccount";
	}
	
	@GetMapping(value="/transaction")
	public String transaction() {
		
		return "/web/transaction";
	}
	
	@GetMapping(value="/point")
	public String point() {
		
		return "/web/point";
	}
}
