package com.sam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sam.service.MasterService;

@Controller
@RequestMapping("/loan/**")
public class LoanController {
	
	@Autowired
	MasterService service;
	
	@GetMapping(value="/individual")
	public String sub02_1() {
		
		return "/web/sub02-1";
	}
	
	@GetMapping(value="/property")
	public String sub02_2() {
		
		return "/web/sub02-2";
	}
	
	@GetMapping(value="/artificial")
	public String sub02_3() {
		
		return "/web/sub02-3";
	}
}
