package com.sam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sam.service.MasterService;

@Controller
@RequestMapping("/invest/**")
public class InvestmentController {
	
	@Autowired
	MasterService service;
	
	@GetMapping(value="/dispersion")
	public String sub01_1() {
		
		return "/web/sub01-1";
	}
	@GetMapping(value="/product")
	public String sub01_2() {
		
		return "/web/sub01-2";
	}
}
