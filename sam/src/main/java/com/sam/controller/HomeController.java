package com.sam.controller;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sam.service.MasterService;

@Controller
public class HomeController {

	@Inject MasterService service;
	
	@RequestMapping(value="/", method= {RequestMethod.GET})
	public String index(Model model) {
		
		return "/index";
	}
}
