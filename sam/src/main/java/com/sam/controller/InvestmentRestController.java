package com.sam.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sam.domain.FundProdEntity;
import com.sam.domain.InvestmentEntity;
import com.sam.domain.Meta;
import com.sam.service.MasterService;
import com.sam.util.Common;

@RestController
@RequestMapping("/api/**")
public class InvestmentRestController {

	@Inject MasterService service;
	
	@PostMapping(value="/investment")
	public ResponseEntity<Map<String, Object>> investment(@RequestBody InvestmentEntity vo) {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		int cnt = service.dataCreate("mapper.InvestmentMapper", "investment", vo);
		
		if(0 < cnt) {
			meta.setResult_code(200);
			meta.setResult_msg("OK");
		} else {
			meta.setResult_code(500);
			meta.setResult_msg("FAIL");
		}
		
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
	
	@PostMapping(value="/product")
	public ResponseEntity<Map<String, Object>> product(@RequestBody FundProdEntity vo) {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		rst.put("list", service.dataList("mapper.InvestmentMapper", "investList", vo));
		
		meta.setResult_code(200);
		meta.setResult_msg("�ݵ� ����Ʈ");
		
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
}
