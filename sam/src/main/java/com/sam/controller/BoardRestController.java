package com.sam.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sam.domain.Meta;
import com.sam.domain.BoardEntity;
import com.sam.service.MasterService;
import com.sam.util.Common;

@RestController
@RequestMapping("/api/board/**")
public class BoardRestController {
	
	@Autowired
	MasterService service;
	 
	@GetMapping(value = "/list")
	public ResponseEntity<Map<String, Object>> password_check(HttpServletRequest request, Model model, HttpSession session,@RequestBody BoardEntity vo) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		List<BoardEntity> list = null;
		
		list = (List<BoardEntity>) service.dataList("mapper.boardMapper", "list", vo);
		
		map.put("result", meta);
		map.put("data", list); 
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity;
	}
}
