package com.sam.controller;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sam.domain.MemberEntity;
import com.sam.domain.Meta;
import com.sam.service.LoginService;
import com.sam.service.MasterService;
import com.sam.util.Common;
import com.sam.util.Util;


/**
 * @version : java1.8 
 * @author : ohs
 * @date : 2018. 8. 8.
 * @class :
 * @message :
 * @constructors :
 * @method :
 */
@RestController
public class LoginController {
	
	
	@Inject MasterService service;
	@Inject LoginService loginService;
	Common common = new Common();
	
	
	/**
	 * 
	 * @msg 로그인
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login", method = {RequestMethod.POST})
	public ResponseEntity<Map<String, Object>> login(HttpServletRequest request, Model model, HttpSession session, @RequestBody MemberEntity vo) throws Exception {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		
		String login_rst = loginService.login(vo);
		
		if(Util.chkNullData(login_rst, "102")) {
			
			String push_token = vo.getPush_token();
			
			vo = loginService.user_info(vo);
			String token = common.makeToken(vo.getEmail(), vo.getPassword(), String.valueOf(vo.getMember_seq()));
			session.setAttribute("MemberEntity", vo);
			rst.put("authToken", token);
			
			
			/**
			 * 푸시토큰 업데이트
			 */
			if(!Util.chkNull(push_token)) {
				vo.setPush_token(push_token);
				loginService.push_token_update(vo);
			}
			
			meta.setResult_code(200);  //계정불일치
		} else if(Util.chkNull(login_rst)){
			meta.setResult_code(500);  //계정불일치
			meta.setResult_msg("이메일을 확인해주세요.");
		} else {
			meta.setResult_code(500); //패스워드 불일치
			meta.setResult_msg("비밀번호를 확인해주세요.");
		}
		
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
	}
	
	/**
	 * 
	 * @msg 회원가입
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/join", method = {RequestMethod.POST})
	public ResponseEntity<Map<String, Object>> join(HttpServletRequest request, Model model, HttpSession session, @RequestBody MemberEntity vo) throws Exception {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		int idcnt = service.dataCount("mapper.LoginMapper", "id_check", vo);
		
		if(0 == idcnt) {
			int phonecnt = service.dataCount("mapper.LoginMapper", "phone_check", vo);
			if(0 == phonecnt) {
				int record = loginService.join(vo);
				
				if(record > 0) {
					String token = common.makeToken(vo.getEmail(), vo.getPassword(), String.valueOf(vo.getMember_seq()));
					rst.put("authToken", token);
					meta.setResult_code(200);
				}else {
					meta.setResult_code(500);
					meta.setResult_msg("회원가입에 실패했습니다.");
				}
			} else {
				meta.setResult_code(500);
				meta.setResult_msg("이미 가입된 전화번호입니다.");
			}
		} else {
			meta.setResult_code(500);
			meta.setResult_msg("이미 가입된 아이디입니다.");
		}
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
		
	}
	
	/**
	 * 
	 * @msg 이메일중복체크
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/email/check", method = {RequestMethod.POST})
	public ResponseEntity<Map<String, Object>> join_check(HttpServletRequest request, Model model, HttpSession session, @RequestBody MemberEntity vo) throws Exception {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		
		int record  = loginService.email_check(vo);
		rst.put("record", record);
		
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
		
		
	}
	
	/**
	 * 
	 * @msg 전화번호 중복 체크
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/phone/check", method = {RequestMethod.POST})
	public ResponseEntity<Map<String, Object>> phone(HttpServletRequest request, Model model, HttpSession session, @RequestBody MemberEntity vo) throws Exception {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		
		int record  = loginService.phone_check(vo);
		rst.put("record", record);
		
		map.put("result", meta);
		map.put("data", rst);
		
		
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;
		
		
	}
	
	/**
	 * 
	 * @msg 로그아웃
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/logout", method = {RequestMethod.POST})
	public ResponseEntity<Map<String, Object>> logout(HttpSession session) {
		
		session.invalidate();
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		meta.setResult_code(200);
		
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
		
	}
	
	/**
	 * 
	 * @msg 닉네임 중복 체크
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/nick/check", method = {RequestMethod.POST})
	public ResponseEntity<Map<String, Object>> nick(HttpServletRequest request, Model model, HttpSession session, @RequestBody MemberEntity vo) throws Exception {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		
		int record  = loginService.nick_check(vo);
		rst.put("record", record);
		
		map.put("result", meta);
		map.put("data", rst);
		
		
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;
		
		
	}
	
	
	/**
	 * 
	 * @msg 유저 정보 가져오기
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/user/info", method = {RequestMethod.GET})
	public ResponseEntity<Map<String, Object>> user_info(HttpServletRequest request, Model model, HttpSession session, @RequestHeader String authToken) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		int member_seq = Integer.parseInt(common.getTokenInfo(request, "member_seq"));
		
		MemberEntity vo = new MemberEntity();
		vo.setMember_seq(member_seq);
		vo = loginService.user_info_member_seq(vo);
		
		if(vo != null) {
			meta.setResult_code(200);
		}else {
			meta.setResult_code(500);
		}
		
		map.put("result", meta);
		map.put("data", vo);
		
		
		
		try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
		catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;
		
	}
	
	
	

	/**
	 * 
	 * @msg 비회원 회원가입
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/join/guest", method = {RequestMethod.GET})
	public ResponseEntity<Map<String, Object>> joinGuest(HttpServletRequest request, Model model, HttpSession session) throws Exception {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		MemberEntity vo = new MemberEntity();
		int record  = loginService.joinGuest(vo);
		
		
		if(record > 0) {
			String token = common.makeToken(null, null, String.valueOf(vo.getMember_seq()));
			rst.put("authToken", token);
			meta.setResult_code(200);
		}else {
			meta.setResult_code(500);
		}
		
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
		
	}
	
	/**
	 * 
	 * @msg 로그아웃
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/logout")
	public ResponseEntity<Map<String, Object>> logout(Model model, HttpServletRequest request, HttpSession session) throws UnsupportedEncodingException {
		session.invalidate();
		
		   Enumeration se = session.getAttributeNames();
		   
		   while(se.hasMoreElements()){
		    String getse = se.nextElement()+"";
		    System.out.println("@@@@@@@ session : "+getse+" : "+session.getAttribute(getse));
		   }

		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		HashMap<String, Object> rst = new HashMap<String, Object>();
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
	}
	
	/**
	 * 
	 * @msg 아이디 찾기
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/find/id")
	public ResponseEntity<Map<String, Object>> findId(Model model, HttpServletRequest request,@RequestBody MemberEntity vo) throws UnsupportedEncodingException {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		MemberEntity entvo = (MemberEntity) service.dataRead("mapper.LoginMapper", "findId", vo);
		
		if(entvo != null) {
			rst.put("findid", entvo);
			meta.setResult_code(200);
		} else {
			meta.setResult_code(500);
			meta.setResult_msg("가입된 정보를 찾을 수 없습니다.");
		}
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
	}
	
	
	/**
	 * 
	 * @msg 비밀번호 찾기
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/find/pw")
	public ResponseEntity<Map<String, Object>> findPw(@RequestBody MemberEntity vo) throws UnsupportedEncodingException {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		MemberEntity entvo = (MemberEntity) service.dataRead("mapper.LoginMapper", "findPw", vo);
		
		if(entvo != null) {
			rst.put("findpw", entvo);
			meta.setResult_code(200);
		} else {
			meta.setResult_code(500);
			meta.setResult_msg("일치하지 않는 정보입니다.");
		}
		map.put("result", meta);
		map.put("data", rst);
		
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
	}
	
	/**
	 * 
	 * @msg 전화번호 중복 체크
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/myinfo/update")
	public ResponseEntity<Map<String, Object>> myInfoUpdate(@RequestBody MemberEntity vo, HttpSession session) throws UnsupportedEncodingException {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		int cnt = service.dataUpdate("mapper.LoginMapper", "infoUpdate", vo);
		if(0 < cnt) {
			vo = (MemberEntity) session.getAttribute("MemberEntity");
			vo = loginService.user_info(vo);
			session.setAttribute("MemberEntity", vo);
			meta.setResult_code(200);
		} else {
			meta.setResult_code(500);
			meta.setResult_msg("회원정보 수정에 실패하였습니다.");
		}
		map.put("result", meta);
		map.put("data", rst);
		
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
	}
}
