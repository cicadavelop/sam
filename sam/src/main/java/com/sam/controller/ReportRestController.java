package com.sam.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sam.domain.Meta;
import com.sam.service.MasterService;
import com.sam.util.Common;

@RestController
@RequestMapping("/api/**")
public class ReportRestController {
	
	@Inject MasterService service;
	Common common = new Common();
	
	/**
	 * 
	 * @msg 보도자료 리스트
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/report")
	public ResponseEntity<Map<String, Object>> report_list(HttpServletRequest request, Model model, HttpSession session ) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		rst.put("report", service.dataList("mapper.ReportMapper", "reportList", null));
		meta.setResult_code(200);
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
		
		
	}
}
