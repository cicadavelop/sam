package com.sam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/terms/**")
public class TermsController {
	
	@GetMapping(value="/guide")
	public String sub03() {
		
		return "/web/sub03";
	}
	
	@GetMapping(value="/release")
	public String release() {
		
		return "/web/sub04";
	}
	@GetMapping(value="/company")
	public String company() {
		
		return "/web/sub05";
	}
	@GetMapping(value="/contact")
	public String contact() {
		
		return "/web/sub06";
	}
	@GetMapping(value="/faq")
	public String faq() {
		
		return "/web/sub07";
	}
}
