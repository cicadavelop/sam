package com.sam.domain;

import com.sam.admin.domain.CommonEntity;

public class LoanEntity extends CommonEntity{
	int loan_seq;
	String loan_type; 
	String loan_type_text; 
	String collateral_estate_yn;
	String collateral_corporation_yn;
	String collateral_except_yn;
	String collateral_content;
	String name; 
	String phone; 
	String reg_num; 
	String gender; 
	String hope_loan; 
	String hope_loan_period; 
	String etc_info;
	
	String gender_text;
	
	
	public String getGender_text() {
		return gender_text;
	}
	public void setGender_text(String gender_text) {
		this.gender_text = gender_text;
	}
	public String getLoan_type_text() {
		return loan_type_text;
	}
	public void setLoan_type_text(String loan_type_text) {
		this.loan_type_text = loan_type_text;
	}
	public int getLoan_seq() {
		return loan_seq;
	}
	public void setLoan_seq(int loan_seq) {
		this.loan_seq = loan_seq;
	}
	public String getLoan_type() {
		return loan_type;
	}
	public void setLoan_type(String loan_type) {
		this.loan_type = loan_type;
	}
	public String getCollateral_estate_yn() {
		return collateral_estate_yn;
	}
	public void setCollateral_estate_yn(String collateral_estate_yn) {
		this.collateral_estate_yn = collateral_estate_yn;
	}
	public String getCollateral_corporation_yn() {
		return collateral_corporation_yn;
	}
	public void setCollateral_corporation_yn(String collateral_corporation_yn) {
		this.collateral_corporation_yn = collateral_corporation_yn;
	}
	public String getCollateral_except_yn() {
		return collateral_except_yn;
	}
	public void setCollateral_except_yn(String collateral_except_yn) {
		this.collateral_except_yn = collateral_except_yn;
	}
	public String getCollateral_content() {
		return collateral_content;
	}
	public void setCollateral_content(String collateral_content) {
		this.collateral_content = collateral_content;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getReg_num() {
		return reg_num;
	}
	public void setReg_num(String reg_num) {
		this.reg_num = reg_num;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getHope_loan() {
		return hope_loan;
	}
	public void setHope_loan(String hope_loan) {
		this.hope_loan = hope_loan;
	}
	public String getHope_loan_period() {
		return hope_loan_period;
	}
	public void setHope_loan_period(String hope_loan_period) {
		this.hope_loan_period = hope_loan_period;
	}
	public String getEtc_info() {
		return etc_info;
	}
	public void setEtc_info(String etc_info) {
		this.etc_info = etc_info;
	} 
	
	
	
}
