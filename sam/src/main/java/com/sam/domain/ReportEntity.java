package com.sam.domain;

import org.springframework.web.multipart.MultipartFile;

import com.sam.admin.domain.CommonEntity;

public class ReportEntity extends CommonEntity{
	
	String report_seq; 
	String report_title;
	String report_content;
	
	MultipartFile report_images;
	String report_image;
	String report_url;
	
	
	
	public MultipartFile getReport_images() {
		return report_images;
	}
	public void setReport_images(MultipartFile report_images) {
		this.report_images = report_images;
	}
	public String getReport_seq() {
		return report_seq;
	}
	public void setReport_seq(String report_seq) {
		this.report_seq = report_seq;
	}
	public String getReport_title() {
		return report_title;
	}
	public void setReport_title(String report_title) {
		this.report_title = report_title;
	}
	public String getReport_content() {
		return report_content;
	}
	public void setReport_content(String report_content) {
		this.report_content = report_content;
	}
	public String getReport_image() {
		return report_image;
	}
	public void setReport_image(String report_image) {
		this.report_image = report_image;
	}
	public String getReport_url() {
		return report_url;
	}
	public void setReport_url(String report_url) {
		this.report_url = report_url;
	}
	
	
}
