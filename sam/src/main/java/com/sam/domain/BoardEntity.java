package com.sam.domain;

public class BoardEntity {
	String seq;
	String type;
	String title;
	String content;
	String iamge;
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getIamge() {
		return iamge;
	}
	public void setIamge(String iamge) {
		this.iamge = iamge;
	}
	@Override
	public String toString() {
		return "BoardEntity [seq=" + seq + ", type=" + type + ", title=" + title + ", content=" + content + ", iamge="
				+ iamge + "]";
	}
}
