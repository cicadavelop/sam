package com.sam.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "common")
public class CommonEntity {

	@Id
	String type;
	
	String name;
	
	String info;
}
