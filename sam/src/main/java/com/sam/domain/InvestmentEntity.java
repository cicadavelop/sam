package com.sam.domain;

import com.sam.admin.domain.CommonEntity;

public class InvestmentEntity extends CommonEntity {
	
	int auto_seq;
	String auto_type;
	String auto_amount;
	String min_profit;
	String max_profit;
	String min_period;
	String max_period;
	String samprod_yn;
	
	int member_seq;
	
	public int getMember_seq() {
		return member_seq;
	}
	public void setMember_seq(int member_seq) {
		this.member_seq = member_seq;
	}
	public int getAuto_seq() {
		return auto_seq;
	}
	public void setAuto_seq(int auto_seq) {
		this.auto_seq = auto_seq;
	}
	public String getAuto_type() {
		return auto_type;
	}
	public void setAuto_type(String auto_type) {
		this.auto_type = auto_type;
	}
	public String getAuto_amount() {
		return auto_amount;
	}
	public void setAuto_amount(String auto_amount) {
		this.auto_amount = auto_amount;
	}
	public String getMin_profit() {
		return min_profit;
	}
	public void setMin_profit(String min_profit) {
		this.min_profit = min_profit;
	}
	public String getMax_profit() {
		return max_profit;
	}
	public void setMax_profit(String max_profit) {
		this.max_profit = max_profit;
	}
	public String getMin_period() {
		return min_period;
	}
	public void setMin_period(String min_period) {
		this.min_period = min_period;
	}
	public String getMax_period() {
		return max_period;
	}
	public void setMax_period(String max_period) {
		this.max_period = max_period;
	}
	public String getSamprod_yn() {
		return samprod_yn;
	}
	public void setSamprod_yn(String samprod_yn) {
		this.samprod_yn = samprod_yn;
	}
}
