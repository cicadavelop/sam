package com.sam.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "board")
public class DateCommonEntity {
	
	@Id
	int seq;
	
	@Temporal(TemporalType.DATE)
	String create_dt;
	
	String create_by;
	
	@Temporal(TemporalType.DATE)
	String delete_dt;
	
	String delete_by;
	
	String delete_yn;
}
