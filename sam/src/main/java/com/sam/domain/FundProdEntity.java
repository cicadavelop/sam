package com.sam.domain;

import org.springframework.web.multipart.MultipartFile;

import com.sam.admin.domain.CommonEntity;

public class FundProdEntity  extends CommonEntity {
	
	int fund_seq;
	String fund_image;
	MultipartFile fund_images;
	String fund_title;
	String fund_sub;
	String fund_profit;
	String fund_period;
	String fund_loan;
	
	 
	public MultipartFile getFund_images() {
		return fund_images;
	}
	public void setFund_images(MultipartFile fund_images) {
		this.fund_images = fund_images;   
	}
	public int getFund_seq() {
		return fund_seq;
	}
	public void setFund_seq(int fund_seq) {
		this.fund_seq = fund_seq;
	}
	
	public String getFund_image() {
		return fund_image;
	}
	public void setFund_image(String fund_image) {
		this.fund_image = fund_image;
	}
	public String getFund_title() {
		return fund_title;
	}
	public void setFund_title(String fund_title) {
		this.fund_title = fund_title;
	}
	public String getFund_sub() {
		return fund_sub;
	}
	public void setFund_sub(String fund_sub) {
		this.fund_sub = fund_sub;
	}
	public String getFund_profit() {
		return fund_profit;
	}
	public void setFund_profit(String fund_profit) {
		this.fund_profit = fund_profit;
	}
	public String getFund_period() {
		return fund_period;
	}
	public void setFund_period(String fund_period) {
		this.fund_period = fund_period;
	}
	public String getFund_loan() {
		return fund_loan;
	}
	public void setFund_loan(String fund_loan) {
		this.fund_loan = fund_loan;
	}
}
