package com.sam.admin.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.sam.admin.domain.AdminEntity;
import com.sam.admin.persistence.AdminLoginDao;

@Service
public class AdminLoginService {
	
	@Inject AdminLoginDao loginDao;
	
	public String login(AdminEntity dy){
		return loginDao.login(dy);
	}
	public int join(AdminEntity dy){
		return loginDao.join(dy);
	}
	public AdminEntity user_info(AdminEntity dy){
		return loginDao.user_info(dy);
	}
	public AdminEntity user_info_member_seq(AdminEntity dy){
		return loginDao.user_info_member_seq(dy);
	}
	public int email_check(AdminEntity dy){
		return loginDao.email_check(dy);
	}
	public int phone_check(AdminEntity dy){ 
		return loginDao.phone_check(dy);
	}
	public int nick_check(AdminEntity dy){ 
		return loginDao.nick_check(dy);
	}
	public int push_token_update(AdminEntity dy){
		return loginDao.push_token_update(dy); 
	}
	
	public int joinGuest(AdminEntity dy){
		return loginDao.joinGuest(dy); 
	}
}
