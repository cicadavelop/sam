package com.sam.admin.domain;

public class FundProdEntity {
	
	int fund_seq;
	String fund_img;
	String fund_title;
	String fund_sub;
	String fund_profit;
	String fund_period;
	String fund_loan;
	String delete_yn;
	String show_yn;
	String create_dt;
	String create_by;
	String update_dt;
	String update_by;
	
	
	public String getDelete_yn() {
		return delete_yn;
	}
	public void setDelete_yn(String delete_yn) {
		this.delete_yn = delete_yn;
	}
	public String getShow_yn() {
		return show_yn;
	}
	public void setShow_yn(String show_yn) {
		this.show_yn = show_yn;
	}
	public String getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(String create_dt) {
		this.create_dt = create_dt;
	}
	public String getCreate_by() {
		return create_by;
	}
	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}
	public String getUpdate_dt() {
		return update_dt;
	}
	public void setUpdate_dt(String update_dt) {
		this.update_dt = update_dt;
	}
	public String getUpdate_by() {
		return update_by;
	}
	public void setUpdate_by(String update_by) {
		this.update_by = update_by;
	}
	public int getFund_seq() {
		return fund_seq;
	}
	public void setFund_seq(int fund_seq) {
		this.fund_seq = fund_seq;
	}
	public String getFund_img() {
		return fund_img;
	}
	public void setFund_img(String fund_img) {
		this.fund_img = fund_img;
	}
	public String getFund_title() {
		return fund_title;
	}
	public void setFund_title(String fund_title) {
		this.fund_title = fund_title;
	}
	public String getFund_sub() {
		return fund_sub;
	}
	public void setFund_sub(String fund_sub) {
		this.fund_sub = fund_sub;
	}
	public String getFund_profit() {
		return fund_profit;
	}
	public void setFund_profit(String fund_profit) {
		this.fund_profit = fund_profit;
	}
	public String getFund_period() {
		return fund_period;
	}
	public void setFund_period(String fund_period) {
		this.fund_period = fund_period;
	}
	public String getFund_loan() {
		return fund_loan;
	}
	public void setFund_loan(String fund_loan) {
		this.fund_loan = fund_loan;
	}
	@Override
	public String toString() {
		return "FundProdEntity [fund_seq=" + fund_seq + ", fund_img=" + fund_img + ", fund_title=" + fund_title
				+ ", fund_sub=" + fund_sub + ", fund_profit=" + fund_profit + ", fund_period=" + fund_period
				+ ", fund_loan=" + fund_loan + "]";
	}
}
