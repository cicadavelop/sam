package com.sam.admin.domain;

public class CommonEntity {

	private int rownum;
	
	
	private String create_dt;
	private String create_dt_text;
	
	private String create_by;
	private String create_by_text;
	
	private String update_dt;
	private String update_dt_text;
	
	private String update_by;
	private String update_by_text;
	
	private String show_yn;
	private String delete_yn;
	private String write_by;
	
	
	
	public String getShow_yn() {
		return show_yn;
	}
	public void setShow_yn(String show_yn) {
		this.show_yn = show_yn;
	}
	public String getCreate_by_text() {
		return create_by_text;
	}
	public void setCreate_by_text(String create_by_text) {
		this.create_by_text = create_by_text;
	}
	public String getUpdate_by_text() {
		return update_by_text;
	}
	public void setUpdate_by_text(String update_by_text) {
		this.update_by_text = update_by_text;
	}
	public String getWrite_by() {
		return write_by;
	}
	public void setWrite_by(String write_by) {
		this.write_by = write_by;
	}
	public String getCreate_dt_text() {
		return create_dt_text;
	}
	public void setCreate_dt_text(String create_dt_text) {
		this.create_dt_text = create_dt_text;
	}
	public String getUpdate_dt_text() {
		return update_dt_text;
	}
	public void setUpdate_dt_text(String update_dt_text) {
		this.update_dt_text = update_dt_text;
	}
	public int getRownum() {
		return rownum;
	}
	public void setRownum(int rownum) {
		this.rownum = rownum;
	}
	public String getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(String create_dt) {
		this.create_dt = create_dt;
	}
	public String getCreate_by() {
		return create_by;
	}
	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}
	public String getUpdate_dt() {
		return update_dt;
	}
	public void setUpdate_dt(String update_dt) {
		this.update_dt = update_dt;
	}
	public String getUpdate_by() {
		return update_by;
	}
	public void setUpdate_by(String update_by) {
		this.update_by = update_by;
	}
	public String getDelete_yn() {
		return delete_yn;
	}
	public void setDelete_yn(String delete_yn) {
		this.delete_yn = delete_yn;
	}
}
