package com.sam.admin.domain;

public class InvestmentEntity {
	
	int auto_seq;
	String auto_type;
	String auto_amount;
	String min_profit;
	String max_profit;
	String min_period;
	String max_period;
	String create_by;
	String create_dt;
	String samprod_yn;
	String delete_yn;
	String update_dt;
	String update_by;
	String show_yn;
	
	public String getCreate_by() {
		return create_by;
	}
	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}
	public String getDelete_yn() {
		return delete_yn;
	}
	public void setDelete_yn(String delete_yn) {
		this.delete_yn = delete_yn;
	}
	public String getUpdate_dt() {
		return update_dt;
	}
	public void setUpdate_dt(String update_dt) {
		this.update_dt = update_dt;
	}
	public String getUpdate_by() {
		return update_by;
	}
	public void setUpdate_by(String update_by) {
		this.update_by = update_by;
	}
	public String getShow_yn() {
		return show_yn;
	}
	public void setShow_yn(String show_yn) {
		this.show_yn = show_yn;
	}
	public int getAuto_seq() {
		return auto_seq;
	}
	public void setAuto_seq(int auto_seq) {
		this.auto_seq = auto_seq;
	}
	public String getAuto_type() {
		return auto_type;
	}
	public void setAuto_type(String auto_type) {
		this.auto_type = auto_type;
	}
	public String getAuto_amount() {
		return auto_amount;
	}
	public void setAuto_amount(String auto_amount) {
		this.auto_amount = auto_amount;
	}
	public String getMin_profit() {
		return min_profit;
	}
	public void setMin_profit(String min_profit) {
		this.min_profit = min_profit;
	}
	public String getMax_profit() {
		return max_profit;
	}
	public void setMax_profit(String max_profit) {
		this.max_profit = max_profit;
	}
	public String getMin_period() {
		return min_period;
	}
	public void setMin_period(String min_period) {
		this.min_period = min_period;
	}
	public String getMax_period() {
		return max_period;
	}
	public void setMax_period(String max_period) {
		this.max_period = max_period;
	}
	public String getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(String create_dt) {
		this.create_dt = create_dt;
	}
	public String getSamprod_yn() {
		return samprod_yn;
	}
	public void setSamprod_yn(String samprod_yn) {
		this.samprod_yn = samprod_yn;
	}
}
