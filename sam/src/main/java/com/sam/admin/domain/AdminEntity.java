package com.sam.admin.domain;

public class AdminEntity {
	
	String admin_seq;
	String email;
	String password;
	String name;
	String nickname;
	String phone;
	String join_date;
	String push_token;
	String create_dt;
	String enter_type;
	public String getAdmin_seq() {
		return admin_seq;
	}
	public void setAdmin_seq(String admin_seq) {
		this.admin_seq = admin_seq;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getJoin_date() {
		return join_date;
	}
	public void setJoin_date(String join_date) {
		this.join_date = join_date;
	}
	public String getPush_token() {
		return push_token;
	}
	public void setPush_token(String push_token) {
		this.push_token = push_token;
	}
	public String getCreate_dt() {
		return create_dt;
	}
	public void setCreate_dt(String create_dt) {
		this.create_dt = create_dt;
	}
	public String getEnter_type() {
		return enter_type;
	}
	public void setEnter_type(String enter_type) {
		this.enter_type = enter_type;
	}
	@Override
	public String toString() {
		return "AdminEntity [admin_seq=" + admin_seq + ", email=" + email + ", password=" + password + ", name=" + name
				+ ", nickname=" + nickname + ", phone=" + phone + ", join_date=" + join_date + ", push_token="
				+ push_token + ", create_dt=" + create_dt + ", enter_type=" + enter_type + "]";
	}
	
	
}
