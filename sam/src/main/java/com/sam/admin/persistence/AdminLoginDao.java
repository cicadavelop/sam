package com.sam.admin.persistence;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.sam.admin.domain.AdminEntity;
import com.sam.admin.util.Common;

@Repository
public class AdminLoginDao {
	
	@Inject private SqlSession sqlSession;
	Common common = new Common();
	private final String NAMESPACE ="mapper.AdminMapper";
	
	public SqlSession getSqlSession() {return sqlSession;}
	public void setSqlSession(SqlSession sqlSession) {this.sqlSession = sqlSession;}
	
	public String login(AdminEntity vo){
		return sqlSession.selectOne(NAMESPACE+".login",vo);
	}
	
	public int join(AdminEntity dd){
		return sqlSession.insert(NAMESPACE+".join",dd);
	}
	
	public AdminEntity user_info(AdminEntity vo){
		return sqlSession.selectOne(NAMESPACE+".user_info",vo);
	}
	public AdminEntity user_info_member_seq(AdminEntity vo){
		return sqlSession.selectOne(NAMESPACE+".user_info_member_seq",vo);
	}
	
	public int email_check(AdminEntity vo){
		return sqlSession.selectOne(NAMESPACE+".email_check",vo);
	}
	public int phone_check(AdminEntity vo){
		return sqlSession.selectOne(NAMESPACE+".phone_check", vo);
	}
	public int nick_check(AdminEntity vo){
		return sqlSession.selectOne(NAMESPACE+".nick_check", vo);
	}
	public int push_token_update(AdminEntity dy){  
		return sqlSession.update(NAMESPACE+".push_token_update",dy);
	}
	
	public int joinGuest(AdminEntity dy){  
		int record = sqlSession.insert(NAMESPACE+".joinGuest",dy);
		if(record > 0) {
			record += sqlSession.update(NAMESPACE+".guestUpdate",dy);
		}
		return record == 2 ? 1 : 0;
	}
}
