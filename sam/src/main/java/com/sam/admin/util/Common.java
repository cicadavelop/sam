package com.sam.admin.util;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.sam.util.Util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class Common {
	public static final int JWT_EX_TIME = 4320 ; //24�떆媛� �쑀�슚�븿 / 移댁뭅�삤�넚 access_token = 12-24�떆媛�, refresh_token �븳�떖 
	
	
	//-----------------      JWT �넗�겙      --------------------------------------------------//
	private String SALT = "secret";
	private String AUTHORIZATION = "authToken";
	/**
	 * 濡쒓렇�씤�떆 JWT token �깮�꽦
	 */
	public String makeToken(String email, String pw, String i){
		long currentTime = System.currentTimeMillis()+(1000*60*JWT_EX_TIME); 
		
		String jwt = "";
		jwt = Jwts.builder().
			setSubject(pw).
			setExpiration(new Date(currentTime)).
			claim("email", email).
			claim("member_seq", i).
			signWith(SignatureAlgorithm.HS256, this.generateKey()).
			compact();
		return jwt;
	}
	
	/**
	 * 濡쒓렇�씤�떆 JWT token �깮�꽦
	 */
	public String makeToken(String member_seq, String type){
		int time = 0;
		
		long currentTime = System.currentTimeMillis()+(1000*60*time); 
		String jwt = "";
		jwt = Jwts.builder().
				setSubject(member_seq).
				setExpiration(new Date(currentTime)).
				claim("member_seq", member_seq).
				signWith(SignatureAlgorithm.HS256, this.generateKey()).
				compact();
		return jwt;
	}
	
	/**
	 * 濡쒓렇�씤�떆 JWT token �깮�꽦
	 */
	public String makeToken(String member_seq){
		long currentTime = System.currentTimeMillis()+(1000*60*JWT_EX_TIME); 
		String jwt = "";
		jwt = Jwts.builder().
				setSubject(member_seq).
				setExpiration(new Date(currentTime)).
				claim("member_seq", member_seq).
				signWith(SignatureAlgorithm.HS256, this.generateKey()).
				compact();
		return jwt;
	}
	
	/**
	 * �넗�겙 �쑀�슚�꽦 泥댄겕
	 */
	public int checkToken(HttpServletRequest request){
		final String TOKEN = request.getHeader(AUTHORIZATION);
		if(Util.chkNull(TOKEN)) return 24;
		
		int rst_code = 0;
		try{
			Jwts.parser().setSigningKey(this.generateKey()).parseClaimsJws(TOKEN);
		}catch (ExpiredJwtException e1){
			rst_code = 20;
		}catch (UnsupportedJwtException  e1){
			rst_code = 21;
		}catch (MalformedJwtException  e1){
			rst_code = 22;
		}catch (SignatureException  e1){
			rst_code = 23;
		}catch (Exception e1){
			rst_code = -1;
		}
		return rst_code;
	}
	/**
	 * �넗�겙 �쑀�슚�꽦 泥댄겕
	 */
	public int checkToken(String TOKEN){
		if(Util.chkNull(TOKEN)) return 24;
		
		int rst_code = 0;
		try{
			Jwts.parser().setSigningKey(this.generateKey()).parseClaimsJws(TOKEN);
		}catch (ExpiredJwtException e1){
			rst_code = 20;
		}catch (UnsupportedJwtException  e1){
			rst_code = 21;
		}catch (MalformedJwtException  e1){
			rst_code = 22;
		}catch (SignatureException  e1){
			rst_code = 23;
		}catch (Exception e1){
			rst_code = -1;
		}
		return rst_code;
	}
	
	/**
	 * �넗�겙 媛��졇�삤湲�
	 */
	public String getToken(HttpServletRequest request){
		return request.getHeader(AUTHORIZATION);
	}
	
	/**
	 * �넗�겙 �궡遺��쓽 object 媛��졇�삤湲�
	 */
	public Jws<Claims> getTokenInfo(HttpServletRequest request){
		String token = getToken(request);
		return Jwts.parser().setSigningKey(this.generateKey()).parseClaimsJws(token);
	}
	public String getTokenInfo(HttpServletRequest request, String key){
		String token = getToken(request);
		return (String) Jwts.parser().setSigningKey(this.generateKey()).parseClaimsJws(token).getBody().get(key);
	}
	public String getTokenInfo(String token, String key){
		return (String) Jwts.parser().setSigningKey(this.generateKey()).parseClaimsJws(token).getBody().get(key);
	}
	
	/**
	 * �넗�겙 �깮�꽦 諛� �솗�씤�떆 �씤肄붾뵫 sha 256�삎�떇�쑝濡� json �뜲�씠�꽣瑜� 諛붿씤�뵫�븳�떎
	 */
	private byte[] generateKey() {
		byte[] key = null;
		try {
			key = SALT.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return key;
	}
	
	//-----------------      JWT �넗�겙      --------------------------------------------------//
	static Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
	public static String GmakeDynamicValueObject(ResponseEntity<Map<String, Object>> entity){
		String ss = "";
		ss += gson.toJson(entity);
		return ss;
	}
	
	public static void returnPrint(String str){
		
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(str);
		str = gson.toJson(je);
		
		System.out.println("--------------------      Response Param     ---------------------------");
		System.out.println(str);
		System.out.println("------------------------------------------------------------------------");
	}
}
