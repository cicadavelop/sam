package com.sam.admin.controller;

import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/supervise/show/**")
public class ShowController extends CommonController{ 
	@RequestMapping(value = {"/{table_name}/{seq_name}/{seq}/{show_yn}"} , method = RequestMethod.GET)
	public @ResponseBody int CommonCodeUpdateGET(Model model,@PathVariable String table_name,@PathVariable String seq_name,@PathVariable int seq,@PathVariable String show_yn) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("table_name", table_name);
		map.put("seq_name", seq_name);
		map.put("seq", seq);
		map.put("show_yn", show_yn);
		
		int record = service.dataUpdate("mapper.AdminCommonMapper","showyn",map);
		return record;
	}
	@RequestMapping(value = "/remove/{table_name}/{seq_name}/{seq}", method = RequestMethod.GET)
	public @ResponseBody int removeDELETE(@PathVariable String table_name,@PathVariable String seq_name,@PathVariable int seq) throws Exception {
    	HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("table_name", table_name);
		map.put("seq_name", seq_name);
		map.put("seq", seq);
    	int record = service.dataDelete("mapper.AdminCommonMapper", "remove", map); 
	    return record;
	}
}
