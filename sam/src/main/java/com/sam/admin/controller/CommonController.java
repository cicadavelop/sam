package com.sam.admin.controller;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;

import com.sam.admin.service.AdminLoginService;
import com.sam.service.MasterService;

@Controller
public class CommonController {
	@Inject MasterService service;
	@Inject AdminLoginService loginService;
	com.sam.admin.util.Common common = new com.sam.admin.util.Common();
	@Inject FileUploadController fileUploadController;
	String redirectUrl = "/supervise";
}
