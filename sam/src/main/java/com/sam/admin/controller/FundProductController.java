package com.sam.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.sam.domain.FundProdEntity;
import com.sam.domain.ReportEntity;

@Controller
@RequestMapping("/supervise/fund*")
public class FundProductController extends com.sam.admin.controller.CommonController {
	
	String beforePath = "adminLayout/fund";
	
	//리스트
	@GetMapping(value="")
	public String list(Model model) {
		List<FundProdEntity> list =  (List<FundProdEntity>) service.dataList("mapper.AdminFundProdMapper", "list", null);
		model.addAttribute("list", list);
		return beforePath+"/list";
	}
	
	//상세
	@GetMapping(value="/{seq}")
	public String read(Model model, @PathVariable String seq ) {
		FundProdEntity fundEntity = (FundProdEntity) service.dataRead("mapper.AdminFundProdMapper", "read", seq);
		model.addAttribute("read", fundEntity);
		return beforePath+"/read";
	}
	
	//등록
	@GetMapping(value="/create")
	public String add(Model model) {
		return beforePath+"/create";
	}
	
	//등록 데이터 저장
	@PostMapping(value="/create")
	public String addPost(Model model, HttpServletRequest request,  FundProdEntity requestParam) {
		
		MultipartFile img_mobile = null;
		img_mobile = requestParam.getFund_images();
		if(img_mobile != null && !img_mobile.isEmpty()) requestParam.setFund_image(fileUploadController.fileupload(img_mobile, request));
		service.dataCreate("mapper.AdminFundProdMapper", "create", requestParam); 
		
		return "redirect:"+redirectUrl+"/fund/list";
	}
	
	//수정
	@GetMapping(value="/update/{seq}")
	public String update(Model model, @PathVariable String seq ) { 
		FundProdEntity FundEntity = (FundProdEntity) service.dataRead("mapper.AdminFundProdMapper", "read", seq);
		model.addAttribute("read", FundEntity);
		return beforePath+"/update";
	}
	
	//수정 데이터 저장
	@PostMapping(value="/update")
	public String updatePost(Model model, HttpServletRequest request, FundProdEntity requestParam) {
		
		MultipartFile img_mobile = null;
		img_mobile = requestParam.getFund_images();
		if(img_mobile != null && !img_mobile.isEmpty()) requestParam.setFund_image(fileUploadController.fileupload(img_mobile, request));
		service.dataUpdate("mapper.AdminFundProdMapper", "update", requestParam); 
		
		return "redirect:"+redirectUrl+"/fund";
		
	}
}
