package com.sam.admin.controller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sam.admin.domain.AdminEntity;
import com.sam.admin.service.AdminLoginService;
import com.sam.admin.util.Common;
import com.sam.domain.Meta;
import com.sam.service.MasterService;
import com.sam.util.Util;

@Controller
@RequestMapping("/supervise")
public class AdminController {
	
	@Inject MasterService service;
	
	@Inject AdminLoginService loginService;
	
	Common common = new Common();
	
	@GetMapping(value="")
	public String login(HttpSession session) {
		AdminEntity loginCheck = (AdminEntity) session.getAttribute("AdminEntity");
		if(null != loginCheck) {
			return "adminLayout/index/home";
		}else {
			return "/admin/login";
		}
	}
	
	
	/**
	 * 
	 * @msg �α���
	 * @param request
	 * @param model
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value="/login")
	public ResponseEntity<Map<String, Object>> login(HttpServletRequest request, Model model, HttpSession session, @RequestBody AdminEntity vo) throws Exception {
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		Meta meta = new Meta();
		
		HashMap<String, Object> rst = new HashMap<String, Object>();
		
		
		String login_rst = loginService.login(vo);
		
		if(Util.chkNullData(login_rst, "102")) {
			
			String push_token = vo.getPush_token();
			
			vo = loginService.user_info(vo);
			String token = common.makeToken(vo.getEmail(), vo.getPassword(), String.valueOf(vo.getAdmin_seq()));
			session.setAttribute("AdminEntity", vo);
			rst.put("authToken", token);
			
			
			/**
			 * Ǫ����ū ������Ʈ
			 */
			if(!Util.chkNull(push_token)) {
				vo.setPush_token(push_token);
				loginService.push_token_update(vo);
			}
			
			meta.setResult_code(200);  //��������ġ
		} else if(Util.chkNull(login_rst)){
			meta.setResult_code(500);  //��������ġ
			meta.setResult_msg("�̸����� Ȯ�����ּ���.");
		} else {
			meta.setResult_code(500); //�н����� ����ġ
			meta.setResult_msg("��й�ȣ�� Ȯ�����ּ���.");
		}
		
	    map.put("result", meta);
	    map.put("data", rst);
	    
	    
	    
	    try {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.OK);} 
	    catch (Exception e) {entity = new ResponseEntity<Map<String, Object>>(map,HttpStatus.BAD_REQUEST);}
	    Common.returnPrint(Common.GmakeDynamicValueObject(entity));
	    return entity;
	}
}
