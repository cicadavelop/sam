package com.sam.admin.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sam.domain.MemberEntity;

@Controller
@RequestMapping("/supervise/member*")
public class AdminMemberController extends com.sam.admin.controller.CommonController{
	
	String beforePath = "adminLayout/member";
	
	//리스트
	@GetMapping(value="")
	public String list(Model model) {
		List<MemberEntity> list =  (List<MemberEntity>) service.dataList("mapper.AdminMemberMapper", "list", null);
		model.addAttribute("list", list);
		return beforePath+"/list";
	}
	
	//상세
	@GetMapping(value="/{seq}")
	public String read(Model model, @PathVariable String seq ) {
		MemberEntity memberEntity = (MemberEntity) service.dataRead("mapper.AdminMemberMapper", "read", seq);
		model.addAttribute("read", memberEntity);
		return beforePath+"/read";
	}
	
	//등록
	@GetMapping(value="/create")
	public String add(Model model) {
		return beforePath+"/create";
	}
	
	//등록 데이터 저장
	@PostMapping(value="/create")
	public String addPost(Model model, HttpServletRequest request,  MemberEntity requestParam) {
		
		service.dataCreate("mapper.AdminMemberMapper", "create", requestParam); 
		return "redirect:"+redirectUrl+"/member";
	}
	
	//수정
	@GetMapping(value="/update/{seq}")
	public String update(Model model, @PathVariable String seq ) { 
		MemberEntity memberEntity = (MemberEntity) service.dataRead("mapper.AdminMemberMapper", "read", seq);
		model.addAttribute("read", memberEntity);
		return beforePath+"/update";
	}
	
	//수정 데이터 저장
	@PostMapping(value="/update")
	public String updatePost(Model model, HttpServletRequest request, MemberEntity requestParam) {
		
		service.dataUpdate("mapper.AdminMemberMapper", "update", requestParam); 
		return "redirect:"+redirectUrl+"/member";
		
	}
}
