package com.sam.admin.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.sam.domain.ReportEntity;

@Controller
@RequestMapping("/supervise/report*")
public class ReportController extends com.sam.admin.controller.CommonController{
	
	String beforePath = "adminLayout/report";
	
	//리스트
	@GetMapping(value="")
	public String list(Model model) {
		List<ReportEntity> list =  (List<ReportEntity>) service.dataList("mapper.AdminReportMapper", "list", null);
		model.addAttribute("list", list);
		return beforePath+"/list";
	}
	
	//상세
	@GetMapping(value="/{seq}")
	public String read(Model model, @PathVariable String seq ) {
		ReportEntity reportEntity = (ReportEntity) service.dataRead("mapper.AdminReportMapper", "read", seq);
		model.addAttribute("read", reportEntity);
		return beforePath+"/read";
	}
	
	//등록
	@GetMapping(value="/create")
	public String add(Model model) {
		return beforePath+"/create";
	}
	
	//등록 데이터 저장
	@PostMapping(value="/create")
	public String addPost(Model model, HttpServletRequest request,  ReportEntity requestParam) {
		
		MultipartFile img_mobile = null;
		img_mobile = requestParam.getReport_images();
		if(img_mobile != null && !img_mobile.isEmpty()) requestParam.setReport_image(fileUploadController.fileupload(img_mobile, request));
		service.dataCreate("mapper.AdminReportMapper", "create", requestParam); 
		
		return "redirect:"+redirectUrl+"/report";
	}
	
	//수정
	@GetMapping(value="/update/{seq}")
	public String update(Model model, @PathVariable String seq ) { 
		ReportEntity reportEntity = (ReportEntity) service.dataRead("mapper.AdminReportMapper", "read", seq);
		model.addAttribute("read", reportEntity);
		return beforePath+"/update";
	}
	
	//수정 데이터 저장
	@PostMapping(value="/update")
	public String updatePost(Model model, HttpServletRequest request, ReportEntity requestParam) {
		
		MultipartFile img_mobile = null;
		img_mobile = requestParam.getReport_images();
		if(img_mobile != null && !img_mobile.isEmpty()) requestParam.setReport_image(fileUploadController.fileupload(img_mobile, request));
		service.dataUpdate("mapper.AdminReportMapper", "update", requestParam); 
		
		return "redirect:"+redirectUrl+"/report";
		
	}
}
