package com.sam.admin.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.sam.domain.LoanEntity;
import com.sam.domain.ReportEntity;

@Controller
@RequestMapping("/supervise/loan*")
public class AdminLoanController extends com.sam.admin.controller.CommonController{
	
	String beforePath = "adminLayout/loan";
	
	//리스트
	@GetMapping(value="")
	public String list(Model model) {
		List<LoanEntity> list =  (List<LoanEntity>) service.dataList("mapper.AdminLoanMapper", "list", null);
		model.addAttribute("list", list);
		return beforePath+"/list";
	}
	
	//상세
	@GetMapping(value="/{seq}")
	public String read(Model model, @PathVariable String seq ) {
		LoanEntity reportEntity = (LoanEntity) service.dataRead("mapper.AdminLoanMapper", "read", seq);
		model.addAttribute("read", reportEntity);
		return beforePath+"/read";
	}
	
}
